import 'dart:convert';

import 'package:flutter/services.dart';

import 'entities/entities.dart';
import 'entities/widget.dart';

class QuizLoader {
  Future<List<Quiz>> load() async {
    final widgets = (jsonDecode(
            await rootBundle.loadString('assets/data/quiz1.json')) as List)
        .map<WidgetData>(
            (dynamic json) => WidgetData.fromJson(json as Map<String, dynamic>))
        .toList();
    return (widgets).map<Quiz>((correct) => Quiz(
              correct: correct,
              others: (widgets)
                  .toList()
                  .sublist(0, 3),
            ))
        .toList();
  }
}