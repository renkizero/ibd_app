// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnswerInfo _$AnswerInfoFromJson(Map<String, dynamic> json) {
  return AnswerInfo(
    json['ans1'] as String,
    json['ans2'] as String,
    json['ans3'] as String,
    json['ans4'] as String,
    json['ans5'] as String,
    json['ans6'] as String,
  );
}

Map<String, dynamic> _$AnswerInfoToJson(AnswerInfo instance) =>
    <String, dynamic>{
      'ans1': instance.ans1,
      'ans2': instance.ans2,
      'ans3': instance.ans3,
      'ans4': instance.ans4,
      'ans5': instance.ans5,
      'ans6': instance.ans6,
    };
