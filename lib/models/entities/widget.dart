import 'package:meta/meta.dart';

class WidgetData {
  WidgetData({
    @required this.question,
    @required this.type
  });

  WidgetData.fromJson(Map<String, dynamic> json)
      : question = json['question'] as String,
        type = json['type'] as String;

  final String question;
  final String type;
}