import 'package:ibd_application/models/entities/answer_info.dart';
import 'package:ibd_application/models/entities/quiz_info.dart';
import 'package:json_annotation/json_annotation.dart';

part 'quiz_result.g.dart';

@JsonSerializable()
class QuizResult {


  @JsonKey(name: 'result')
  List<QuizInfo> result;

  QuizResult(this.result);

  factory QuizResult.fromJson(Map<String, dynamic> json) => _$QuizResultFromJson(json);

  Map<String, dynamic> toJson() => _$QuizResultToJson(this);
  
}