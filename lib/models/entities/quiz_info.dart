import 'package:ibd_application/models/entities/answer_info.dart';
import 'package:json_annotation/json_annotation.dart';

part 'quiz_info.g.dart';

@JsonSerializable()
class QuizInfo {

  @JsonKey(name: 'question')
  String question;

  @JsonKey(name: 'type')
  String type;

  @JsonKey(name: 'answer')
  List<AnswerInfo> ansinfo;

  QuizInfo(this.question, this.type);

  factory QuizInfo.fromJson(Map<String, dynamic> json) => _$QuizInfoFromJson(json);

  Map<String, dynamic> toJson() => _$QuizInfoToJson(this);
  
}