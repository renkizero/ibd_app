// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quiz_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QuizInfo _$QuizInfoFromJson(Map<String, dynamic> json) {
  return QuizInfo(
    json['question'] as String,
    json['type'] as String,
  )..ansinfo = (json['answer'] as List)
      ?.map((e) =>
          e == null ? null : AnswerInfo.fromJson(e as Map<String, dynamic>))
      ?.toList();
}

Map<String, dynamic> _$QuizInfoToJson(QuizInfo instance) => <String, dynamic>{
      'question': instance.question,
      'type': instance.type,
      'answer': instance.ansinfo,
    };
