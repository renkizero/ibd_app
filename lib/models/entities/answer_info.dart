import 'package:json_annotation/json_annotation.dart';

part 'answer_info.g.dart';

@JsonSerializable()
class AnswerInfo {

  @JsonKey(name: 'ans1')
  String ans1;

  @JsonKey(name: 'ans2')
  String ans2;

  @JsonKey(name: 'ans3')
  String ans3;

  @JsonKey(name: 'ans4')
  String ans4;

  @JsonKey(name: 'ans5')
  String ans5;

  @JsonKey(name: 'ans6')
  String ans6;

  AnswerInfo(this.ans1, this.ans2, this.ans3, this.ans4, this.ans5, this.ans6);

  factory AnswerInfo.fromJson(Map<String, dynamic> json) => _$AnswerInfoFromJson(json);

  Map<String, dynamic> toJson() => _$AnswerInfoToJson(this);
}