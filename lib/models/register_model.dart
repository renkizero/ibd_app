class RegisterModel {
  String gender;
  String firstname;
  String lastname;
  String email;
  String phone;
  String password;

  RegisterModel(this.gender, this.firstname, this.lastname, this.email,
      this.phone, this.password);

  @override
  String toString() {
    return '{ ${this.gender}, ${this.firstname}, ${this.lastname} , ${this.email}, ${this.phone}, ${this.password} }';
  }
}
