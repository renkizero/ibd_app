import 'pages/constants/dbconfig.dart';

Future<void> saveMemo(String date, String month, String year, String hour,
    String min, String note) async {
  final table = "memo";
  final row = {
    "date": date,
    "month": month,
    "year": year,
    "hour": hour,
    "min": min,
    "note": note,
    "status": "1",
  };
  await db
      .insert(table: table, row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}

Future<void> updateMemo(String date, String month, String year, String hour,
    String min, String note, String updated) async {
  final table = "memo";
  final row = {
    "date": date,
    "month": month,
    "year": year,
    "hour": hour,
    "min": min,
    "note": note,
    "status": "1",
  };
  await db
      .update(
          table: table, where: 'updated="$updated"', row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}

Future<void> deleteMemo(String updated) async {
  final table = "memo";
  final row = {
    "status": "0",
  };
  await db
      .update(
          table: table, where: 'updated="$updated"', row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}

Future<void> deleteItem(int itemId) async {
  final table = "memo";
  await db
      .delete(table: table, where: 'id="$itemId"', verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}



Future<void> deleteNote(String updated) async {
  final table = "note";
  await db
      .delete(table: table, where: 'updated="$updated"', verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}


Future<void> updateNote(String subject, String note,String updated) async {
  final table = "note";
  final row = {
    "subject": subject,
    "note": note,
  };
  await db
      .update(
          table: table, where: 'updated = "$updated"', row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}

Future<void> saveNote(String subject, String note) async {
  final table = "note";
  final row = {
    "subject": subject,
    "note": note,
  };
  await db
      .insert(table: table, row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}



Future<void> saveWell(String datefull, String day, String month, String year, String type, String val) async {
  final table = "well";
  final row = {
    "datefull": datefull,
    "day": day,
    "month": month,
    "year": year,
    "type": type,
    "val": val,
  };
  await db
      .insert(table: table, row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}


Future<void> updatePaintInfo2(  String med) async {
  final table = "paintinfo";
  final row = {
    "med": med
  };
  await db
      .update(
      table: table, row: row, verbose: true, where: "id = 1")
      .catchError((dynamic e) {
    throw (e);
  });
}



Future<void> updatePaintInfo1(  String choice) async {
  final table = "paintinfo";
  final row = {
    "choice": choice
  };
  await db
      .update(
      table: table, row: row, verbose: true, where: "id = 1")
      .catchError((dynamic e) {
    throw (e);
  });
}



Future<void> savePaintInfo( String hospital, String doctor, String choice, String med) async {
  final table = "paintinfo";
  final row = {
    "hospital": hospital,
    "doctor": doctor,
    "choice": choice,
    "med": med,
  };
  await db
      .insert(table: table, row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}



Future<void> updatePaintInfo( String hospital, String doctor, String choice, String med) async {
  final table = "paintinfo";
  final row = {
    "hospital": hospital,
    "doctor": doctor,
    "choice": choice,
    "med": med,
  };
  await db
      .update(
      table: table, row: row, verbose: true, where: "id = 1")
      .catchError((dynamic e) {
    throw (e);
  });
}




Future<void> saveProfile(String title, String firstname, String lastname, String email, String phone, String password) async {
  final table = "users";
  final row = {
    "title": title,
    "firstname": "$firstname",
    "lastname": "$lastname",
    "email": email,
    "phone": "$phone",
    "password": "$password",
  };


  await db
      .update(
      table: table, row: row, verbose: true, where: "id = 1")
      .catchError((dynamic e) {
    throw (e);
  });


}



Future<void> updateProfile(int title, String firstname, String lastname, String email, String phone, String password) async {
  final table = "user";
  final row = {
    "title": title,
    "firstname": firstname,
    "lastname": lastname,
    "email": email,
    "phone": phone,
    "password": password,
  };
  await db
      .update(
      table: table, row: row, verbose: true, where: null)
      .catchError((dynamic e) {
    throw (e);
  });
}



Future<void> updateQuiz(String choice, String topic) async {
  final table = "quiz";
  final row = {
    "choice": choice,
  };
  await db
      .update(
      table: table, where: 'topic="$topic"', row: row, verbose: true)
      .catchError((dynamic e) {
    throw (e);
  });
}
