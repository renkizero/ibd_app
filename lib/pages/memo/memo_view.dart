import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:ibd_application/pages/memo/memo_edit1.dart';
import 'package:sqlcool/sqlcool.dart';

class MemoView extends StatefulWidget {
  const MemoView({Key key, this.id}) : super(key: key);
  final String id;

  @override
  _MemoViewState createState() => _MemoViewState(id);
}

class _MemoViewState extends State<MemoView> {
  String _subject;
  String _note;
  Function _onChanged;
  String _id;
  SelectBloc bloc;
  String id;
  _MemoViewState(this.id);

  void initState() {
    super.initState();
    // print(id);
    this.bloc = SelectBloc(
        database: db, table: "note", where: "updated = $id", reactive: true);
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: const Text(
            'บันทึก',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Kanit',
              fontSize: 20,
              color: Color(0xff000000),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) {
                    //return new MemoView("${row['updated']}");
                    return new MemoEditNew(id: id);
                  },
                ),
              ),
              child: Text(
                'แก้ไข',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Kanit',
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                  color: Color(0xff000000),
                ),
              ),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
          leading: FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Icon(Icons.arrow_back_ios),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          backgroundColor: HexColor("FFFFFF"),
        ),
        body: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: StreamBuilder<List<Map>>(
                stream: bloc.items,
                builder:
                    // ignore: missing_return
                    (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData && snapshot.data != null) {
                    if (snapshot.data != null) {
                      final dynamic item = snapshot.data;
                      print(item[0]);
                      final note = "${item[0]["note"]}";
                      final subject = "${item[0]["subject"]}";
                      return new GestureDetector(
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 0,
                                      color: const Color(0xFFFFFF),
                                    ),
                                  ),
                                  child: Container(
                                      padding: const EdgeInsets.only(
                                          top: 15, left: 16.0),
                                      child: Text(
                                        subject,
                                        style: new TextStyle(
                                          fontFamily: 'Kanit',
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      )),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(top: 1),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.9,
                                      height: 250,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: 0,
                                          color: const Color(0xFFFFFF),
                                        ),
                                      ),
                                      child: Container(
                                          padding: const EdgeInsets.only(
                                              left: 16.0, top: 16),
                                          child: new Text(
                                            note,
                                            style: new TextStyle(
                                              fontFamily: 'Kanit',
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16,
                                              color: Colors.black,
                                            ),
                                          )),
                                    ))
                              ]),
                        ),
                      );
                    }else {
                      return Container();
                    }
                  } else {
                    return Container();
                  }
                })));
  }
}
