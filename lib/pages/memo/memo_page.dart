import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/listview.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:ibd_application/pages/memo/memo_edit.dart';
import 'package:ibd_application/pages/memo/memo_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ibd_application/database.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:timeago/timeago.dart';

class MemoPage extends StatefulWidget {
  const MemoPage({Key key}) : super(key: key);
  static const routeName = '/memo';

  @override
  _MemoPageState createState() => _MemoPageState();
}

class _MemoPageState extends State<MemoPage> {
  List<Map<String, dynamic>> _rows;
  var _memoList = new List<String>();
  var _currentIndex = -1;
  bool _loading = true;
  final _biggerFont = const TextStyle(fontSize: 18.0);
  bool databaseIsReady = false;
  var _ready = false;
  SelectBloc bloc;
  bool hasData = false;

  @override
  void initState() {
    this.bloc = SelectBloc(
        database: db, table: "note", orderBy: "updated DESC", reactive: true);
    //_getData().then((_) => setState(() => _ready = true));
    _ready = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
          backgroundColor: HexColor("FFFFFF"),
          title: const Text('บันทึก',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Kanit',
                fontSize: 20,
                color: Color(0xff000000),
              )),
          actions: <Widget>[
            FlatButton(
              onPressed: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: InkWell(
                  onTap: () => _addMemo(),
                  child: Container(
                    width: 45,
                    height: 45,
                    child: Align(
                      child: Image.asset(
                        "assets/images/group-2631.png",
                        fit: BoxFit.none,
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: const Color(0xffffffff),
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0, 2),
                          blurRadius: 6,
                          color: const Color(0xff5e4589).withOpacity(0.3),
                        )
                      ],
                    ),
                  )),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ]),
      body: StreamBuilder<List<Map>>(
              stream: bloc.items,
              // ignore: missing_return
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData && snapshot.data != null) {
                  if (snapshot.data != null) {
                    // the select query has not found anything
                    if (snapshot.data.length == 0) {
                      return Center(
                          child:  Container(width: 0.0, height: 0.0));
                    } else {
                      return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          final dynamic row = snapshot.data[index];
                          // final row = _rows[index];
                          double c_width =
                              MediaQuery.of(context).size.width * 0.8;

                          final DateTime timeStamp =
                              DateTime.fromMillisecondsSinceEpoch(
                                  row['updated'] * 1000);
                          // var date = new DateTime.fromMicrosecondsSinceEpoch(row['updated'] * 1000);
                          /*ListTile(
                    title: Text(row['subject']), trailing: Text(row["note"])

                    );*/
                          return new InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                    builder: (context) {
                                      //return new MemoView("${row['updated']}");
                                      return new MemoView(
                                          id: "${row['updated']}");
                                    },
                                  ),
                                );
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 100,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, left: 4, right: 4),
                                        child: Container(
                                            padding: EdgeInsets.only(
                                                top: 10, left: 20, right: 20),
                                            width: c_width,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  row['subject'],
                                                  style: TextStyle(
                                                    fontFamily: 'Kanit',
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xff4c5264),
                                                  ),
                                                ),
                                                Text(
                                                  row['note'],
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    fontFamily: 'Kanit',
                                                    fontSize: 14,
                                                    color: Color(0xff4c5264),
                                                  ),
                                                ),
                                                Text(
                                                  "${timeStamp.day}/${timeStamp.month}/${timeStamp.year}",
                                                  style: TextStyle(
                                                    fontFamily: 'Kanit',
                                                    fontSize: 12,
                                                    color: Color(0xffbcc5d3),
                                                  ),
                                                )
                                              ],
                                            ))),
                                    Container(
                                        width: 30,
                                        height: 150,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/images/icons-chevron_.png",
                                              fit: BoxFit.none,
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              ));
                        },
                      );
                    }
                  }
                  else {
                    return Container(width: 0.0, height: 0.0);
                  }
                }else{
                  return Container(width: 0.0, height: 0.0);
                }})
    );
    /*floatingActionButton: FloatingActionButton(
        onPressed: _addMemo,
        tooltip: 'New Memo',
        child: Icon(Icons.add),
      )*/
  }

  void storeMemoList() async {
    /*final prefs = await SharedPreferences.getInstance();
    const key = "memo-list";
    final success = await prefs.setStringList(key, _memoList);
    if (!success) {
      debugPrint("Failed to store value");
    }*/

    //_getData();
  }

  void _onChanged(String text, String note) {
    setState(() {
      if (text.length > 0 && note.length > 0) {
        saveNote(text, note);

        //_memoList[_currentIndex] = text;
        // loadMemoList();
      }
    });
  }

  void _addMemo() {
    setState(() {
      _memoList.add("");
      _currentIndex = _memoList.length - 1;
      storeMemoList();
      Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return new MemoEdit(_memoList[_currentIndex], _onChanged);
        },
      ));
    });
  }
}
