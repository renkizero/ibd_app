import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/hexcolor.dart';

class MemoEdit extends StatelessWidget {
  String _subject;
  String _note;
  Function _onChanged;
  String _id;

  MemoEdit(this._id, this._onChanged);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: const Text(
            'บันทึก',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Kanit',
              fontSize: 20,
              color: Color(0xff000000),
            ),
          ),
          actions: <Widget>[
            /*  FlatButton(
            onPressed: () => FocusScope.of(context).requestFocus(FocusNode()),
            child: Icon(Icons.check),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),*/
          ],
          leading: FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Icon(Icons.arrow_back_ios),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          backgroundColor: HexColor("FFFFFF"),
        ),
        body: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
              padding: EdgeInsets.all(12),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 50,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: const Color(0xffe2e8ed),
                        ),
                      ),
                      child: Container(
                          padding: const EdgeInsets.only(left: 16.0),
                          child: new TextField(
                            controller: TextEditingController(text: _subject),
                            maxLines: 1,
                            decoration: InputDecoration.collapsed(
                                hintText: "หัวเรืื่อง",
                                hintStyle: TextStyle(
                                  fontFamily: 'Kanit',
                                  fontSize: 16,
                                  color: Color(0xffbcc5d3),
                                )),
                            style: new TextStyle(
                              fontFamily: 'Kanit',
                              fontSize: 16,
                              color: Colors.black,
                            ),
                            onChanged: (text) {
                              _subject = text;
                            },
                          )),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: 250,
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: const Color(0xffe2e8ed),
                            ),
                          ),
                          child: Container(
                              padding:
                                  const EdgeInsets.only(left: 16.0, top: 16),
                              child: new TextField(
                                controller: TextEditingController(text: _note),
                                maxLines: 99,
                                decoration: InputDecoration.collapsed(
                                    hintText: "ข้อความ",
                                    hintStyle: TextStyle(
                                      fontFamily: 'Kanit',
                                      fontSize: 16,
                                      color: Color(0xffbcc5d3),
                                    )),
                                style: new TextStyle(
                                  fontFamily: 'Kanit',
                                  fontSize: 16,
                                  color: Colors.black,
                                ),
                                onChanged: (text) {
                                  _note = text;
                                },
                              )),
                        )),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Container(
                          width: 335,
                          height: 50,
                          decoration: BoxDecoration(
                            color: const Color(0xfff8b6b8),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: InkWell(
                            onTap: () {
                              _onChanged(_subject, _note);
                              Navigator.pop(context);
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'บันทึกข้อมูล',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'Kanit',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff5e4589),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    )
                  ]),
            )));
  }
}
