import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:sqlcool/sqlcool.dart';

class MemoEditNew extends StatefulWidget {
  const MemoEditNew({Key key, this.id}) : super(key: key);
  final String id;

  @override
  _MemoEditNewState createState() => _MemoEditNewState(id);
}

class _MemoEditNewState extends State<MemoEditNew> {
  String _subject;
  String _note;
  Function _onChanged;
  String _id;
  SelectBloc bloc;
  String id;
  _MemoEditNewState(this.id);

  void initState() {
    super.initState();

    this.bloc = SelectBloc(
        database: db, table: "note", where: "updated = $id", reactive: true);
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: const Text(
            'แก้ไข',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Kanit',
              fontSize: 20,
              color: Color(0xff000000),
            ),
          ),
          actions: <Widget>[],
          leading: FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Icon(Icons.arrow_back_ios),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          backgroundColor: HexColor("FFFFFF"),
        ),
        body: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: StreamBuilder<List<Map>>(
                stream: bloc.items,
                builder:
                    // ignore: missing_return
                    (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData && snapshot.data != null) {
                    if (snapshot.data != null) {
                      final dynamic item = snapshot.data;
                      print(item[0]);
                      final note = "${item[0]["note"]}";
                      _note = note;
                      final subject = "${item[0]["subject"]}";
                      _subject = subject;
                      return new Container(
                        padding: EdgeInsets.all(12),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.9,
                                height: 50,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 0,
                                    color: const Color(0xffbcc5d3),
                                  ),
                                ),
                                child: Container(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: new TextField(
                                      controller:
                                          TextEditingController(text: _subject),
                                      maxLines: 1,
                                      decoration: InputDecoration.collapsed(
                                          hintText: "หัวเรืื่อง",
                                          hintStyle: TextStyle(
                                            fontFamily: 'Kanit',
                                            fontSize: 16,
                                            color: Color(0xffbcc5d3),
                                          )),
                                      style: new TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                      onChanged: (text) {
                                        _subject = text;
                                      },
                                    )),
                              ),
                              Padding(
                                  padding: EdgeInsets.only(top: 10),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    height: 250,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        width: 0,
                                        color: const Color(0xffbcc5d3),
                                      ),
                                    ),
                                    child: Container(
                                        padding: const EdgeInsets.only(
                                            left: 16.0, top: 16),
                                        child: new TextField(
                                          controller: TextEditingController(
                                              text: _note),
                                          maxLines: 99,
                                          decoration: InputDecoration.collapsed(
                                              hintText: "ข้อความ",
                                              hintStyle: TextStyle(
                                                fontFamily: 'Kanit',
                                                fontSize: 16,
                                                color: Color(0xffbcc5d3),
                                              )),
                                          style: new TextStyle(
                                            fontFamily: 'Kanit',
                                            fontSize: 16,
                                            color: Colors.black,
                                          ),
                                          onChanged: (text) {
                                            _note = text;
                                          },
                                        )),
                                  )),
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Container(
                                    width: 335,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      color: const Color(0xfff8b6b8),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: InkWell(
                                      onTap: () async {
                                        //_onChanged(_subject, _note);
                                        updateNote(_subject, _note, id);
                                        Navigator.pop(context);
                                      },
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'แก้ไขข้อมูล',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontFamily: 'Kanit',
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xff5e4589),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                              ),
                              Container(
                                  padding: EdgeInsets.only(top: 30),
                                  width: MediaQuery.of(context).size.width,
                                  height: 100,
                                  child: InkWell(
                                    onTap: () async {
                                      //_onChanged(_subject, _note);
                                      //updateNote(_subject, _note, id);
                                     // Navigator.pop(context);

                                      var count = 0;
                                      Navigator.popUntil(context, (route) {
                                        return count++ == 2;
                                      });
                                      deleteNote(id);
                                    },
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        const Text(
                                          'ลบข้อมูล',
                                          style: TextStyle(
                                            fontFamily: 'Kanit',
                                            fontSize: 16,
                                            color: Color(0xffbcc5d3),
                                          ),
                                        )
                                      ],
                                    ),
                                  ))
                            ]),
                      );
                    }
                  }
                })));
  }
}
