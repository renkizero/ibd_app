import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:ibd_application/pages/appointment/appointment_page.dart';
import 'package:ibd_application/util.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/values/values.dart';

class AppointMentAddPage extends StatefulWidget {
  const AppointMentAddPage({Key key, this.id}) : super(key: key);
  final String id;
  static const routeName = '/appointmentadd';

  @override
  _AppointMentAddPageState createState() => _AppointMentAddPageState(id);
}

class _AppointMentAddPageState extends State<AppointMentAddPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  DateTime _timeOfDay1 = DateTime.now();
  TimeOfDay _timeOfDay2 = TimeOfDay.now();
  int _currentValue = 1;
  String note;
  String id;
  _AppointMentAddPageState(this.id);

  void initState() {
    super.initState();
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    initializeNotifications();
  }

  initializeNotifications() async {
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/launcher_icon');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
    await Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => AppointmentPage()),
    );
  }

  @override
  Widget build(BuildContext context) {

    var now = DateTime.now();
    var today= new DateTime(now.year, now.month, now.day,now.hour,0,0);

    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return new Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: BackButton(color: Colors.black),
          title: const _Title(),
          backgroundColor: HexColor("FFFFFF"),
        ),
        body: SingleChildScrollView(
            reverse: true,
            child: Padding(
                padding: EdgeInsets.only(bottom: bottom),
                child: new GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 10, left: 30, right: 30),
                        child: Text(
                          'วันนัด',
                          style: TextStyle(
                            fontFamily: "Kanit",
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        height: 150.0,
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.date,
                          onDateTimeChanged: (DateTime dateTime) {
                            _timeOfDay1 = dateTime;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 30),
                        child: Text(
                          'เวลานัด',
                          style: TextStyle(
                            fontFamily: "Kanit",
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        height: 150.0,
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.time,
                          minimumDate: DateTime.now().subtract(Duration(days: 1)),
                          initialDateTime: today,
                          minuteInterval: 30,
                          onDateTimeChanged: (DateTime newDateTime) {
                            _timeOfDay2 = TimeOfDay.fromDateTime(newDateTime);
                          },
                          use24hFormat: true,
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: 30, right: 30),
                          child: Container(
                              width: 335,
                              height: 90,
                              decoration: BoxDecoration(
                                color: const Color(0xffffffff),
                                border: Border.all(
                                  width: 1,
                                  color: const Color(0xffe2e8ed),
                                ),
                              ),
                              child: FormBuilder(
                                  key: _fbKey,
                                  autovalidate: true,
                                  child: Column(children: <Widget>[
                                    TextField(
                                      onChanged: (text) {
                                        note = text;
                                      },
                                      maxLines: 4,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.all(5),
                                          hintText: "บันทึกช่วยจำ",
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          hintStyle: TextStyle(
                                            fontFamily: 'Kanit',
                                            fontSize: 16,
                                            color: Color(0xffbcc5d3),
                                          )),
                                    ),
                                  ])))),
                      Padding(
                          padding:
                              EdgeInsets.only(top: 30, left: 30, right: 30),
                          child: Container(
                              width: 335,
                              height: 50,
                              decoration: BoxDecoration(
                                color: const Color(0xfff8b6b8),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: InkWell(
                                onTap: () async {
                                  //  saveMemo(date, month, year, hour, min, note)
                                  print(" save ${_timeOfDay1} ${_timeOfDay2}");
                                  //   if(_timeOfDay1.day>0 && _timeOfDay1.month>0 && _timeOfDay1.year>0 && _timeOfDay2.hour>0  && _timeOfDay2.minute>0){
                                  saveMemo(
                                      _timeOfDay1.day.toString(),
                                      _timeOfDay1.month.toString(),
                                      _timeOfDay1.year.toString(),
                                      _timeOfDay2.hour.toString(),
                                      _timeOfDay2.minute.toString(),
                                      note.toString());

                                  var dateTimeCreatedAt = DateTime(
                                      _timeOfDay1.year,
                                      _timeOfDay1.month,
                                      _timeOfDay1.day,
                                      _timeOfDay2.hour,
                                      _timeOfDay2.minute);

                                  // DateTime dateTimeCreatedAt = DateTime.parse('${_timeOfDay1.year.toString()}-${_timeOfDay1.month.toString()}-${_timeOfDay1.day.toString()} ${_timeOfDay1.hour.toString()}:${_timeOfDay1.minute.toString()}:00');
                                  DateTime dateTimeNow = DateTime.now();
                                  final differenceInDays = dateTimeCreatedAt
                                      .difference(dateTimeNow)
                                      .inSeconds;
                                  print('$differenceInDays');

                                  var scheduledNotificationDateTime =
                                      new DateTime.now().add(new Duration(
                                          seconds: differenceInDays));
                                  var androidPlatformChannelSpecifics =
                                      new AndroidNotificationDetails(
                                          'your other channel id',
                                          'your other channel name',
                                          'your other channel description');
                                  var iOSPlatformChannelSpecifics =
                                      new IOSNotificationDetails();
                                  NotificationDetails platformChannelSpecifics =
                                      new NotificationDetails(
                                          androidPlatformChannelSpecifics,
                                          iOSPlatformChannelSpecifics);

                                  var d1 = _timeOfDay1.day.toString();
                                  var d2 = _timeOfDay1.month.toString();
                                  var d3 = _timeOfDay1.year.toString();
                                  var d4 =  _timeOfDay2.hour.toString();
                                  var d5 =  _timeOfDay2.minute.toString();
                                  await flutterLocalNotificationsPlugin
                                      .schedule(
                                          0,
                                          'IBD App',
                                          "สวัสดีครับคุณ มีนัดพบคุณหมอวันที่ $d1/$d2/$d3 เวลา $d4.$d5 อย่าลืมนะคับผม",
                                          scheduledNotificationDateTime,
                                          platformChannelSpecifics);

                                  //  }
                                  Navigator.pop(context);
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'บันทึกข้อมูล',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff5e4589),
                                      ),
                                    ),
                                  ],
                                ),
                              )))
                    ],
                  ),
                ))));
  }
}

class _Title extends StatelessWidget {
  const _Title({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          'เพิ่มวันที่นัดหมาย',
          style: TextStyle(
            fontFamily: "Kanit",
            fontSize: 20,
            color: Color(0xff000000),
          ),
        )
      ],
    );
  }
}
