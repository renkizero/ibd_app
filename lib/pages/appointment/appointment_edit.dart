import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:ibd_application/pages/appointment/appointment_page.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:ibd_application/util.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/values/values.dart';
import 'package:sqlcool/sqlcool.dart';

class AppointMentEditPage extends StatefulWidget {
  const AppointMentEditPage({Key key, this.id}) : super(key: key);
  final String id;
  static const routeName = '/appointmentadd';

  @override
  _AppointMentEditPageState createState() => _AppointMentEditPageState(id);
}

class _AppointMentEditPageState extends State<AppointMentEditPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  DateTime _timeOfDay1 = DateTime.now();
  TimeOfDay _timeOfDay2 = TimeOfDay.now();
  int _currentValue = 1;
  String note;
  String id;
  var isLoading = true;
  SelectBloc bloc;
  _AppointMentEditPageState(this.id);

  void initState() {
    super.initState();
    this.bloc = SelectBloc(
        database: db, table: "memo", where: "updated = $id", reactive: true);
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    initializeNotifications();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  initializeNotifications() async {
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/launcher_icon');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
    await Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => AppointmentPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return new Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: BackButton(color: Colors.black),
          title: const _Title(),
          backgroundColor: HexColor("FFFFFF"),
        ),
        body: SingleChildScrollView(
            reverse: true,
            child: Padding(
                padding: EdgeInsets.only(bottom: bottom),
                child: Container(
                    margin: EdgeInsets.only(top: 11),
                    child: Center(
                        child: StreamBuilder<List<Map>>(
                            stream: bloc.items,
                            builder:
                                // ignore: missing_return
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData && snapshot.data != null) {
                                if (snapshot.data != null) {
                                  final dynamic item = snapshot.data;
                                  print(item[0]);
                                  final old_note = "${item[0]["note"]}";
                                  final old_date = "${item[0]["date"]}";
                                  final old_month = "${item[0]["month"]}";
                                  final old_year = "${item[0]["year"]}";
                                  final old_hh = "${item[0]["hour"]}";
                                  final old_mm = "${item[0]["min"]}";

                                  return new GestureDetector(
                                    onTap: () {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 10, left: 30, right: 30),
                                          child: Text(
                                            'วันนัด',
                                            style: TextStyle(
                                              fontFamily: "Kanit",
                                              fontSize: 20,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xff000000),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: 10),
                                          height: 150.0,
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode.date,
                                            initialDateTime: DateTime(
                                                int.parse(old_year),
                                                int.parse(old_month),
                                                int.parse(old_date)),
                                            onDateTimeChanged:
                                                (DateTime dateTime) {
                                              _timeOfDay1 = dateTime;
                                            },
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 30),
                                          child: Text(
                                            'เวลานัด',
                                            style: TextStyle(
                                              fontFamily: "Kanit",
                                              fontSize: 20,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xff000000),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: 10),
                                          height: 150.0,
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode.time,
                                            initialDateTime: DateTime(
                                                int.parse(old_year),
                                                int.parse(old_month),
                                                int.parse(old_date),
                                                int.parse(old_hh),
                                                int.parse(old_mm),
                                                0),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {
                                              _timeOfDay2 =
                                                  TimeOfDay.fromDateTime(
                                                      newDateTime);
                                            },
                                            use24hFormat: false,
                                            minuteInterval: 1,
                                          ),
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 30, right: 30),
                                            child: Container(
                                                width: 335,
                                                height: 90,
                                                decoration: BoxDecoration(
                                                  color:
                                                      const Color(0xffffffff),
                                                  border: Border.all(
                                                    width: 1,
                                                    color:
                                                        const Color(0xffe2e8ed),
                                                  ),
                                                ),
                                                child: FormBuilder(
                                                    key: _fbKey,
                                                    autovalidate: true,
                                                    child: Column(children: <
                                                        Widget>[
                                                      TextField(
                                                        controller:
                                                            TextEditingController()
                                                              ..text = old_note,
                                                        onChanged: (text) {
                                                          note = text;
                                                        },
                                                        maxLines: 4,
                                                        decoration:
                                                            InputDecoration(
                                                                contentPadding:
                                                                    EdgeInsets
                                                                        .all(5),
                                                                hintText:
                                                                    "บันทึกช่วยจำ",
                                                                enabledBorder: UnderlineInputBorder(
                                                                    borderSide: BorderSide(
                                                                        color: Colors
                                                                            .grey)),
                                                                focusedBorder:
                                                                    UnderlineInputBorder(
                                                                        borderSide: BorderSide(
                                                                            color: Colors
                                                                                .white)),
                                                                hintStyle:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      'Kanit',
                                                                  fontSize: 16,
                                                                  color: Color(
                                                                      0xffbcc5d3),
                                                                )),
                                                      ),
                                                    ])))),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                top: 30, left: 30, right: 30),
                                            child: Container(
                                                width: 335,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                  color:
                                                      const Color(0xfff8b6b8),
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                child: InkWell(
                                                  onTap: () async {
                                                    //  saveMemo(date, month, year, hour, min, note)
                                                    print(
                                                        "save ${_timeOfDay1} ${_timeOfDay2}");
                                                    //   if(_timeOfDay1.day>0 && _timeOfDay1.month>0 && _timeOfDay1.year>0 && _timeOfDay2.hour>0  && _timeOfDay2.minute>0){
                                                    updateMemo(
                                                        _timeOfDay1.day
                                                            .toString(),
                                                        _timeOfDay1.month
                                                            .toString(),
                                                        _timeOfDay1.year
                                                            .toString(),
                                                        _timeOfDay2.hour
                                                            .toString(),
                                                        _timeOfDay2.minute
                                                            .toString(),
                                                        note.toString(),
                                                        id);

                                                    var dateTimeCreatedAt =
                                                        DateTime(
                                                            _timeOfDay1.year,
                                                            _timeOfDay1.month,
                                                            _timeOfDay1.day,
                                                            _timeOfDay2.hour,
                                                            _timeOfDay2.minute);

                                                    // DateTime dateTimeCreatedAt = DateTime.parse('${_timeOfDay1.year.toString()}-${_timeOfDay1.month.toString()}-${_timeOfDay1.day.toString()} ${_timeOfDay1.hour.toString()}:${_timeOfDay1.minute.toString()}:00');
                                                    DateTime dateTimeNow =
                                                        DateTime.now();
                                                    final differenceInDays =
                                                        dateTimeCreatedAt
                                                            .difference(
                                                                dateTimeNow)
                                                            .inSeconds;
                                                    print('$differenceInDays');

                                                    var scheduledNotificationDateTime =
                                                        new DateTime.now().add(
                                                            new Duration(
                                                                seconds:
                                                                    differenceInDays));
                                                    var androidPlatformChannelSpecifics =
                                                        new AndroidNotificationDetails(
                                                            'your other channel id',
                                                            'your other channel name',
                                                            'your other channel description');
                                                    var iOSPlatformChannelSpecifics =
                                                        new IOSNotificationDetails();
                                                    NotificationDetails
                                                        platformChannelSpecifics =
                                                        new NotificationDetails(
                                                            androidPlatformChannelSpecifics,
                                                            iOSPlatformChannelSpecifics);
                                                    await flutterLocalNotificationsPlugin
                                                        .schedule(
                                                            0,
                                                            'IBD Application',
                                                            note.toString(),
                                                            scheduledNotificationDateTime,
                                                            platformChannelSpecifics);

                                                    //  }
                                                    Navigator.pop(context);
                                                  },
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        'แก้ไขข้อมูล',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                          fontFamily: 'Kanit',
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color:
                                                              Color(0xff5e4589),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ))),
                                        InkWell(
                                            onTap: () async {
                                              isLoading = false;
                                              Navigator.pop(context);
                                              deleteMemo(id);
                                            },
                                            child: Container(
                                              padding: EdgeInsets.only(top: 30),
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: 100,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  const Text(
                                                    'ลบข้อมูล',
                                                    style: TextStyle(
                                                      fontFamily: 'Kanit',
                                                      fontSize: 16,
                                                      color: Color(0xffbcc5d3),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ))
                                      ],
                                    ),
                                  );
                                }
                              } else {}
                            }))))));
  }

  void _popupDialog(BuildContext context, String id) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('คำเตือน'),
            content: Text('คุณต้องการลบนัดหมายนี้'),
            actions: <Widget>[
              FlatButton(onPressed: () => deleteMemo(id), child: Text('ลบ')),
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('ยกเลิก')),
            ],
          );
        });
  }
}

class _Title extends StatelessWidget {
  const _Title({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          'แก้ไขวันที่นัดหมาย',
          style: TextStyle(
            fontFamily: "Kanit",
            fontSize: 20,
            color: Color(0xff000000),
          ),
        )
      ],
    );
  }
}
