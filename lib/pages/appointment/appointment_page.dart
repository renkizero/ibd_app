import 'dart:async';
import 'dart:math';

import 'package:bloc_provider/bloc_provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:date_util/date_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/bloc/ibd_bloc.dart';
import 'package:ibd_application/bloc/quiz_blocn.dart';
import 'package:ibd_application/pages/appointment/appointment_add.dart';
import 'package:ibd_application/pages/appointment/appointment_edit.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:ibd_application/pages/quiz/quiz_1/quiz_1_page.dart';
import 'package:ibd_application/pages/quiz/quiz_1/quiz_1_q.dart';
import 'package:ibd_application/pages/quiz/quiz_2/quiz_2_q.dart';
import 'package:ibd_application/pages/quiz/quiz_2/quiz_2_start.dart';
import 'package:ibd_application/pages/quiz/quiz_fin.dart';
import 'package:ibd_application/pages/quiz/quiz_page.dart';
import 'package:ibd_application/pages/quiz/quiz_start.dart';
import 'package:ibd_application/util.dart';

import 'package:ibd_application/values/values.dart';
import 'package:ibd_application/pages/pages.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:ibd_application/database.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';



class AppointmentPage extends StatefulWidget {
  const AppointmentPage({Key key}) : super(key: key);
  static const routeName = '/appointment';
  @override
  _AppointmentPageState createState() => _AppointmentPageState();
}

class _AppointmentPageState extends State<AppointmentPage> {

  SelectBloc bloc;

  SelectBloc blocQ1;
  SelectBloc blocQ2;
  SelectBloc blocQ3;

  int _index = 0;

  StreamSubscription _changefeed;
  bool databaseIsReady = false;

  var total_q1 = 1;
  var total_q2 = 1;
  var total_q3 = 1;

  var start_q1 = 0;
  var start_q2 = 0;
  var start_q3 = 0;

  String doctor ;

  @override
  void initState() {
    /*db.onReady.then((_) {
      print("STATE: THE DATABASE IS READY");
      setState(() {
        databaseIsReady = true;
      });
    });*/
    databaseIsReady = true;

    this.bloc = SelectBloc(
        database: db,
        table: "memo",
        where: " status = 1",
        orderBy: 'updated DESC',
        reactive: true);



    this.blocQ1 = SelectBloc(
        database: db,
        table: "quiz",
        where: " topic = 1",
        reactive: true);


    this.blocQ2 = SelectBloc(
        database: db,
        table: "quiz",
        where: " topic = 2",
        reactive: true);



    this.blocQ3 = SelectBloc(
        database: db,
        table: "quiz",
        where: " topic = 3",
        reactive: true);

    // listen for changes in the database
    _changefeed = db.changefeed.listen((change) {
      print("CHANGE IN THE DATABASE:");
      print("Change type: ${change.type}");
      print("Number of items impacted: ${change.value}");
      print("Query: ${change.query}");
      if (change.type == DatabaseChange.update) {
        print("${change.value} items updated");
      }
    });

    super.initState();
    _getData();
  }

  Future<void> _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString("email"));
   // print("getdata");





    this.setState(() async {
     // this.total_q1 = prefs.getInt("totalq1") ?? 4;
     // this.total_q2 = prefs.getInt("totalq2") ?? 3;
     // this.total_q3 = prefs.getInt("totalq3") ?? 3;

      this.start_q1 = prefs.getInt("startq1") ?? 0;
      this.start_q2 = prefs.getInt("startq2") ?? 0;
      this.start_q3 = prefs.getInt("startq3") ?? 0;
      try {
        List<Map<String, dynamic>> rows = await db.select(
          table: "paintinfo",
          where: "id  = 1",
        );

        this.doctor = rows[0]["doctor"];
      } catch (e) {
        rethrow;
      }


    });
  }

  @override
  void dispose() {
    _changefeed.cancel();
    bloc.dispose();
    blocQ1.dispose();
    blocQ2.dispose();
    blocQ3.dispose();
    super.dispose();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));

    _refreshController.loadComplete();
  }

  void checkQ1() {
    if (this.start_q1 == this.total_q1) {
    } else if (this.start_q1 > 0) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => QuizPage(current: start_q1))).then((value) {
        if (value) // if true and you have come back to your Settings screen
          _getData();
      });
    } else {
      Navigator.push(
              context, MaterialPageRoute(builder: (context) => QuizStart()))
          .then((value) {
        if (value) // if true and you have come back to your Settings screen
          _getData();
      });
    }
  }

  void checkQ2() {
    if (this.start_q2 == this.total_q2) {
    } else if (this.start_q2 > 0) {
      Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => QuizIn1Page(current: start_q2)))
          .then((value) {
        if (value) // if true and you have come back to your Settings screen
          _getData();
      });
    } else {
      Navigator.push(
              context, MaterialPageRoute(builder: (context) => QuizInStart()))
          .then((value) {
        if (value) // if true and you have come back to your Settings screen
          _getData();
      });
    }
  }

  void checkQ3() {
    if (this.start_q3 == this.total_q3) {
    } else if (this.start_q3 > 0) {
      Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => QuizIn2Page(current: start_q3)))
          .then((value) {
        if (value) // if true and you have come back to your Settings screen
          _getData();
      });
    } else {
      Navigator.push(
              context, MaterialPageRoute(builder: (context) => QuizIn2Start()))
          .then((value) {
        if (value) // if true and you have come back to your Settings screen
          _getData();
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return !databaseIsReady
        ? Scaffold(
            body: Center(child: const Text("The database is initializing ...")))
        : Scaffold(
            appBar: AppBar(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "วันที่นับพบแพทย์",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: HexColor("000000"),
                      fontFamily: "Kanit",
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                    ),
                  ),
                  InkWell(
                      onTap: () =>
                          /*Navigator.of(
        context,
        rootNavigator: true,
      ).pushReplacementNamed(AppointMentAddPage.routeName)*/
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AppointMentAddPage())),
                      // handle your onTap here
                      child: Container(
                        width: 45,
                        height: 45,
                        child: Align(
                          child: Image.asset(
                            "assets/images/group-2631.png",
                            fit: BoxFit.none,
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: const Color(0xffffffff),
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              offset: const Offset(0, 2),
                              blurRadius: 6,
                              color: const Color(0xff5e4589).withOpacity(0.3),
                            )
                          ],
                        ),
                      ))
                ],
              ),
              backgroundColor: HexColor("FFFFFF"),
              actions: <Widget>[],
            ),
            body: Container(
                color: const Color(0xFFFFFF),
                child: SmartRefresher(
                    enablePullDown: true,
                    enablePullUp: true,
                    header: WaterDropHeader(),
                    footer: CustomFooter(
                      builder: (BuildContext context, LoadStatus mode) {
                        Widget body;
                        if (mode == LoadStatus.idle) {
                        } else if (mode == LoadStatus.loading) {
                        } else if (mode == LoadStatus.failed) {
                        } else if (mode == LoadStatus.canLoading) {
                        } else {}
                        return Container(
                          height: 55.0,
                          child: Center(child: body),
                        );
                      },
                    ),
                    controller: _refreshController,
                    onRefresh: _onRefresh,
                    onLoading: _onLoading,
                    child: ListView(children: <Widget>[
                      new Container(
                          height: 170,
                          color: const Color(0xffedf4fa),
                          child: Container(
                            width: 84,
                            child: Align(
                              child: Image.asset(
                                "assets/images/group_app_33.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          )),
                      Container(
                          margin: EdgeInsets.only(top: 11),
                          child: Center(
                            child: StreamBuilder<List<Map>>(
                                stream: bloc.items,
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (snapshot.hasData &&
                                      snapshot.data != null) {
                                    if (snapshot.data != null) {
                                      // the select query has not found anything
                                      if (snapshot.data.length == 0) {
                                        return SizedBox(
                                          height: 170, // card height
                                          child: PageView.builder(
                                            itemCount: 1,
                                            controller: PageController(
                                                viewportFraction: 0.8),
                                            onPageChanged: (int index) =>
                                                setState(() => _index = index),
                                            itemBuilder: (_, i) {
                                              return Transform.scale(
                                                scale: i == _index ? 1 : 0.9,
                                                child: Card(
                                                    elevation: 0.0,
                                                    child:
                                                        noAppoinment(context)),
                                              );
                                            },
                                          ),
                                        );
                                      }
                                      return Container(
                                        height: 330,
                                        child: SizedBox(
                                          height: 180, // card height
                                          child: PageView.builder(
                                            itemCount: snapshot.data.length,
                                            controller: PageController(
                                                viewportFraction: 0.8),
                                            onPageChanged: (int index) =>
                                                setState(() => _index = index),
                                            itemBuilder:
                                                (BuildContext context, ix) {
                                              final dynamic item =
                                                  snapshot.data[ix];
                                              final note = "${item["note"]}";
                                              final date = "${item["date"]}";
                                              final month = "${item["month"]}";
                                              final year = "${item["year"]}";
                                              final hh = "${item["hour"]}";
                                              final mm = "${item["min"]}";

                                              final id = "${item["updated"]}";

                                              return Transform.scale(
                                                scale: ix == _index ? 1 : 0.9,
                                                child: Card(
                                                    elevation: 0.0,
                                                    child: appoinmentSlider(
                                                        context,
                                                        note,
                                                        date,
                                                        month,
                                                        year,
                                                        hh,
                                                        mm,
                                                        id)),
                                              );
                                            },
                                          ),
                                        ),
                                      );
                                    } else {
                                      return SizedBox(
                                        height: 170, // card height
                                        child: PageView.builder(
                                          itemCount: 1,
                                          controller: PageController(
                                              viewportFraction: 0.8),
                                          onPageChanged: (int index) =>
                                              setState(() => _index = index),
                                          itemBuilder: (_, i) {
                                            return Transform.scale(
                                              scale: i == _index ? 1 : 0.9,
                                              child: Card(
                                                  elevation: 0.0,
                                                  child: noAppoinment(context)),
                                            );
                                          },
                                        ),
                                      );
                                    }
                                  } else {
                                    return Container();
                                  }
                                }),
                          )),
                      Container(
                          height: 450,
                          margin: EdgeInsets.only(left: 22, right: 22, top: 10),
                          decoration: BoxDecoration(
                            color: AppColors.primaryBackground,
                            boxShadow: [
                              Shadows.secondaryShadow,
                            ],
                            borderRadius: Radii.k10pxRadius,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                /*Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(right: 7, bottom: 29),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            children: [
                                                 Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      width: 50,
                                                      height: 50,
                                                      margin: EdgeInsets.only(top: 5),
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                                        children: [ ]
                                                      )
                                                    )
                                                 ),

                                            ]
                                          )
                                        )
                                   )*/
                                Container(
                                  width: 294,
                                  height: 400,
                                  margin: EdgeInsets.only(left: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              right: 7, bottom: 29),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: [
                                              Align(
                                                alignment: Alignment.topLeft,
                                                child: Container(
                                                  width: 23,
                                                  height: 350,
                                                  margin:
                                                      EdgeInsets.only(top: 1),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .stretch,
                                                    children: [
                                                      Align(
                                                        alignment:
                                                            Alignment.topLeft,
                                                        child: Container(
                                                          width: 23,
                                                          height: 22,
                                                          child: Stack(
                                                            alignment: Alignment
                                                                .center,
                                                            children: [
                                                              Positioned(
                                                                left: 0,
                                                                right: 2,
                                                                child:
                                                                    Image.asset(
                                                                  "assets/images/suggest.png",
                                                                  fit: BoxFit
                                                                      .none,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment:
                                                            Alignment.topLeft,
                                                        child: Container(
                                                          width: 15,
                                                          height: 300,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Stack(
                                                            alignment: Alignment
                                                                .center,
                                                            children: [
                                                              Positioned(
                                                                left: 7,
                                                                top: 0,
                                                                child:
                                                                    Container(
                                                                  width: 1,
                                                                  height: 210,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: AppColors
                                                                        .primaryElement,
                                                                  ),
                                                                  child:
                                                                      Container(),
                                                                ),
                                                              ),
                                                              Align(
                                                                // อันแรก
                                                                alignment:
                                                                    Alignment
                                                                        .topLeft,
                                                                child:
                                                                    Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              65),
                                                                  width: 15,
                                                                  height: 15,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: AppColors
                                                                        .primaryElement,
                                                                    border: Border
                                                                        .fromBorderSide(
                                                                            Borders.primaryBorder),
                                                                    borderRadius:
                                                                        BorderRadius.all(
                                                                            Radius.circular(7.5)),
                                                                  ),
                                                                  child:
                                                                      Container(),
                                                                ),
                                                              ),
                                                              Align(
                                                                alignment:
                                                                    Alignment
                                                                        .topLeft,
                                                                child:
                                                                    Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              145),
                                                                  width: 15,
                                                                  height: 15,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: AppColors
                                                                        .primaryElement,
                                                                    border: Border
                                                                        .fromBorderSide(
                                                                            Borders.primaryBorder),
                                                                    borderRadius:
                                                                        BorderRadius.all(
                                                                            Radius.circular(7.5)),
                                                                  ),
                                                                  child:
                                                                      Container(),
                                                                ),
                                                              ),
                                                              Align(
                                                                alignment:
                                                                    Alignment
                                                                        .topLeft,
                                                                child:
                                                                    Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              215),
                                                                  width: 15,
                                                                  height: 15,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: AppColors
                                                                        .primaryElement,
                                                                    border: Border
                                                                        .fromBorderSide(
                                                                            Borders.primaryBorder),
                                                                    borderRadius:
                                                                        BorderRadius.all(
                                                                            Radius.circular(7.5)),
                                                                  ),
                                                                  child:
                                                                      Container(),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    Align(
                                                      alignment:
                                                          Alignment.topRight,
                                                      child: Container(
                                                        width: 255,
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "แบบสอบถามที่ต้องทำร่วมกับหมอในวันนี้",
                                                          textAlign:
                                                              TextAlign.left,
                                                          style: TextStyle(
                                                            color:
                                                                Color.fromARGB(
                                                                    255,
                                                                    69,
                                                                    79,
                                                                    99),
                                                            fontFamily: "Kanit",
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontSize: 18,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      child: StreamBuilder<List<Map>>(
                                                        stream: blocQ1.items,
                                                        builder: (context, snapshot) {
                                                          if (snapshot.hasData &&  snapshot.data != null && snapshot.data.length > 0) {

                                                            var item = snapshot.data;
                                                            //var q1 = item[0]["choice"];
                                                           // var s1 = item[0]["total"];

                                                            int q1 = int.parse(snapshot.data[0]["choice"].toString());
                                                            int s1 = int.parse(snapshot.data[0]["total"].toString());


                                                            return InkWell(
                                                                onTap: () => checkQ1(),
                                                                //  return new AppointMentAddPage(_onChanged);

                                                                // return new MemoEdit(_memoList[_currentIndex], _onChanged);
                                                                // MaterialPageRoute(builder: (context) => QuizStart())),
                                                                child: Align(
                                                                    alignment: Alignment
                                                                        .topRight,
                                                                    child: Column(
                                                                      children: <
                                                                          Widget>[
                                                                        Container(
                                                                          width: 255,
                                                                          margin: EdgeInsets
                                                                              .only(
                                                                              top:
                                                                              26),
                                                                          child: Text(
                                                                            "รายงานอาการลำไส้อับเสบเรื้อรังโดยผู่ป่วย\nPatient report symptoms",
                                                                            textAlign:
                                                                            TextAlign
                                                                                .left,
                                                                            style:
                                                                            TextStyle(
                                                                              color: Color.fromARGB(
                                                                                  255,
                                                                                  69,
                                                                                  79,
                                                                                  99),
                                                                              fontFamily:
                                                                              "Kanit",
                                                                              fontWeight:
                                                                              FontWeight
                                                                                  .w400,
                                                                              fontSize:
                                                                              12,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          width: 255,
                                                                          margin: EdgeInsets
                                                                              .only(
                                                                              top:
                                                                              1),
                                                                          child: Text(
                                                                            "$q1/$s1",
                                                                            textAlign:
                                                                            TextAlign
                                                                                .right,
                                                                            style:
                                                                            TextStyle(
                                                                              color: Color.fromARGB(
                                                                                  255,
                                                                                  69,
                                                                                  79,
                                                                                  99),
                                                                              fontFamily:
                                                                              "Kanit",
                                                                              fontSize:
                                                                              10,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          width: 255,
                                                                          child:
                                                                          Container(
                                                                            height: 4,
                                                                            margin: EdgeInsets
                                                                                .only(
                                                                                top:
                                                                                5),
                                                                            child:
                                                                            Stack(
                                                                              alignment:
                                                                              Alignment
                                                                                  .center,
                                                                              children: [
                                                                                StepProgressIndicator(
                                                                                  padding:
                                                                                  0,
                                                                                  totalSteps:
                                                                                  s1,
                                                                                  currentStep:
                                                                                  q1,
                                                                                  selectedColor:
                                                                                  HexColor("C4403E"),
                                                                                  unselectedColor:
                                                                                  HexColor("E2E8ED"),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    )));
                                                          }else{
                                                              return Container();
                                                          }

                                                        }
                                                      ),
                                                    ),
                                                      StreamBuilder<List<Map>>(
                                                          stream: blocQ2.items,
                                                          builder: (context, snapshot) {
                                                            if (snapshot.hasData &&  snapshot.data != null && snapshot.data.length > 0) {
                                                              var item = snapshot.data;
                                                              int q2 = int.parse(snapshot.data[0]["choice"].toString());
                                                              int s2 = int.parse(snapshot.data[0]["total"].toString());


                                                              return InkWell(
                                                                  onTap: () => checkQ2(),
                                                                  child: Align(
                                                                      alignment: Alignment
                                                                          .topRight,
                                                                      child: Column(
                                                                        children: <
                                                                            Widget>[
                                                                          Container(
                                                                            width: 255,
                                                                            margin: EdgeInsets
                                                                                .only(
                                                                                top:
                                                                                26),
                                                                            child: Text(
                                                                              "แบบสอบถามประเมินคุณภาพชีวิต EQ-5D-5L ",
                                                                              textAlign:
                                                                              TextAlign
                                                                                  .left,
                                                                              style:
                                                                              TextStyle(
                                                                                color: Color.fromARGB(
                                                                                    255,
                                                                                    69,
                                                                                    79,
                                                                                    99),
                                                                                fontFamily:
                                                                                "Kanit",
                                                                                fontWeight:
                                                                                FontWeight
                                                                                    .w400,
                                                                                fontSize:
                                                                                12,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 255,
                                                                            margin: EdgeInsets
                                                                                .only(
                                                                                top:
                                                                                1),
                                                                            child: Text(
                                                                              "$q2/$s2",
                                                                              textAlign:
                                                                              TextAlign
                                                                                  .right,
                                                                              style:
                                                                              TextStyle(
                                                                                color: Color.fromARGB(
                                                                                    255,
                                                                                    69,
                                                                                    79,
                                                                                    99),
                                                                                fontFamily:
                                                                                "Kanit",
                                                                                fontSize:
                                                                                10,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 255,
                                                                            child:
                                                                            Container(
                                                                              height: 4,
                                                                              margin: EdgeInsets
                                                                                  .only(
                                                                                  top:
                                                                                  5),
                                                                              child:
                                                                              Stack(
                                                                                alignment:
                                                                                Alignment
                                                                                    .center,
                                                                                children: [
                                                                                  StepProgressIndicator(
                                                                                    padding:
                                                                                    0,
                                                                                    totalSteps:
                                                                                    s2,
                                                                                    currentStep:
                                                                                    q2,
                                                                                    selectedColor:
                                                                                    HexColor("C4403E"),
                                                                                    unselectedColor:
                                                                                    HexColor("E2E8ED"),
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          )
                                                                        ],
                                                                      )));

                                                            }else{
                                                              return Container();

                                                            }
                                                      }
                                                    ),
                                                    StreamBuilder<List<Map>>(
                                                        stream: blocQ3.items,
                                                      builder: (context, snapshot) {
                                                        if (snapshot.hasData &&  snapshot.data != null && snapshot.data.length > 0) {
                                                          var item = snapshot.data;
                                                          int q3 = int.parse(snapshot.data[0]["choice"].toString());
                                                          int s3 = int.parse(snapshot.data[0]["total"].toString());

                                                          return InkWell(
                                                              onTap: () => checkQ3(),
                                                              child: Align(
                                                                  alignment: Alignment
                                                                      .topRight,
                                                                  child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      Container(
                                                                        width: 255,
                                                                        margin: EdgeInsets
                                                                            .only(
                                                                            top:
                                                                            26),
                                                                        child: Text(
                                                                          "แบบสอบถามประเมินคุณภาพชีวิตผู้ป่วย โรคลำไส้อับเสบเรื้อรัง HANDS",
                                                                          textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                          style:
                                                                          TextStyle(
                                                                            color: Color.fromARGB(
                                                                                255,
                                                                                69,
                                                                                79,
                                                                                99),
                                                                            fontFamily:
                                                                            "Kanit",
                                                                            fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                            fontSize:
                                                                            12,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        width: 255,
                                                                        margin: EdgeInsets
                                                                            .only(
                                                                            top:
                                                                            1),
                                                                        child: Text(
                                                                          "$q3/$s3",
                                                                          textAlign:
                                                                          TextAlign
                                                                              .right,
                                                                          style:
                                                                          TextStyle(
                                                                            color: Color.fromARGB(
                                                                                255,
                                                                                69,
                                                                                79,
                                                                                99),
                                                                            fontFamily:
                                                                            "Kanit",
                                                                            fontSize:
                                                                            10,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        width: 255,
                                                                        child:
                                                                        Container(
                                                                          height: 4,
                                                                          margin: EdgeInsets
                                                                              .only(
                                                                              top:
                                                                              5),
                                                                          child:
                                                                          Stack(
                                                                            alignment:
                                                                            Alignment
                                                                                .center,
                                                                            children: [
                                                                              StepProgressIndicator(
                                                                                padding:
                                                                                0,
                                                                                totalSteps:
                                                                                s3,
                                                                                currentStep:
                                                                                q3,
                                                                                selectedColor:
                                                                                HexColor("C4403E"),
                                                                                unselectedColor:
                                                                                HexColor("E2E8ED"),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      )
                                                                    ],
                                                                  )));
                                                        }else{
                                                          return Container();

                                                        }

                                                      }
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: 1,
                                        margin: EdgeInsets.only(bottom: 5),
                                        decoration: BoxDecoration(
                                          color: AppColors.primaryElement,
                                        ),
                                        child: Container(),
                                      ),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Container(
                                          margin: EdgeInsets.only(left: 69),
                                          child: Text(
                                            "คุณหมอ $doctor",
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              color: AppColors.primaryText,
                                              fontFamily: "Kanit",
                                              fontWeight: FontWeight.w400,
                                              fontSize: 16,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ])),
                    ]))));
  }
}

Widget noteSlider() {
  return Stack(alignment: Alignment.bottomCenter, children: [
    Container(
        width: 250,
        height: 128,
        decoration: BoxDecoration(
          boxShadow: [
            Shadows.secondaryShadow,
          ],
          borderRadius: Radii.k10pxRadius,
        ),
        child: Container())
  ]);
}

Widget genAppSlider() {
  return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Row(
        children: <Widget>[
          Container(
            child: new Stack(
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(
                        left: 60.0, right: 20.0, top: 110.0),
                    child: noteSlider()),
                noteSlider()
              ],
            ),
          )
        ],
      ));
}

Widget appoinmentSlider(BuildContext context, String note, String date,
    String month, String year, String hh, String mm, String id) {
  var savedDateString =
      "${year}-${month.padLeft(2, '0')}-${date.padLeft(2, '0')} ${hh.padLeft(2, '0')}:${mm.padLeft(2, '0')}:00";
  DateTime tempDate = DateTime.parse(savedDateString);

  var textweekday = "MONDAY";
  if (tempDate.weekday == 1) {
    textweekday = "MONDAY";
  } else if (tempDate.weekday == 2) {
    textweekday = "TUESDAY";
  } else if (tempDate.weekday == 3) {
    textweekday = "WEDNESDAY";
  } else if (tempDate.weekday == 4) {
    textweekday = "THURSDAY";
  } else if (tempDate.weekday == 5) {
    textweekday = "FRIDAY";
  } else if (tempDate.weekday == 6) {
    textweekday = "SATURDAY";
  } else if (tempDate.weekday == 7) {
    textweekday = "SUNDAY";
  } else {
    textweekday = "MONDAY";
  }
  var hhreal = int.parse(hh);
  var textam = "AM";
  if (int.parse(hh) > 12) {
    hhreal = hhreal - 12;
    textam = "PM";
  }
  var hhpad = "${hhreal}".padLeft(2, '0');
  var mmpad = "${mm}".padLeft(2, '0');

  var dateUtility = new DateUtil();
  String monthName = dateUtility.month(int.parse(month));
  return InkWell(
      onTap: () => Navigator.push(context,
          MaterialPageRoute(builder: (context) => AppointMentEditPage(id: id))),
      child: Column(
        children: <Widget>[
          Container(
            width: 310,
            height: 186,
            decoration: BoxDecoration(
              gradient: const LinearGradient(
                begin: Alignment(0.29, 0.08),
                end: Alignment(0.75, 1.42),
                colors: [Color(0xff8a6cbc), Color(0xfff37b7d)],
              ),
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  offset: const Offset(0, 3),
                  blurRadius: 6,
                  color: const Color(0xff8a6cbc).withOpacity(0.4),
                )
              ],
            ),
            child: IntrinsicHeight(
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Column(children: [
                        Container(
                            height: 110.0,
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    margin: EdgeInsets.all(10),
                                    width: 44,
                                    height: 44,
                                    child: Image.asset(
                                      "assets/images/group-1936_1.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(
                                      " วันที่หมอนัด",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: AppColors.secondaryText,
                                        fontFamily: "Kanit",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                        Container(
                          height: 50.0,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "  ${textweekday}",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: AppColors.secondaryText,
                                        fontFamily: "Kanit",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                      ),
                                    )),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    " ${hhpad}.${mmpad} ${textam} ",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: AppColors.secondaryText,
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 20,
                                    ),
                                  ),
                                ),
                              ]),
                        ),
                      ]),
                    ),
                    Expanded(
                      child: Column(children: [
                        Container(
                            height: 110.0,
                            margin: EdgeInsets.only(right: 10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "${date}",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    color: AppColors.secondaryText,
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 90,
                                  ),
                                )
                              ],
                            )),
                        Container(
                            height: 50.0,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                      alignment: Alignment.topRight,
                                      child: Text(
                                        "${monthName}  ",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: AppColors.secondaryText,
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12,
                                        ),
                                      )),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Text(
                                      "${year} ",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: AppColors.secondaryText,
                                        fontFamily: "Kanit",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ),
                                ])),
                      ]),
                    )
                  ]),
            ),
          ),
          Container(
            child: Container(
              width: 260,
              height: 110,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16.0),
                    bottomRight: Radius.circular(16.0)),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(0, 3),
                    blurRadius: 10,
                    color: const Color(0xff8a6cbc).withOpacity(0.3),
                  )
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 16, left: 16),
                    width: 250,
                    child: Text(
                      'บันทึกช่วยจำ',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Kanit',
                        fontSize: 14,
                        color: Color(0xff626776),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 3, left: 16),
                    width: 250,
                    child: Text(
                      '$note',
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 14,
                        color: Color(0xff626776),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ));
}

Widget genSlider1(BuildContext context) {
  return Stack(children: <Widget>[
    Container(
      height: 200,
      child: Container(
        color: HexColor("FF0000"),
      ),
    )
  ]);
}

Widget noAppoinment(BuildContext context) {
  return Container(
    width: 310,
    height: 166,
    decoration: BoxDecoration(
      gradient: const LinearGradient(
        begin: Alignment(0.29, 0.08),
        end: Alignment(0.75, 1.42),
        colors: [Color(0xff8a6cbc), Color(0xfff37b7d)],
      ),
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          offset: const Offset(0, 3),
          blurRadius: 6,
          color: const Color(0xff8a6cbc).withOpacity(0.4),
        )
      ],
    ),
    padding: EdgeInsets.only(top: 20),
    child: Column(
      children: <Widget>[
        Container(
          width: 45,
          height: 45,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () =>
                    /*Navigator.of(
        context,
        rootNavigator: true,
      ).pushReplacementNamed(AppointMentAddPage.routeName)*/
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AppointMentAddPage())),
                child: Image.asset(
                  "assets/images/group-2631.png",
                  fit: BoxFit.none,
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 5),
          child: Text(
            'เพิ่มนัดหมาย',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Kanit',
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: Color(0xffffffff),
            ),
          ),
        ),
        const Text(
          'ไม่พลาดการนัดหมาย\nบันทึกช่วงเวลา',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Kanit',
            fontSize: 16,
            fontWeight: FontWeight.w300,
            color: Color(0xffffffff),
          ),
        )
      ],
    ),
  );
}

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class TitleBloc implements Bloc {

  final String title;

  TitleBloc(this.title);

  @override
  void dispose() {
    print('disposed: bloc $title');
  }
}