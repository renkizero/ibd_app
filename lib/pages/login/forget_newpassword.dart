import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:ibd_application/pages/info/info1_page.dart';
import 'package:ibd_application/pages/login/forget_passwordemail.dart';
import 'package:ibd_application/pages/login/login_page.dart';
import 'package:ibd_application/pages/register/register_page1.dart';
import 'package:ibd_application/pages/register/register_page3.dart';
import 'package:ibd_application/util.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:ibd_application/values/values.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ForgetPasswordNewPassPage extends StatefulWidget {
  @override
  _ForgetPasswordNewPassPageState createState() => _ForgetPasswordNewPassPageState();
}

class _ForgetPasswordNewPassPageState extends State<ForgetPasswordNewPassPage> {
  final GlobalKey<FormBuilderState> _fbKey1 = GlobalKey<FormBuilderState>();

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var email;
  var password;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getData();
  }

  _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString("email"));

    this.setState(() {
      this.email = prefs.getString("email");
      this.password = prefs.getString("password");
      // print(this.email);
    });
  }

  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
      Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: BackButton(color: Colors.black),
          title: const _Title(),
          elevation: 0.0,
        ),
        body: Container(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: 319,
                    margin: EdgeInsets.only(top: 1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 4),
                            child: Text(
                              "ตั้งค่า\nรหัสผ่านใหม่",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: HexColor("4C5264"),
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 40,
                              ),
                            ),
                          ),
                        ), Padding(
                          padding: const EdgeInsets.only(top:12.0),
                          child: Text(
                            'กรุณาใส่ใหม่ของคุณรหัสผ่าน',

                            style: TextStyle(
                              fontFamily: 'Kanit',
                              fontSize: 16,

                              color: Color(0xff4c5264).withOpacity(0.5),


                            ),
                          ),
                        ),
                        FormBuilder(
                            key: _fbKey1,
                            autovalidate: false,
                            child: Padding(
                              padding: const EdgeInsets.only(top:30.0),
                              child: Column(
                                  children: <Widget>[
                                  FormBuilderTextField(
                                    attribute: "password",
                                    maxLines: 1,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                        labelText: "รหัสผ่าน",
                                        labelStyle: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [FormBuilderValidators.required()],
                                  ),FormBuilderTextField(
                                      attribute: "password",
                                      maxLines: 1,
                                      obscureText: true,
                                      decoration: InputDecoration(
                                          labelText: "ยืนยันรหัสผ่าน",
                                          labelStyle: TextStyle(
                                            color: HexColor("BCC5D3"),
                                            fontFamily: "Kanit",
                                            fontSize: 15,
                                          )),
                                      validators: [FormBuilderValidators.required()],
                                    )
                                  ]),
                            )),
                        InkWell(
                            onTap: () async {
                              SharedPreferences prefs =
                              await SharedPreferences.getInstance();

                              /*prefs.setInt("register", 1);

                          Route route = MaterialPageRoute(
                              builder: (context) => HomePage.wrapped());
                          Navigator.pushAndRemoveUntil(
                              context, route, (route) => false);*/
                              if (_fbKey1.currentState.saveAndValidate()) {

                                Route route = MaterialPageRoute(
                                    builder: (context) => RegisterFirstPage());
                                /*  Navigator.push(
                                    context, route);*/

                                Navigator.of(context).pushAndRemoveUntil(route, (route) => false);

                              }
                              // val.forEach((k, v) => print('${k}: ${v}'));

                              // print(prefs.getString("email_input"));
                            },
                            child: Container(
                              height: 50,
                              margin: EdgeInsets.only(top: 30, left: 4, bottom: 8),
                              decoration: BoxDecoration(
                                color: HexColor("F8B6B8"),
                                borderRadius: Radii.k10pxRadius,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "ตกลง",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: AppColors.accentText,
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ))
                      ],
                    ),
                  ),
                ),
              ],
            )));
  }

  void _popupDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Alert'),
            content: Text('อีเมล์หรือรหัสผ่าน ของท่านไม่ถูกต้อง'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('ตกลง')),
            ],
          );
        });
  }
}

class _Title extends StatelessWidget {
  const _Title({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[],
    );
  }
}
