class Constants{
  static const String KANIT = "Kanit";
  static const String OPEN_SANS = "Kanit";
  static const String SKIP = "ข้าม";
  static const String NEXT = "Next";
  static const String SLIDER_HEADING_1 = "ให้เราช่วยดูแลสุขภาพของท่านให้ปลอดภัยจากโรคร้าย";
  static const String SLIDER_HEADING_2 = "ไม่พลาดการนัดหมายบันทึกช่วงเวลา";
  static const String SLIDER_HEADING_3 = "บันทึกข้อมูลสุขภาพด้วยตัวเองแบบง่ายๆ";
  static const String SLIDER_DESC_1 = "ให้เราได้เป็นส่วนหนึ่ง ของการดูแลสุขภาพคุณให้หายดีจากอาการป่วยที่รักษาได้";
  static const String SLIDER_DESC_2 = "เพื่อป้องกันการผิดพลาดการพบแพทย์ในวันนัดหมาย เราช่วยแจ้งเตือนและดูแลคุณได้ตลอดเวลา";
  static const String SLIDER_DESC_3 = "ช่วยเก็บบันทึกสุขภาพคุณให้สุขภาพคุณปลอดภัยดีอยู่ตลอดเวลาได้ เมื่อพบหมอ";
}