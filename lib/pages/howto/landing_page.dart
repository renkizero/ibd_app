import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/pages/constants/constants.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:ibd_application/pages/howto/slider_layout_view.dart';
import 'package:ibd_application/custom_font.dart';
import 'package:ibd_application/pages/login/login_page.dart';
import 'package:jiffy/jiffy.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LandingPage extends StatefulWidget {

  const LandingPage({Key key}) : super(key: key);

  static const routeName = '/landingpage';


  @override
  State<StatefulWidget> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  


  @override
  Widget build(BuildContext context) {
    //print(Jiffy().month.toString().padLeft(2, '0'));
    return Scaffold(
      backgroundColor: Colors.white,
      body: onBordingBody(),
    );
  }

  Widget onBordingBody() => Container(
        child: SliderLayoutView(),
      );
}