import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/pages/constants/constants.dart';
import 'package:ibd_application/pages/howto/slider.dart';
import 'package:ibd_application/pages/register/register_page1.dart';

class SlideItem extends StatelessWidget {
  int index;
  var currentPage;
  SlideItem(this.index, this.currentPage);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.width ,
          width: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(sliderArrayList[index].sliderImageUrl))

          ),
        ),
        Padding(
            padding: EdgeInsets.only(right: 20, left: 20),
            child: Text(
              sliderArrayList[index].sliderHeading,
              style: TextStyle(
                fontFamily: Constants.KANIT,
                fontSize: 30,
                color: Color(0xff5e4589),
              ),
            )),
        Padding(
          padding: EdgeInsets.only(top: 10, right: 20, left: 20, bottom: 30),
          child: Text(sliderArrayList[index].sliderSubHeading,
              style: TextStyle(
                fontFamily: Constants.KANIT,
                fontSize: 16,
                color: Color(0xff5e4589),
              )),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.0),
            child: InkWell(
                onTap: () {
                  if (this.index == 2) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterFirstPage()));
                  } else {}
                },
                child: (InkWell(
                  onTap: () {
                    print("test");
                    this.currentPage = this.currentPage + 1;
                    // this.currentPage = this.currentPage + 1;
                    /*Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterFirstPage()));*/
                  },
                  child: Container(
                      width: 315,
                      height: 50,
                      decoration: BoxDecoration(
                        color: Color(0xfff8b6b8),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            this.index == 2 ? 'เริ่มสมัครสมาชิก' : 'ถัดไป',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: Constants.KANIT,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff5e4589),
                            ),
                          )
                        ],
                      )),
                ))),
          ),
        )
      ],
    );
  }
}
