import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/constants.dart';
import 'package:ibd_application/pages/howto/slider.dart';
import 'package:ibd_application/pages/howto/slide_dots.dart';
import 'package:ibd_application/pages/howto/slide_item.dart';
import 'package:ibd_application/pages/register/register_page1.dart';

class SliderLayoutView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SliderLayoutViewState();
}

class _SliderLayoutViewState extends State<SliderLayoutView> {
  int _currentPage = 0;
  final PageController _pageController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < 2) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void changePageViewPostion(int whichPage) {
    if (_pageController != null) {
      whichPage = whichPage + 1; // because position will start from 0
      double jumpPosition = MediaQuery
          .of(context)
          .size
          .width / 2;
      double orgPosition = MediaQuery
          .of(context)
          .size
          .width / 2;
      for (int i = 0; i < 3; i++) {
        _pageController.jumpTo(jumpPosition);
        if (i == whichPage) {
          break;
        }
        jumpPosition = jumpPosition + orgPosition;
      }
    }
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) => topSliderLayout();

  Widget topSliderLayout() => Container(
        child: Padding(
            padding: EdgeInsets.only(top: 16.0),
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: <Widget>[
                PageView.builder(
                  scrollDirection: Axis.horizontal,
                  controller: _pageController,
                  onPageChanged: _onPageChanged,
                  itemCount: sliderArrayList.length,
                  itemBuilder: (ctx, i) => new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.width ,
                        width: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(sliderArrayList[i].sliderImageUrl))

                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(top:20,right: 40, left: 40),
                          child: Text(
                            sliderArrayList[i].sliderHeading,
                            style: TextStyle(
                              fontFamily: Constants.KANIT,
                              fontSize: 30,
                              color: Color(0xff5e4589),
                            ),
                          )),
                      Padding(
                        padding: EdgeInsets.only(top: 10, right: 40, left: 40, bottom: 30),
                        child: Text(sliderArrayList[i].sliderSubHeading,
                            style: TextStyle(
                              fontFamily: Constants.KANIT,
                              fontSize: 16,
                              color: Color(0xff5e4589),
                            )),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40.0),
                          child: (InkWell(
                            onTap: () {
                              //
                              var cp = 1;

                              if(i == 0){
                                cp = 1;
                              }else if(i == 1){
                                cp = 2;
                              }else{
                                cp = 3;
                              }
                              print(cp);
                              if(cp>2){
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => RegisterFirstPage()));
                              }else{
                                _pageController.jumpToPage(cp);
                              }
                               // for regular jump

                              //_currentPage = _currentPage + 1;
                              // this.currentPage = this.currentPage + 1;
                              /**/
                            },
                            child: Container(
                                width: 315,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xfff8b6b8),
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      i == 2 ? 'เริ่มสมัครสมาชิก' : 'ถัดไป',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: Constants.KANIT,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff5e4589),
                                      ),
                                    )
                                  ],
                                )),
                          )),
                        ),
                      )
                    ],
                  ),
                ),
                Stack(
                  alignment: AlignmentDirectional.topStart,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Padding(
                        padding: EdgeInsets.only(right: 45.0, bottom: 75.0),
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          RegisterFirstPage()));
                            },
                            child: Text(
                              Constants.SKIP,
                              style: TextStyle(
                                color: HexColor("#8A6CBC"),
                                fontFamily: Constants.OPEN_SANS,
                                fontSize: 18.0,
                              ),
                            )),
                      ),
                    ),
                    /*Align(
                      alignment: Alignment.bottomLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 15.0, bottom: 15.0),
                        child: Text(
                          Constants.SKIP,
                          style: TextStyle(
                            fontFamily: Constants.OPEN_SANS,
                            fontWeight: FontWeight.w600,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ),*/

                    Container(
                      alignment: AlignmentDirectional.bottomCenter,
                      margin: EdgeInsets.only(bottom: 80.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          for (int i = 0; i < sliderArrayList.length; i++)
                            if (i == _currentPage)
                              SlideDots(true)
                            else
                              SlideDots(false)
                        ],
                      ),
                    ),
                  ],
                )
              ],
            )),
      );
}
