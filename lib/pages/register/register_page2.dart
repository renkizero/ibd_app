import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/pages/register/register_page3.dart';
import 'package:ibd_application/util.dart';
import 'package:ibd_application/values/values.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:libphonenumber/libphonenumber.dart';

class RegisterInputPage extends StatefulWidget {
  const RegisterInputPage({Key key}) : super(key: key);
  static const routeName = '/registerinputpage';

  @override
  _RegisterInputPageState createState() => _RegisterInputPageState();
}

class _RegisterInputPageState extends State<RegisterInputPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  bool pristine = true;

  void activate() {
    setState(() {
      pristine = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.black),
        title: const _Title(),
        elevation: 0.0,
      ),
      body: Container(
          child: Container(
              child: Padding(
                  padding: EdgeInsets.only(top: 1.0),
                  child: ListView(children: <Widget>[
                    Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 30, top: 1),
                            child: Text(
                              "สมัครสมาชิก",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: HexColor("4C5264"),
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 40,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 30, top: 17),
                            child: Text(
                              "ข้อมูลส่วนตัว",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: HexColor("4C5264"),
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 30, right: 30),
                            child: FormBuilder(
                                key: _fbKey,
                                autovalidate: !pristine,
                                initialValue: {
                                  'date': DateTime.now(),
                                  'accept_terms': false,
                                },
                                child: Column(children: <Widget>[
                                  FormBuilderDropdown(
                                    attribute: "gender",
                                    decoration: InputDecoration(labelText: "" , labelStyle: TextStyle(
                                      color: HexColor("BCC5D3"),
                                      fontFamily: "Kanit",
                                      fontSize: 22,
                                    )),
                                    // initialValue: 'Male',
                                    hint: Text(
                                      'คำนำหน้า',
                                      style: TextStyle(
                                        color: HexColor("BCC5D3"),
                                        fontFamily: "Kanit",
                                        fontSize: 15,
                                      ),
                                    ),
                                    validators: [
                                      FormBuilderValidators.required()
                                    ],
                                    items: ['นาย', 'นางสาว', 'นาง']
                                        .map((gender) => DropdownMenuItem(
                                            value: gender,
                                            child: Text("$gender")))
                                        .toList(),
                                  ),
                                  FormBuilderTextField(
                                    attribute: "firstname",
                                    maxLines: 1,
                                    decoration: InputDecoration(
                                        labelText: "ชื่อ",
                                        labelStyle: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [
                                      FormBuilderValidators.max(70),
                                      FormBuilderValidators.required()
                                    ],
                                  ),
                                  FormBuilderTextField(
                                    attribute: "lastname",
                                    maxLines: 1,
                                    decoration: InputDecoration(
                                        labelText: "นามสกุล",
                                        labelStyle: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [
                                      FormBuilderValidators.max(70),
                                      FormBuilderValidators.required()
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 31),
                                      child: Text(
                                        "ข้อมูลในการเข้าร่วม",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: HexColor("4C5264"),
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                  FormBuilderTextField(
                                    attribute: "email",
                                    maxLines: 1,
                                    keyboardType: TextInputType.emailAddress,
                                    decoration: InputDecoration(
                                        labelText: "อีเมล์",
                                        labelStyle: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [
                                      FormBuilderValidators.email(),
                                      FormBuilderValidators.required()
                                    ],
                                  ),
                                  FormBuilderTextField(
                                    attribute: "phone",
                                    maxLength: 10,
                                    maxLines: 1,
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                        labelText: "เบอร์โทรศัพท์",
                                        labelStyle: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [
                                      FormBuilderValidators.numeric(
                                          errorText:
                                              "โทรศัทพ์ความยาวไม่เกิน 10 ตัว"),
                                      FormBuilderValidators.numeric(),
                                      FormBuilderValidators.maxLength(10),
                                      FormBuilderValidators.required()
                                    ],
                                  ),
                                  FormBuilderTextField(
                                    attribute: "password",
                                    maxLines: 1,
                                    maxLength: 8,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                        labelText: "รหัสผ่าน",
                                        labelStyle: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [
                                      FormBuilderValidators.maxLength(8),
                                      FormBuilderValidators.required()
                                    ],
                                  ),
                                  FormBuilderTextField(
                                    attribute: "password",
                                    maxLines: 1,
                                    maxLength: 8,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                        labelText: "ยืนยันรหัสผ่าน",
                                        labelStyle: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [
                                      FormBuilderValidators.maxLength(8),
                                      FormBuilderValidators.required()
                                    ],
                                  ),
                                  FormBuilderCheckbox(
                                    attribute: 'accept_terms',
                                    label: Text("ยินยอมรับเงื่อนไขและข้อตกลง",
                                        style: TextStyle(
                                          color: HexColor("BCC5D3"),
                                          fontFamily: "Kanit",
                                          fontSize: 15,
                                        )),
                                    validators: [
                                      FormBuilderValidators.requiredTrue(
                                        errorText:
                                            "กรุณากด ยินยอมรับเงื่อนไขและข้อตกลง",
                                      ),
                                    ],
                                  ),
                                ])))
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                            width: 315,
                            height: 50,
                            margin: EdgeInsets.only(bottom: 77),
                            decoration: BoxDecoration(
                              color: HexColor("F8B6B8"),
                              borderRadius: Radii.k10pxRadius,
                            ),
                            child: InkWell(
                              onTap: () async {
                                setState(() {
                                  pristine = true;
                                });

                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                if (_fbKey.currentState.saveAndValidate()) {
                                  /*Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RegisterFininshPage()));*/
                                /*  print(_fbKey.currentState.value);
                                  var val = _fbKey.currentState.value;
                                  val.forEach((k, v) =>
                                      prefs.setString('${k}', '${v}'));*/

                                  var val = _fbKey.currentState.value;

                                  print(val);
                                  var _firstname =  val["firstname"].toString();
                                  var _lastname =  val["lastname"].toString();
                                  var _email =  val["email"].toString();
                                  var _phone =  val["phone"].toString();
                                  var _password =  val["password"].toString();
                                  var _gender = "1";
                                  if(val["gender"] == "นาย"){
                                    _gender = "1";
                                  }else if(val["gender"] == "นางสาว"){
                                    _gender = "2";
                                  }else if(val["gender"] == "นาง"){
                                    _gender = "3";
                                  }

                                  saveProfile(_gender,_firstname,_lastname,_email,_phone,_password);
                                  prefs.setString('phone', _phone);
                                  prefs.setString('email', _email);
                                  prefs.setString('password', _password);

                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RegisterFininshPage()));
                                }
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "สมัครสมาชิก",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: AppColors.accentText,
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      ),
                    )
                  ])))),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[],
    );
  }
}
