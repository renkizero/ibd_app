import 'package:flutter/material.dart';
import 'package:ibd_application/pages/constants/constants.dart';
import 'package:ibd_application/pages/login/login_page.dart';
import 'package:ibd_application/pages/register/register_page2.dart';
import 'package:ibd_application/util.dart';

class RegisterFirstPage extends StatefulWidget {

   const RegisterFirstPage({Key key}) : super(key: key);

  static const routeName = '/registerfirstpage';

  @override
  _RegisterFirstPageState createState() => _RegisterFirstPageState();
}

class _RegisterFirstPageState extends State<RegisterFirstPage> {


  @override
  Widget build(BuildContext context) {


        return Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            child: Padding(
                padding: EdgeInsets.only(top:1.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.width  ,
                        width: MediaQuery.of(context).size.height ,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/Group_2484.png"),
                            )
                        )
                      ) , 
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(children:<Widget>[ Text(
          'ให้เราดูแลสุขภาพคุณ',
          style: TextStyle(
            fontFamily: "Kanit",
            fontSize: 35,
            color: Color(0xff5e4589),
          ),
        ), Padding(
        padding: EdgeInsets.only(top:10),
        child:Text(
          'ให้เราได้เป็นส่วนหนึ่ง ของการดูแลสุขภาพคุณ\nให้หายดีจากอาการป่วยที่รักษาได้\n',
          style: TextStyle(
            fontFamily: "Kanit",
            fontSize: 16,
            color: Color(0xff5e4589),

          ),
        )
        ),
        Container(
          width: 315,
           height: 50,
           decoration: BoxDecoration(
            color: const Color(0xfff8b6b8),
            borderRadius: BorderRadius.circular(6),
             ),
           child:
           InkWell(
                onTap: (){
                  Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => RegisterInputPage()));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                   Text(
                  'สมัครสมาชิก',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: Constants.KANIT,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff5e4589),
                  ),
                )],
                )
           ),
        ) ,
     Padding(
        padding: EdgeInsets.only(top:10),
        child:

         InkWell(
                        onTap: (){
                         /* Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => LoginPage()));*/
                                    Route route = MaterialPageRoute(builder: (context) => LoginPage());
                          Navigator.push(context, route);

                        },
                    child:
        Container(
          width: 315,
           height: 50,
           decoration: BoxDecoration(
            color: const Color(0xff8a6cbc),

            borderRadius: BorderRadius.circular(6),

             ),
           child:
           Column(
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
                 Text(
                  'เข้าสู่ระบบ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: Constants.KANIT,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: HexColor("FFFFFF"),
                  ),
                ),
         ],
       ),
    )
         )


 )

    ],),
                      )
                ]
                      )
        )
      ),
    );
  }
}