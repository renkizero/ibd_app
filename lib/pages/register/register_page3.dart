import 'package:flutter/material.dart';
import 'package:ibd_application/pages/login/login_page.dart';
import 'package:ibd_application/pages/register/register_page2.dart';
import 'package:ibd_application/util.dart';
import 'package:ibd_application/values/values.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterFininshPage extends StatefulWidget {
  const RegisterFininshPage({Key key}) : super(key: key);

  static const routeName = '/registerfinishpage';

  @override
  _RegisterFininshPageState createState() => _RegisterFininshPageState();
}

class _RegisterFininshPageState extends State<RegisterFininshPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          child: Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        height: MediaQuery.of(context).size.width ,
                        width: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                          image: AssetImage("assets/images/Group_2450.png"),
                        ))),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 27),
                            child: Text(
                              "ยืนยันตัวตน\nของท่าน",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: HexColor("4C5264"),
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 40,
                                height: 1,
                              ),
                            ),
                          ), Container(
                            margin: EdgeInsets.only(left: 27,top: 10),
                            child: Text(
                              'ระบบได้ทำการส่งลิ้งไปที่อีเมลท่าน\nกรุณาตรวจสอบอีเมลของท่าน \nเพื่อทำการยืนยันตัวตน',

                              style: TextStyle(
                                fontFamily: 'Kanit',
                                fontSize: 16,

                                color: Color(0xff4c5264).withOpacity(0.5),


                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                                width: 315,
                                height: 50,
                                margin: EdgeInsets.only(bottom: 77, top: 22),
                                decoration: BoxDecoration(
                                  color: HexColor("F8B6B8"),
                                  borderRadius: Radii.k10pxRadius,
                                ),
                                child: InkWell(
                                  onTap: () async {
                                    SharedPreferences prefs =
                                        await SharedPreferences.getInstance();

                                    prefs.setInt("register", 1);
                                    /* Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => LoginPage()));*/
                                    Route route = MaterialPageRoute(
                                        builder: (context) => LoginPage());
                                    Navigator.pushReplacement(context, route);
                                  },
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "เปิดอีเมล์",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: AppColors.accentText,
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                        ])
                  ]))),
    );
  }
}
