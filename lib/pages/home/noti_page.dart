import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/listview.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:ibd_application/pages/memo/memo_edit.dart';
import 'package:ibd_application/pages/memo/memo_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ibd_application/database.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:timeago/timeago.dart';


class NotiPage extends StatefulWidget {
   const NotiPage({Key key}) : super(key: key);
   static const routeName = '/memo';

  @override
  _NotiPageState createState() => _NotiPageState();
}

class _NotiPageState extends State<NotiPage> {

  List<Map<String, dynamic>> _rows;
  var _memoList = new List<String>();
  var _currentIndex = -1;
  bool _loading = true;
  final _biggerFont = const TextStyle(fontSize: 18.0);
  bool databaseIsReady = false;
 var _ready = false;

 bool hasData = false;


 @override
  void initState() {
  
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
     
    return new Scaffold(
       resizeToAvoidBottomPadding: false ,
      appBar: new AppBar(
        leading: BackButton(
            color: Colors.black
        ),
          backgroundColor: HexColor("FFFFFF"),
        title: const Text(
      'การแจ้งเตือน',
      textAlign: TextAlign.center,
      style: TextStyle(
      fontFamily: 'Kanit',
      fontSize: 20,
      
      color: Color(0xff000000),
      
      
      )
    )
    ),
      body: 
                  Container(
                    width: MediaQuery.of(context).size.width,
                      height: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(top:10,left:4,right:4),
                              child:  
                              Container (
                          padding:  EdgeInsets.only(top:10,left:20,right:20),
                          width: MediaQuery.of(context).size.width*0.8,
                          child: Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: <Widget>[
                               Text(
                                "วันที่หมอนัดหมาย",
                                
                                style: TextStyle(
                                fontFamily: 'Kanit',
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff4c5264),
                                
                              ),
                              ), Text(

                                  "28 JANUARY 2020",
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                  fontFamily: 'Kanit',
                                  fontSize: 14,
                                  
                                  color: Color(0xff4c5264),
                                  
                                  
                                ),
                                ), Text(
                                      "SUNDAY 9.00 AM",
                                      
                                      style: TextStyle(
                                      fontFamily: 'Kanit',
                                      fontSize: 12,
                                      
                                      color: Color(0xffbcc5d3),
                                      
                                      
                                    ),
                                    )
                           ],
                         )
                              )
                            ), Container(
                                      width: 30,
                                      height: 150,
                                        child:  Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                          Image.asset(
                                                "assets/images/icons-chevron_.png",
                                                fit: BoxFit.none,
                                              ),
                                        ],)
                                          
                                      ) 
                        ],
                      ),
                    )
           
    );
      /*floatingActionButton: FloatingActionButton(
        onPressed: _addMemo,
        tooltip: 'New Memo',
        child: Icon(Icons.add),
      )*/
    
  }

}
