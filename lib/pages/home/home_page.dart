import 'dart:io';

import 'package:disposable_provider/disposable_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ibd_application/models/model.dart';
import 'package:provider/provider.dart';
import 'package:touch_indicator/touch_indicator.dart';
import 'package:vsync_provider/vsync_provider.dart';
import 'package:ibd_application/pages/pages.dart';

import 'app_bottom_navigation_bar.dart';

class HomePage extends StatelessWidget {
  const HomePage._({Key key}) : super(key: key);

  static Widget wrapped() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
      Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,  
    ));
    return ChangeNotifierProvider(
      create: (context) => HomePageState(),
      child: const HomePage._(),
    );
  }

  // TODO(mono): Keep state
  static const _pages = {
    0: HomeTab(),
    1: AppointmentPage(),
    2: CheckInPage(),
    3: MemoPage(),
    4: MorePage(),
  };

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Scaffold(
          bottomNavigationBar: const AppBottomNavigationBar(),
          body: Stack(
            children: <Widget>[
              _pages[
                  context.select((HomePageState state) => state.currentIndex)]
            ],
          ),
        ),
      ],
    );
  }
}

class HomePageState with ChangeNotifier {
  var _currentIndex = 0;
  int get currentIndex => _currentIndex;
  set currentIndex(int index) {
    _currentIndex = index;
    notifyListeners();
  }
}

class _FadeScreen extends StatelessWidget {
  const _FadeScreen({Key key}) : super(key: key);

  static final _fadeTween = Tween<double>(begin: 0, end: 0.7);

  @override
  Widget build(BuildContext context) {
    final notifier = context.read<PlayerNotifier>();
    return IgnorePointer(
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            bottom: null,
            child: FadeTransition(
              opacity: notifier.topFadeAnimation,
              child: Container(
                height: MediaQuery.of(context).padding.top,
                color: Colors.black,
              ),
            ),
          ),
          FadeTransition(
            opacity: notifier.expandingAnimation.drive(_fadeTween),
            child: Container(color: Colors.black),
          ),
        ],
      ),
    );
  }
}
