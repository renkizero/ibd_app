import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:ibd_application/pages/howto/landing_page.dart';
import 'package:ibd_application/pages/info/info0_page.dart';
import 'package:ibd_application/pages/info/info1_page.dart';
import 'package:ibd_application/pages/login/login_page.dart';
import 'package:ibd_application/pages/register/register_page1.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<int> _counter;

  @override
  void initState() {
    super.initState();
    _register();
  }

  _register() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int register = prefs.getInt('register');
    if (register == 1) {
      Timer.run(() {
        Route route =
            MaterialPageRoute(builder: (context) => RegisterFirstPage());
        Navigator.pushReplacement(context, route);
      });
    } else if (register == 2) {
      Timer.run(() {
        Route route =
            MaterialPageRoute(builder: (context) => HomePage.wrapped());
       // Route route =
        //    MaterialPageRoute(builder: (context) => Info0Page());
        Navigator.pushReplacement(context, route);
      });
    } else {
      Route route = MaterialPageRoute(builder: (context) => LandingPage());
      Navigator.pushReplacement(context, route);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
