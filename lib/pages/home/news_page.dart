import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';

class NewsPages extends StatefulWidget {
  @override
  _NewsPagesState createState() => _NewsPagesState();
}

class _NewsPagesState extends State<NewsPages> {

  String kNavigationExamplePage = '''
<!DOCTYPE html><html>
<head><title>Navigation Delegate Example</title></head>
<body>
<p>
The navigation delegate is set to block navigation to the youtube website.
</p>
<ul>
<ul><a href="https://www.youtube.com/">https://www.youtube.com/</a></ul>
<ul><a href="https://www.google.com/">https://www.google.com/</a></ul>
</ul>
</body>
</html>
''';


  @override
  Widget build(BuildContext context) {
     return  Scaffold(
          resizeToAvoidBottomPadding: false ,
           
      appBar: AppBar(
        leading: BackButton(
            color: Colors.black
        ),
        backgroundColor: HexColor("FFFFFF"),
        title:  Container(
                      width: 180,
                      height:55,
                      child: Image.asset(
                        "assets/images/ibd_title.png",
                        fit: BoxFit.fill,
                      ),
                    ),
      ),
      body: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 20, top: 20, right: 20),
              child: Text(
                "ท่านรู้หรือไม่ว่า IBD มีสาเหตุ",
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: HexColor("5E4589"),
                  fontFamily: "Kanit",
                  fontWeight: FontWeight.w400,
                  fontSize: 30,
                  height: 1.33333,
                ),
              ),
            ),
             Container(
                width: 145,
                height: 15,
                margin: EdgeInsets.only(left: 20, top: 4),
                child: Row(
                  children: [
                    Container(
                      width: 13,
                      height: 13,
                      child: Image.asset(
                        "assets/images/symbol-22--1.png",
                        fit: BoxFit.none,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        margin: EdgeInsets.only(left: 4),
                        child: Text(
                          "Fri 9 Nov, 15:00 - 15:30 GMT",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: HexColor("BCC5D3"),
                            fontFamily: "Kanit",
                            fontWeight: FontWeight.w400,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),Container(
                width: MediaQuery.of(context).size.width,
                      height: 259,
                      child: Image.asset(
                        "assets/images/3c423dfcb193ff62e1fd28c72833d87e.png",
                        fit: BoxFit.none,
                      ),
              ),Center(
        child: SingleChildScrollView(
          child: Html(
            data: """
    <div>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at cursus augue. Morbi lobortis, lacus sit amet gravida iaculis, magna quam condimentum velit, molestie tempus enim mauris tristique nisl. Nam eu mi posuere risus pretium efficitur. Aenean euismod eros lacus, nec gravida dolor tempor at. Donec sed arcu vitae dui condimentum pretium. In purus tortor, eleifend sit amet urna vitae, vestibulum dapibus nulla. Praesent dignissim vitae nunc vitae blandit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer ultricies, lectus quis consequat facilisis, ante mi aliquet eros, sit amet consectetur purus lectus ac justo. Etiam a odio sit amet metus mattis pretium sit amet a mi. Proin rhoncus dapibus lorem eget porttitor. Praesent pharetra egestas velit, nec porta libero facilisis eu. Sed ultricies luctus felis, vitae vestibulum diam tincidunt at. In nec lectus lorem. Quisque viverra tortor tristique leo faucibus, ac laoreet dolor lacinia. Integer a purus viverra, tempor est ut, interdum urna.

Praesent viverra est ligula, in feugiat eros pellentesque a. Donec commodo, odio non vulputate blandit, quam magna elementum sapien, eget interdum nisl nunc a tortor. In ut varius lorem. Etiam non imperdiet diam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec vel tincidunt enim. Sed nec augue nec elit feugiat dapibus non et neque. Phasellus lacus nisi, fermentum sit amet mauris eu, pellentesque venenatis arcu. Pellentesque non ex ut turpis venenatis tempus. Maecenas vel urna sapien.

Duis vel risus egestas, ullamcorper elit quis, consectetur orci. Nulla fermentum, ex vulputate lobortis placerat, tellus lectus feugiat tortor, vel ultrices lorem est sodales libero. Proin in viverra sapien. Pellentesque lobortis est vel purus bibendum ultrices in rhoncus elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec mi tincidunt, porttitor mauris sit amet, mollis nibh. Sed sodales ex nec ligula lobortis euismod. Aliquam euismod blandit elit et lobortis. Proin placerat mollis mattis. Quisque est lacus, imperdiet sit amet cursus quis, facilisis at nisi. Pellentesque porttitor diam vel massa lacinia viverra. Ut velit diam, interdum vitae ex ut, porttitor tempus eros. Pellentesque in sagittis justo. Curabitur sit amet condimentum leo. Sed ipsum lectus, iaculis placerat bibendum eget, accumsan et tellus.
  """,
            //Optional parameters:
            padding: EdgeInsets.all(8.0),
            linkStyle: const TextStyle(
              color: Colors.redAccent,
              decorationColor: Colors.redAccent,
              decoration: TextDecoration.underline,
            ),
            onLinkTap: (url) { 
              print("Opening $url...");
            },
            onImageTap: (src) {
              print(src);
            },
          ),
        ),
      ),
              
            
           ]
           ) // 
       );
  }
}
