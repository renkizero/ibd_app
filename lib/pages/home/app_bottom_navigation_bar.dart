import 'package:flutter/material.dart';
import 'package:ibd_application/models/model.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:provider/provider.dart';

class AppBottomNavigationBar extends StatelessWidget {
  const AppBottomNavigationBar({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final notifier = context.watch<PlayerNotifier>();

    return AnimatedBuilder(
      animation: notifier.expandingAnimation,
      builder: (context, child) => Align(
        alignment: Alignment.topCenter,
        heightFactor: 1 - notifier.expandingAnimation.value,
        child: child,
      ),
      child: BottomNavigationBar(
        currentIndex:
            context.select((HomePageState state) => state.currentIndex),
        onTap: (index) => context.read<HomePageState>().currentIndex = index,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            title: const Text(
              "Home",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 94, 69, 137),
                fontFamily: "Kanit",
                fontWeight: FontWeight.w400,
                fontSize: 9,
              ),
            ),
            icon: ImageIcon(
              AssetImage("assets/images/home-icon1.png"),
              color: Color.fromARGB(255, 94, 69, 137),
            ),
          ),
          BottomNavigationBarItem(
            title: const Text(
              "Appointment",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 94, 69, 137),
                fontFamily: "Kanit",
                fontWeight: FontWeight.w400,
                fontSize: 9,
              ),
            ),
            icon: ImageIcon(AssetImage("assets/images/calendar-icon.png"),
                color: Color.fromARGB(255, 94, 69, 137)),
          ),
          BottomNavigationBarItem(
            title: const Text(
              "Wellness",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 94, 69, 137),
                fontFamily: "Kanit",
                fontWeight: FontWeight.w400,
                fontSize: 9,
              ),
            ),
            icon: ImageIcon(AssetImage("assets/images/group-2406.png"),
                color: Color.fromARGB(255, 94, 69, 137)),
          ),
          BottomNavigationBarItem(
            title: const FittedBox(
              child: const Text(
                "Memo",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color.fromARGB(255, 94, 69, 137),
                  fontFamily: "Kanit",
                  fontWeight: FontWeight.w400,
                  fontSize: 9,
                ),
              ),
            ),
            icon: ImageIcon(AssetImage("assets/images/register-icon.png"),
                color: Color.fromARGB(255, 94, 69, 137)),
          ),
          BottomNavigationBarItem(
            title: const Text(
              "More",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 94, 69, 137),
                fontFamily: "Kanit",
                fontWeight: FontWeight.w400,
                fontSize: 9,
              ),
            ),
            icon: ImageIcon(AssetImage("assets/images/group-2407.png"),
                color: Color.fromARGB(255, 94, 69, 137)),
          ),
        ],
      ),
    );
  }
}
