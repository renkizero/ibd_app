import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/pages/appointment/appointment_add.dart';
import 'package:ibd_application/pages/home/news_page.dart';
import 'package:ibd_application/pages/home/noti_page.dart';
import 'package:ibd_application/pages/quiz/quiz_page.dart';
import 'package:ibd_application/router.dart';
import 'package:ibd_application/util.dart';
import 'package:ibd_application/values/values.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeTab extends StatelessWidget {
  const HomeTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RefreshController _refreshController =
        RefreshController(initialRefresh: false);

    void _onRefresh() async {
      // monitor network fetch
      await Future.delayed(Duration(milliseconds: 1000));
      // if failed,use refreshFailed()
      _refreshController.refreshCompleted();
    }

    void _onLoading() async {
      // monitor network fetch
      await Future.delayed(Duration(milliseconds: 1000));

      _refreshController.loadComplete();
    }

    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.transparent,
//           extendBodyBehindAppBar: true,
            appBar: AppBar(
              brightness: Brightness.dark,
              backgroundColor: Colors.transparent,
              title: const _Title(),
              elevation: 0.0,
              actions: <Widget>[
                FlatButton(
                  onPressed: () =>
                      Navigator.of(context).push(MaterialPageRoute<void>(
                    builder: (BuildContext context) {
                      return new NotiPage();
                    },
                  )),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: 45,
                        height: 45,
                        child: Align(
                          child: Image.asset(
                            "assets/images/notifications.png",
                            fit: BoxFit.none,
                          ),
                        ),
                        decoration: BoxDecoration(),
                      ),Stack(children:<Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left : 22.0),
                          child: Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              color: const Color(0xffc4403e),
                              shape: BoxShape.circle,
                            ),

                          ),
                        ), Padding(
                          padding: const EdgeInsets.only(left : 30.0,top: 2),
                          child: Text(
                            '1',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Kanit',
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Color(0xffffffff),

                            ),
                          ),
                        )
                      ],)
                    ],
                  ),
                  shape:
                      CircleBorder(side: BorderSide(color: Colors.transparent)),
                ),
              ],
            ),
            body: new Container(
                color: const Color(0xFFFFFF),
                child: SmartRefresher(
                    enablePullDown: true,
                    enablePullUp: true,
                    header: WaterDropHeader(),
                    footer: CustomFooter(
                      builder: (BuildContext context, LoadStatus mode) {
                        Widget body;
                        if (mode == LoadStatus.idle) {
                        } else if (mode == LoadStatus.loading) {
                          body = CupertinoActivityIndicator();
                        } else if (mode == LoadStatus.failed) {
                        } else if (mode == LoadStatus.canLoading) {
                        } else {}
                        return Container(
                          height: 55.0,
                          child: Center(child: body),
                        );
                      },
                    ),
                    controller: _refreshController,
                    onRefresh: _onRefresh,
                    onLoading: _onLoading,
                    child: ListView(children: <Widget>[
                      Container(
                        height: 340,
                        child: Align(
                          child: Image.asset(
                            "assets/images/group_app_34.png",
                          ),
                        ),
                      ),
                      Container(
                        height: 185,
                        margin: EdgeInsets.only(left: 22, right: 22, top: 1),
                        decoration: BoxDecoration(
                          gradient: Gradients.primaryGradient,
                          boxShadow: [
                            Shadows.primaryShadow,
                          ],
                          borderRadius: Radii.k10pxRadius,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                  Container(
                                    width: 84,
                                    margin: EdgeInsets.only(left: 20),
                                    child: Align(
                                      child: Image.asset(
                                        "assets/images/Group_1953.png",
                                        fit: BoxFit.none,
                                      ),
                                    ),
                                  )
                                ])),
                            Container(
                                width: 200,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Align(
                                        alignment: Alignment.topCenter,
                                        child: Text(
                                          "ไม่พลาดการนัดหมาย\nบันทึกช่วย",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 255, 255, 255),
                                            fontFamily: "Kanit",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 20,
                                            height: 1.25,
                                          ),
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () =>
                                            /*Navigator.of(
                                          context,
                                          rootNavigator: true,
                                        ).pushReplacementNamed(AppointMentAddPage.routeName)*/
                                            Navigator.push(
                                                context,
                                                // MaterialPageRoute(builder: (context) => AppointMentAddPage())),
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        AppointMentAddPage())),
                                        //  return new AppointMentAddPage(_onChanged);

                                        // return new MemoEdit(_memoList[_currentIndex], _onChanged);
                                        // MaterialPageRoute(builder: (context) => QuizStart())),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Container(
                                            width: 100,
                                            height: 30,
                                            margin: EdgeInsets.only(
                                                top: 3, right: 68),
                                            decoration: BoxDecoration(
                                              color: HexColor("F8B6B8"),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(6)),
                                            ),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                    child: Text("เพิ่มนัดหมาย",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                          color: HexColor(
                                                              "5E4589"),
                                                          fontFamily: "Kanit",
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 14,
                                                        )))
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    ]))
                          ],
                        ),
                      ),
                      Container(width: 200, child: Column()),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          width: 130,
                          height: 30,
                          margin: EdgeInsets.only(left: 31, top: 22),
                          child: Row(
                            children: [
                              Container(
                                width: 24,
                                height: 21,
                                child: Image.asset(
                                  "assets/images/group-1880.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                              Spacer(),
                              Text(
                                "เช็คสุขภาพ",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 69, 79, 99),
                                  fontFamily: "Kanit",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 20,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 20, top: 34, right: 20),
                          width: 310,
                          height: 186,
                          decoration: BoxDecoration(
                            color: const Color(0xffffffff),
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: const Offset(0, 3),
                                blurRadius: 6,
                                color: const Color(0xff8a6cbc).withOpacity(0.4),
                              )
                            ],
                          ),
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 20, right: 20.0, left: 20.0, bottom: 20),
                              child: Row(children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 1, right: 1.0, left: 1.0),
                                    child: LineChart(
                                      sampleData1(),
                                      swapAnimationDuration:
                                          Duration(milliseconds: 250),
                                    ),
                                  ),
                                )
                              ]))),
                      Container(
                          margin: EdgeInsets.only(left: 20, top: 34, right: 20),
                          child: Column(children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 136,
                                height: 30,
                                child: Row(
                                  children: [
                                    Container(
                                      width: 21,
                                      height: 18,
                                      child: Image.asset(
                                        "assets/images/group-82.png",
                                        fit: BoxFit.none,
                                      ),
                                    ),
                                    Spacer(),
                                    Text(
                                      "ความรู้ทั่วไป",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 69, 79, 99),
                                        fontFamily: "Kanit",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ])),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            InkWell(
                                onTap: () =>
                                    /*Navigator.of(
                                          context,
                                          rootNavigator: true,
                                        ).pushReplacementNamed(AppointMentAddPage.routeName)*/
                                    Navigator.push(
                                        context,
                                        // MaterialPageRoute(builder: (context) => AppointMentAddPage())),
                                        MaterialPageRoute(
                                            builder: (context) => NewsPages())),
                                //  return new AppointMentAddPage(_onChanged);

                                // return new MemoEdit(_memoList[_currentIndex], _onChanged);
                                // MaterialPageRoute(builder: (context) => QuizStart())),
                                child: Container(
                                  height: 300,
                                  margin: EdgeInsets.only(
                                      left: 22, right: 22, top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        child: Image.asset(
                                          "assets/images/3c423dfcb193ff62e1fd28c72833d87e.png",
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Container(
                                          width: 145,
                                          height: 15,
                                          margin: EdgeInsets.only(top: 9),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: 13,
                                                height: 13,
                                                child: Image.asset(
                                                  "assets/images/symbol-22--1.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 4),
                                                  child: Text(
                                                    "Fri 9 Nov, 15:00 - 15:30 GMT",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      color:
                                                          AppColors.accentText,
                                                      fontFamily: "Kanit",
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 10,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "IBD มีสาเหตุจากอะไร?\nเรามาดูสาเหตุของการเกิดโรคไอบีดี",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: AppColors.accentElement,
                                            fontFamily: "Kanit",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 20,
                                            height: 1.25,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  InkWell(
                                    onTap: () =>
                                        /*Navigator.of(
                                          context,
                                          rootNavigator: true,
                                        ).pushReplacementNamed(AppointMentAddPage.routeName)*/
                                        Navigator.push(
                                            context,
                                            // MaterialPageRoute(builder: (context) => AppointMentAddPage())),
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    NewsPages())),
                                    //  return new AppointMentAddPage(_onChanged);

                                    // return new MemoEdit(_memoList[_currentIndex], _onChanged);
                                    // MaterialPageRoute(builder: (context) => QuizStart())),
                                    child: Container(
                                      height: 300,
                                      margin: EdgeInsets.only(
                                          left: 22, right: 22, top: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            child: Image.asset(
                                              "assets/images/88719a4e139f0d73df9135a32bc25b2e.png",
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              width: 145,
                                              height: 15,
                                              margin: EdgeInsets.only(top: 9),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    width: 13,
                                                    height: 13,
                                                    child: Image.asset(
                                                      "assets/images/symbol-22--1.png",
                                                      fit: BoxFit.none,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          left: 4),
                                                      child: Text(
                                                        "Fri 9 Nov, 15:00 - 15:30 GMT",
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                          color: AppColors
                                                              .accentText,
                                                          fontFamily: "Kanit",
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 10,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              "IBD มีสาเหตุจากอะไร?\nเรามาดูสาเหตุของการเกิดโรคไอบีดี",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                color: AppColors.accentElement,
                                                fontFamily: "Kanit",
                                                fontWeight: FontWeight.w400,
                                                fontSize: 20,
                                                height: 1.25,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ])
                          ])
                    ])))));
  }

  LineChartData sampleData1() {
    return LineChartData(
      lineTouchData: LineTouchData(
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.blueGrey.withOpacity(0.8),
        ),
        touchCallback: (LineTouchResponse touchResponse) {
          print(touchResponse);
        },
        handleBuiltInTouches: true,
      ),
      gridData: const FlGridData(
        show: false,
      ),
      titlesData: FlTitlesData(
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: TextStyle(
            fontFamily: 'Kanit',
            color: const Color(0xff72719b),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          margin: 5,
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '30';
              case 2:
                return '';
              case 3:
                return '1';
              case 4:
                return '';
              case 5:
                return '2';
              case 6:
                return '';
              case 7:
                return '3';
              case 8:
                return '';
              case 9:
                return '4';
              case 10:
                return '';
              case 11:
                return '5';
              case 12:
                return '';
              case 13:
                return '6';
            }
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: false,
          textStyle: TextStyle(
            color: const Color(0xff75729e),
            fontFamily: 'Kanit',
            fontWeight: FontWeight.normal,
            fontSize: 12,
          ),
          getTitles: (value) {
            return value.toString();
          },
          margin: 8,
          reservedSize: 30,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(
            color: const Color(0xff4e4965),
            width: 1,
          ),
          left: BorderSide(
            color: const Color(0xff4e4965),
            width: 1,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: 14,
      maxY: 4,
      minY: 0,
      lineBarsData: linesBarData1(),
    );
  }

  List<LineChartBarData> linesBarData1() {
    LineChartBarData lineChartBarData1 = const LineChartBarData(
      spots: [
        FlSpot(1, 1),
        FlSpot(3, 1.5),
        FlSpot(5, 1.4),
        FlSpot(7, 3.4),
        FlSpot(10, 2),
        FlSpot(12, 2.2),
        FlSpot(13, 1.8),
      ],
      isCurved: true,
      colors: [
        Color(0xff4af699),
      ],
      barWidth: 8,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: false,
      ),
      belowBarData: BarAreaData(
        show: false,
      ),
    );
    final LineChartBarData lineChartBarData2 = LineChartBarData(
      spots: [
        FlSpot(1, 1),
        FlSpot(3, 2.8),
        FlSpot(7, 1.2),
        FlSpot(10, 2.8),
        FlSpot(12, 2.6),
        FlSpot(13, 3.9),
      ],
      isCurved: true,
      colors: [
        Color(0xffaa4cfc),
      ],
      barWidth: 8,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: false,
      ),
      belowBarData: BarAreaData(show: false, colors: [
        Color(0x00aa4cfc),
      ]),
    );
    LineChartBarData lineChartBarData3 = LineChartBarData(
      spots: const [
        FlSpot(1, 2.8),
        FlSpot(3, 1.9),
        FlSpot(6, 3),
        FlSpot(10, 1.3),
        FlSpot(13, 2.5),
      ],
      isCurved: true,
      colors: const [
        Color(0xffc4403e),
      ],
      barWidth: 8,
      isStrokeCapRound: true,
      dotData: const FlDotData(
        show: false,
      ),
      belowBarData: const BarAreaData(
        show: false,
      ),
    );
    return [
      lineChartBarData1,
      lineChartBarData2,
      lineChartBarData3,
    ];
  }
}

class _Title extends StatelessWidget {
  const _Title({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          "IBD Friend",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: AppColors.accentText,
            fontFamily: "Kanit",
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        )
      ],
    );
  }
}
