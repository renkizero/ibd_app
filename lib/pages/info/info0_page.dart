/*
*  patient_report_symptoms13_widget.dart
*  Detail
*
*  Created by .
*  Copyright © 2018 . All rights reserved.
    */

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:ibd_application/pages/info/info1_page.dart';
import 'package:ibd_application/values/values.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqlcool/sqlcool.dart';

class Info0Page extends StatefulWidget {
  @override
  _Info0PageState createState() => _Info0PageState();
}

class _Info0PageState extends State<Info0Page>
    with SingleTickerProviderStateMixin {
  SelectBloc bloc;

  AnimationController _controller;
  Animation _animation;

  FocusNode _focusNode = FocusNode();

  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var hotpital = "";

  @override
  void initState() {
    super.initState();

    this.bloc = SelectBloc(
        database: db, table: "paintinfo", where: "id = 1", reactive: true);

    _getData();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString("email"));

    List<Map<String, dynamic>> rows = await db.select(
      table: "paintinfo",
      where: "id = 1",
    );

    print(rows[0]);
    //this.doctor = rows[0]["doctor"];

    this.setState(() {
      //  print(this.emo1);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(0.5, 0),
              end: Alignment(0.5, 1),
              stops: [
                0,
                1,
              ],
              colors: [
                Color.fromARGB(255, 138, 108, 188),
                Color.fromARGB(255, 69, 45, 110),
              ],
            ),
          ),
          child: new InkWell(
            // to dismiss the keyboard when the user tabs out of the TextField
            splashColor: Colors.transparent,
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Container(
              child: FormBuilder(
                  key: _fbKey,
                  initialValue: {
                    'date': DateTime.now(),
                    'accept_terms': false,
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                            width: 17,
                            height: 17,
                            margin: EdgeInsets.only(top: 44, right: 9),
                            child: Container(
                              child: Stack(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Route route = MaterialPageRoute(
                                          builder: (context) =>
                                              HomePage.wrapped());
                                      Navigator.pushReplacement(context, route);
                                    },
                                    child: Image.asset(
                                      "assets/images/group-2039.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            "Patient info",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.primaryText,
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 176,
                          height: 20,
                          margin: EdgeInsets.only(top: 36),
                          child: Image.asset(
                            "assets/images/group-1520.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                      StreamBuilder<List<Map>>(
                          stream: bloc.items,
                          builder: (context, snapshot) {
                            String _doctor ;
                            String _hospital ;
                            if (snapshot.hasData) {
                              final dynamic item = snapshot.data[0];
                              _doctor = item["doctor"].toString();

                              _hospital = item["hospital"].toString();
                            }
                           // print("_doctor==> $_doctor");
                            return Expanded(
                              flex: 1,
                              child: Container(
                                margin: EdgeInsets.only(top: 40),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Positioned(
                                      left: 0,
                                      top: 0,
                                      right: 0,
                                      child: Container(
                                        height: 633,
                                        decoration: BoxDecoration(
                                          color: AppColors.primaryBackground,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                        ),
                                        child: Container(),
                                      ),
                                    ),
                                    Positioned(
                                        left: 1,
                                        top: 71,
                                        right: 1,
                                        bottom: 91,
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: [
                                              Align(
                                                alignment: Alignment.center,
                                                child: Container(
                                                  child: Text(
                                                    "เลือกโรงพยาบาล",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      color:
                                                          AppColors.primaryText,
                                                      fontFamily: "Kanit",
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 22,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ])),
                                    Positioned(
                                      top: 126,
                                      child: Center(
                                        child: Container(
                                          height: 70,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.8,
                                          margin: EdgeInsets.only(top: 61),
                                          decoration: BoxDecoration(
                                            color: HexColor("F4F8FC"),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                          ),
                                          child: Container(
                                            child: Column(children: [
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: FormBuilderDropdown(
                                                      attribute: "hopistal",
                                                      decoration:
                                                          InputDecoration(
                                                              enabledBorder: UnderlineInputBorder(
                                                                  borderSide: BorderSide(
                                                                      color: HexColor(
                                                                          "F4F8FC"))),
                                                              focusedBorder: UnderlineInputBorder(
                                                                  borderSide: BorderSide(
                                                                      color: Colors
                                                                          .white)),
                                                              labelText:
                                                                  "เลือกโรงพยาบาล",
                                                              labelStyle:
                                                                  TextStyle(
                                                                color: HexColor(
                                                                    "BCC5D3"),
                                                                fontFamily:
                                                                    "Kanit",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 22,
                                                              )),
                                                      items: [
                                                        'โรงพยาบาล A',
                                                        'โรงพยาบาล B',
                                                        'โรงพยาบาล C'
                                                      ]
                                                          .map((hospital) =>
                                                              DropdownMenuItem(
                                                                  value:
                                                                      hospital,
                                                                  child: Text(
                                                                      "$hospital",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        color: AppColors
                                                                            .primaryText,
                                                                        fontFamily:
                                                                            "Kanit",
                                                                        fontWeight:
                                                                            FontWeight.w400,
                                                                        fontSize:
                                                                            16,
                                                                      ))))
                                                          .toList(),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ]),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      top: 226,
                                      child: Container(
                                        height: 70,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.8,
                                        margin: EdgeInsets.only(top: 61),
                                        decoration: BoxDecoration(
                                          color: HexColor("F4F8FC"),
                                          borderRadius: Radii.k10pxRadius,
                                        ),
                                        child: Container(
                                          margin: EdgeInsets.only(left: 10),
                                          child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                FormBuilderTextField(
                                                  attribute: "doctor",
                                                  initialValue: _doctor,
                                                  maxLines: 1,
                                                  decoration: InputDecoration(
                                                      labelText:
                                                          "ระบุหมอเจ้าของ",
                                                      enabledBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: HexColor(
                                                                      "F4F8FC"))),
                                                      focusedBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                      labelStyle: TextStyle(
                                                        color:
                                                            HexColor("BCC5D3"),
                                                        fontFamily: "Kanit",
                                                        fontSize: 16,
                                                      )),
                                                  onChanged: (value) {
                                                   // _fbKey.currentState.setAttributeValue('doctor', "testset");
                                                  },
                                                )
                                              ]),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                        left: 35,
                                        top: 380,
                                        child: InkWell(
                                            onTap: () async {
                                              /*Route route = MaterialPageRoute(
                                                builder: (context) => Info1Page());
                                            Navigator.pushAndRemoveUntil(
                                                context, route, (route) => false);*/
                                              SharedPreferences prefs =
                                                  await SharedPreferences
                                                      .getInstance();
                                              if (_fbKey.currentState
                                                  .saveAndValidate()) {
                                                print(
                                                    _fbKey.currentState.value);
                                                var val =
                                                    _fbKey.currentState.value;
                                                /*val.forEach((k, v) =>
                                                  prefs.setString('${k}', '${v}'));
                                               */
                                                var _hospital =
                                                    val["hopistal"].toString();

                                                print(_hospital);

                                                var _doctor =
                                                    val["doctor"].toString();

                                                updatePaintInfo(
                                                    _hospital, _doctor, "", "");

                                                Route route = MaterialPageRoute(
                                                    builder: (context) =>
                                                        Info1Page());
                                                Navigator.pushAndRemoveUntil(
                                                    context,
                                                    route,
                                                    (route) => false);
                                              }
                                            },
                                            child: Align(
                                              alignment: Alignment.topCenter,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(top: 47),
                                                width: 315,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                  color: HexColor("F8B6B8"),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(6)),
                                                ),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      "ถัดไป",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 94, 69, 137),
                                                        fontFamily: "Kanit",
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 16,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )))
                                  ],
                                ),
                              ),
                            );
                          }),
                    ],
                  )),
            ),
          )),
    );
  }
}
