/*
*  patient_report_symptoms12_widget.dart
*  Detail
*
*  Created by .
*  Copyright © 2018 . All rights reserved.
    */

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/checkin/checkin_page.dart';
import 'package:ibd_application/pages/checkin/checkin_quizpage.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:ibd_application/pages/info/info1_page.dart';
import 'package:ibd_application/values/values.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Info2Page extends StatefulWidget {
  @override
  _Info2PageState createState() => _Info2PageState();
}

class _Info2PageState extends State<Info2Page> {


  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment(0.5, 0),
            end: Alignment(0.5, 1),
            stops: [
              0,
              1,
            ],
            colors: [
              Color.fromARGB(255, 138, 108, 188),
              Color.fromARGB(255, 69, 45, 110),
            ],
          ),
        ),
        child: FormBuilder(
          key: _fbKey,
          initialValue: {
            'date': DateTime.now(),
            'accept_terms': false,
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  width: 17,
                  height: 17,
                  margin: EdgeInsets.only(top: 44, right: 9),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        top: 0,
                        right: 0,
                        child: Image.asset(
                          "assets/images/group-2039.png",
                          fit: BoxFit.none,
                        ),
                      ),
                      Positioned(
                        top: 0,
                        right: 0,
                        child: Image.asset(
                          "assets/images/group-2039-2.png",
                          fit: BoxFit.none,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text(
                    "Patient info",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: AppColors.primaryText,
                      fontFamily: "Kanit",
                      fontWeight: FontWeight.w400,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 176,
                  height: 20,
                  margin: EdgeInsets.only(top: 36),
                  child: Image.asset(
                    "assets/images/group-1520-2.png",
                    fit: BoxFit.none,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        left: 0,
                        top: 0,
                        right: 0,
                        child: Container(
                          height: 633,
                          decoration: BoxDecoration(
                            color: AppColors.primaryBackground,
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Container(),
                        ),
                      ),
                      Positioned(
                        left: 20,
                        top: 71,
                        right: 20,
                        bottom: 51,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 80),
                                child: Text(
                                  "ข้อมูลการใช้ยา",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: AppColors.primaryText,
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 30,
                                  ),
                                ),
                              ),
                            ),
                            Spacer(),Center(

                              child:  InkWell(
                                onTap: () async {
                                  Route route = MaterialPageRoute(
                                      builder: (context) => Info1Page());
                                  Navigator.pushAndRemoveUntil(
                                      context, route, (route) => false);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      width: 18,
                                      height: 18,
                                      margin: EdgeInsets.only(right: 10),
                                      child: Image.asset(
                                        "assets/images/arrow-icon_2.png",
                                        fit: BoxFit.none,
                                      ),
                                    ),
                                    Text(
                                      'ย้อนกลับ',

                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,

                                        color: Color(0xffbcc5d3),


                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Positioned(
                        left: 40,
                        top: 116,
                        child: Container(
                          height: 70,
                          width: MediaQuery.of(context).size.width * 0.8,
                          margin: EdgeInsets.only(top: 61),
                          decoration: BoxDecoration(
                            color: HexColor("F4F8FC"),
                            borderRadius: Radii.k10pxRadius,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FormBuilderTextField(
                                  attribute: "probiotic",
                                  maxLines: 1,
                                  decoration: InputDecoration(
                                      labelText:
                                      "ระบุยา",
                                      enabledBorder:
                                      UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: HexColor(
                                                  "F4F8FC"))),
                                      focusedBorder:
                                      UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors
                                                  .white)),
                                      labelStyle: TextStyle(
                                        color:
                                        HexColor("BCC5D3"),
                                        fontFamily: "Kanit",
                                        fontSize: 16,
                                      )),
                                  onChanged: (value) {
                                    // _fbKey.currentState.setAttributeValue('doctor', "testset");
                                  },
                                )
                              ]),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:400.0),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: InkWell(
                              onTap: () async {
                                /*SharedPreferences prefs =
                                          await SharedPreferences.getInstance();
                                      prefs.setInt("input_info", 1);*/

                                if (_fbKey.currentState
                                    .saveAndValidate()) {
                                  print(
                                      _fbKey.currentState.value);
                                  var val =
                                      _fbKey.currentState.value;
                                  /*val.forEach((k, v) =>
                                                    prefs.setString('${k}', '${v}'));
                                                 */
                                  var probiotic =
                                  val["probiotic"].toString();
                                  updatePaintInfo2(probiotic);

                                }

                                Route route = MaterialPageRoute(
                                    builder: (context) => HomePage.wrapped());
                                Navigator.pushAndRemoveUntil(
                                    context, route, (route) => false);
                              },
                              child: Container(
                                width: 315,
                                height: 50,
                                margin: EdgeInsets.only(bottom: 16),
                                decoration: BoxDecoration(
                                  color: HexColor("F8B6B8"),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "ถัดไป",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: AppColors.accentText,
                                        fontFamily: "Kanit",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
