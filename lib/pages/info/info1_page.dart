/*
*  patient_report_symptoms11_widget.dart
*  Detail
*
*  Created by .
*  Copyright © 2018 . All rights reserved.
    */

import 'package:flutter/material.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:ibd_application/pages/info/info0_page.dart';
import 'package:ibd_application/pages/info/info2_page.dart';
import 'package:ibd_application/util.dart';
import 'package:ibd_application/values/values.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Info1Page extends StatefulWidget {
  @override
  _Info1PageState createState() => _Info1PageState();
}

class _Info1PageState extends State<Info1Page> {
  _saveData(String content) async {
    updatePaintInfo1(content);
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString("user_choice", content);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment(0.5, 0),
            end: Alignment(0.5, 1),
            stops: [
              0,
              1,
            ],
            colors: [
              Color.fromARGB(255, 138, 108, 188),
              Color.fromARGB(255, 69, 45, 110),
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 17,
                height: 17,
                margin: EdgeInsets.only(top: 44, right: 9),
                child: Container(
                  child: Stack(
                    children: [
                      InkWell(
                        onTap: () {
                          Route route = MaterialPageRoute(
                              builder: (context) => HomePage.wrapped());
                          Navigator.pushReplacement(context, route);
                        },
                        child: Image.asset(
                          "assets/images/group-2039.png",
                          fit: BoxFit.none,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  "Patient info",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: AppColors.primaryText,
                    fontFamily: "Kanit",
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 176,
                height: 20,
                margin: EdgeInsets.only(top: 36),
                child: Image.asset(
                  "assets/images/group-1520-3.png",
                  fit: BoxFit.none,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(top: 40),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      child: Container(
                        height: 633,
                        decoration: BoxDecoration(
                          color: AppColors.primaryBackground,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Container(),
                      ),
                    ),
                    Positioned(
                      left: 20,
                      top: 71,
                      right: 20,
                      bottom: 51,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(left: 59),
                              child: Text(
                                "เลือกโรคที่คุณเป็น",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: AppColors.primaryText,
                                  fontFamily: "Kanit",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 30,
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                              onTap: () async {
                                _saveData("Ulcerative Colitis");
                                Route route = MaterialPageRoute(
                                    builder: (context) => Info2Page());
                                Navigator.pushAndRemoveUntil(
                                    context, route, (route) => false);
                              },
                              child: Container(
                                height: 60,
                                margin: EdgeInsets.only(top: 61),
                                decoration: BoxDecoration(
                                  color: HexColor("DCDCEF"),
                                  borderRadius: Radii.k10pxRadius,
                                ),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Ulcerative Colitis",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: AppColors.primaryText,
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                        ),
                                      )
                                    ]),
                              )),
                          InkWell(
                              onTap: () async {
                                _saveData("Crohn’s Disease");
                                Route route = MaterialPageRoute(
                                    builder: (context) => Info2Page());
                                Navigator.pushAndRemoveUntil(
                                    context, route, (route) => false);
                              },
                              child: Container(
                                height: 60,
                                margin: EdgeInsets.only(top: 20),
                                decoration: BoxDecoration(
                                  color: HexColor("DCDCEF"),
                                  borderRadius: Radii.k10pxRadius,
                                ),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Crohn’s Disease",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: AppColors.primaryText,
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16,
                                        ),
                                      )
                                    ]),
                              )),
                          Spacer(),
                          Center(
                            child: InkWell(
                              onTap: () async {
                                Route route = MaterialPageRoute(
                                    builder: (context) => Info0Page());
                                Navigator.pushAndRemoveUntil(
                                    context, route, (route) => false);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 18,
                                    height: 18,
                                    margin: EdgeInsets.only(right: 10),
                                    child: Image.asset(
                                      "assets/images/arrow-icon_2.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                  Text(
                                    'ย้อนกลับ',
                                    style: TextStyle(
                                      fontFamily: 'Kanit',
                                      fontSize: 16,
                                      color: Color(0xffbcc5d3),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 77,
                              height: 24,
                              margin: EdgeInsets.only(left: 124),
                              child: Row(
                                children: [],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
