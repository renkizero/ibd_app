import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibd_application/pages/quiz/quiz_1/quiz_1_q.dart';
import 'package:ibd_application/pages/quiz/quiz_page.dart';
import 'package:ibd_application/util.dart';

class QuizInStart extends StatefulWidget {
  @override
  _QuizInStartState createState() => _QuizInStartState();
}

class _QuizInStartState extends State<QuizInStart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: HexColor("C4C4E0"),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            InkWell(
                onTap: (){
                  //Route route = MaterialPageRoute(builder: (context) => QuizPage());
                  Navigator.pop(context);
                }
                ,child:
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 17,
                height: 17,
                margin: EdgeInsets.only(top: 42, right: 9),
                child: Image.asset(
                  "assets/images/group-2039.png",
                  fit: BoxFit.none,
                ),
              ),
            )
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 315,
                height: 316,
                margin: EdgeInsets.only(top: 1),
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      bottom: 0,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            left: 0,
                            right: 0,
                            child: Image.asset(
                              "assets/images/Group_2484_2.png",
                              fit: BoxFit.fill,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(top: 19),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 0,
                      right: 0,
                      bottom: 0,
                      child: Container(
                          height: 400,
                          decoration: BoxDecoration(
                            color: HexColor("#FFFFFF"),
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Column(
                            children: <Widget>[
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top:50,left: 38),
                                        child: Text(
                                          "ให้เราดูแลสุขภาพ",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: HexColor("000000"),
                                            fontFamily: "Kanit",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 35,
                                          ),
                                        ),
                                      ),
                                    ),Align(
                                      alignment: Alignment.topCenter,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 7),
                                        child: Text(
                                          "ให้เราได้เป็นส่วนหนึ่ง ของการดูแลสุขภาพคุณ\nให้หายดีจากอาการ",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: HexColor("4C5264"),
                                            fontFamily: "Kanit",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            height: 1.25,
                                          ),
                                        ),
                                      ),
                                    ),Align(
                                      alignment: Alignment.topCenter,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 27),
                                        child: Text(
                                          "เริ่มทำแบบสอบถาม",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: HexColor("4C5264"),
                                            fontFamily: "Kanit",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 20,
                                            height: 1.3,
                                          ),
                                        ),
                                      ),
                                    ),Align(
                                      alignment: Alignment.topCenter,
                                      child: Container(
                                        child: Text(
                                          "EQ-5D-5L",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: HexColor("8A6CBC"),
                                            fontFamily: "Kanit",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 20,
                                            height: 1.3,
                                          ),
                                        ),
                                      ),
                                    ),InkWell(
                                        onTap: (){
                                          Route route = MaterialPageRoute(builder: (context) => QuizIn1Page(current: 0));
                                          Navigator.pushReplacement(context, route);
                                        }
                                        ,child:Align(
                                      alignment: Alignment.topCenter,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 27),
                                        width: 315,
                                        height: 50,
                                        decoration: BoxDecoration(
                                          color: HexColor("AA88E2"),
                                          borderRadius: BorderRadius.all(Radius.circular(6)),
                                        ),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "เริ่ม",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Kanit",
                                                fontWeight: FontWeight.w400,
                                                fontSize: 16,
                                              ),
                                            )


                                          ],
                                        ),
                                      ),
                                    )
                                    )
                                  ]
                              )
                            ],
                          )
                      ),
                    ),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}