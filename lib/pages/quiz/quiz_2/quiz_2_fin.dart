import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibd_application/pages/quiz/quiz_1/quiz_1_page.dart';
import 'package:ibd_application/pages/quiz/quiz_2/quiz_2_start.dart';
import 'package:ibd_application/util.dart';

class QuizIn2Finish extends StatefulWidget {
  @override
  _QuizIn2FinishState createState() => _QuizIn2FinishState();
}

class _QuizIn2FinishState extends State<QuizIn2Finish> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 252, 239, 239),
            ),
            child: Container(
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
              ),
              child: Column(
                children: [
                  InkWell(
                      onTap: (){
                        //Route route = MaterialPageRoute(builder: (context) => QuizPage());
                        Navigator.pop(context);
                      }
                      ,child:
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 17,
                      height: 17,
                      margin: EdgeInsets.only(top: 42, right: 9),
                      child: Image.asset(
                        "assets/images/group-2039.png",
                        fit: BoxFit.none,
                      ),
                    ),
                  )
                  ),
                  Container(
                    width: 243,
                    height: 320,
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        Positioned(
                          top: 42,
                          child: Image.asset(
                            "assets/images/group_app_34_1.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 81,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 0,
                          child: Text(
                            "HANDS",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: HexColor("5E4589"),
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 24,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 37,
                          child: Text(
                            "แบบสอบถามประเมินคุณภาพชีวิตผู้ป่วย\nโรคลำไส้อับเสบเรื้อรัง",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: HexColor("5E4589"),
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: HexColor("DCDCEF"),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    width: 315,
                    height: 130,
                    margin: EdgeInsets.only(top: 18),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "ผู้ป่วยต้องดูแลตัวเองให้ดียิ่งขึ้น โดยการควบคุมอาหาร การรับประทานอาหารที่คุณหมอได้บอกให้ เช่น ไม่ทานเค็ม ไม่ทานหวานมาก ควรดื่มน้ำเปล่าวันละ 3 ขวดเป็นต้น ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: HexColor("000000"),
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      margin: EdgeInsets.only(top: 27),
                      width: 315,
                      height: 50,
                      decoration: BoxDecoration(
                        color: HexColor("F8B6B8"),
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                              onTap: (){
                               // Route route = MaterialPageRoute(builder: (context) => QuizIn2Start());
                               // Navigator.pushReplacement(context, route);
                                Navigator.pop(context);
                              }
                              ,child:Text(
                            "สรุปผล",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color.fromARGB(255, 94, 69, 137),
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                            ),
                          )
                          )

                        ],
                      ),
                    ),
                  )
                ],
              ),

            )
        )
    );

  }
}