import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibd_application/bloc/quiz_bloc.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/models/entities/answer_info.dart';
import 'package:ibd_application/models/entities/entities.dart';
import 'package:ibd_application/pages/quiz/quiz_1/quiz_1_fin.dart';
import 'package:ibd_application/pages/quiz/quiz_2/quiz_2_fin.dart';
import 'package:ibd_application/pages/quiz/quiz_fin.dart';
import 'package:ibd_application/util.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_logger/simple_logger.dart';
import 'package:ibd_application/values/values.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:ibd_application/bloc/quiz_bloc1.dart';

class QuizIn2Page extends StatefulWidget {
  const QuizIn2Page({Key key, this.current}) : super(key: key);
  final int current;
  static const routeName = '/QuizIn2Page';

  @override
  _QuizIn2PageState createState() => _QuizIn2PageState(this.current);
}

class _QuizIn2PageState extends State<QuizIn2Page> {
  static const double _horizontalMargin = 16;
  int current = 0;
  int prog_current = 0;
  final logger = SimpleLogger();
  int total = 0;

  var number = 0;

  _QuizIn2PageState(this.current);

  @override
  void initState() {
    bloc.getQuiz3();
    mainBloc = MainBloc();
    setState(() {
      this.current = current;
      this.prog_current =  current;
    });
    super.initState();
  }

  @override
  void dispose() {
    mainBloc = null; // destroying the mainBloc object to free resources
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: StreamBuilder<QuizResult>(
          stream: bloc.subject.stream,
          builder: (context, AsyncSnapshot<QuizResult> snapshot) {
            //print(snapshot.hasData);
            if (snapshot.hasData) {
              QuizResult list = snapshot.data;

              return _buildQuizAll(list);
            } else {
              return _buildErrorWidget();
            }
          },
        ));
  }

  Widget _buildErrorWidget() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Text("Error occured:"),
          ],
        ));
  }

  Widget _buildQuizAll(QuizResult list) {
    List<QuizInfo> info = list.result;

    this.total = list.result.length;

    var type = info[current].type;

    var condition = 1;

    if (condition == 1) {
      return Scaffold(
          body:
          /*Center(
           child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StepProgressIndicator(
                      totalSteps: this.total,
                      currentStep: this.prog_current,
                      selectedColor: Colors.red,
                      unselectedColor: Colors.yellow,
                    ),
              _buildQuiz(list)
          ]
           )
         )
      );*/

          Container(
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(0.5, 0),
                  end: Alignment(0.5, 1),
                  stops: [
                    0,
                    1,
                  ],
                  colors: [
                    HexColor("C4C4E0"),
                    HexColor("C4C4E0"),
                  ],
                ),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    InkWell(
                        onTap: () {
                          //Route route = MaterialPageRoute(builder: (context) => QuizIn2Page());
                          Navigator.pop(context, true);
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            width: 17,
                            height: 17,
                            margin: EdgeInsets.only(top: 42, right: 9),
                            child: Image.asset(
                              "assets/images/group-2039.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        )),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        margin: EdgeInsets.only(top: 1),
                        child: Text(
                          "HANDS",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.primaryText,
                            fontFamily: "Kanit",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        margin: EdgeInsets.only(top: 1),
                        child: Text(
                          "แบบสอบถามประเมินคุณภาพชีวิตผู้ป่วย\nโรคลำไส้อับเสบเรื้อรัง",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.primaryText,
                            fontFamily: "Kanit",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 28,
                      margin: EdgeInsets.only(left: 18, top: 18, right: 19),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 23,
                            margin: EdgeInsets.only(left: 2),
                            child: Row(
                              crossAxisAlignment:
                              CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "HANDS",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: AppColors.primaryText,
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "${this.prog_current + 1} / ${this.total}",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: AppColors.primaryText,
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Container(
                            height: 4,
                            margin: EdgeInsets.only(right: 1),
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                StepProgressIndicator(
                                  padding: 0,
                                  totalSteps: this.total,
                                  currentStep: this.prog_current + 1,
                                  selectedColor: HexColor("F97B7D"),
                                  unselectedColor: Colors.white,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        flex: 1,
                        child: Container(
                            margin: EdgeInsets.only(top: 25),
                            child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Positioned(
                                    left: 0,
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      height: 633,
                                      decoration: BoxDecoration(
                                        color: AppColors.primaryBackground,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                      ),
                                      child: _buildQuiz(list),
                                    ),
                                  ),
                                ])))
                  ])));
    } else if (condition == 2) {
      return Scaffold(
          body:
          /*Center(
           child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StepProgressIndicator(
                      totalSteps: this.total,
                      currentStep: this.prog_current,
                      selectedColor: Colors.red,
                      unselectedColor: Colors.yellow,
                    ),
              _buildQuiz(list)
          ]
           )
         )
      );*/

          Container(
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(0.5, 0),
                  end: Alignment(0.5, 1),
                  stops: [
                    0,
                    1,
                  ],
                  colors: [
                    Color.fromARGB(255, 243, 123, 125),
                    Color.fromARGB(255, 196, 64, 62),
                  ],
                ),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    InkWell(
                        onTap: () {
                          //Route route = MaterialPageRoute(builder: (context) => QuizIn2Page());
                          Navigator.pop(context, true);
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            width: 17,
                            height: 17,
                            margin: EdgeInsets.only(top: 42, right: 9),
                            child: Image.asset(
                              "assets/images/group-2039.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        )),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        margin: EdgeInsets.only(top: 1),
                        child: Text(
                          "Crohn’s Disease",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.primaryText,
                            fontFamily: "Kanit",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        margin: EdgeInsets.only(top: 1),
                        child: Text(
                          "แบบประเมินความรุนแรงของโรค",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.primaryText,
                            fontFamily: "Kanit",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 28,
                      margin: EdgeInsets.only(left: 18, top: 18, right: 19),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 23,
                            margin: EdgeInsets.only(left: 2),
                            child: Row(
                              crossAxisAlignment:
                              CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Crohn’s Disease (Harvey Bradshaw Index)",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: AppColors.primaryText,
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "${this.prog_current + 1} / ${this.total}",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: AppColors.primaryText,
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Container(
                            height: 4,
                            margin: EdgeInsets.only(right: 1),
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                StepProgressIndicator(
                                  padding: 0,
                                  totalSteps: this.total,
                                  currentStep: this.prog_current + 1,
                                  selectedColor: HexColor("C4403E"),
                                  unselectedColor: Colors.white,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        flex: 1,
                        child: Container(
                            margin: EdgeInsets.only(top: 25),
                            child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Positioned(
                                    left: 0,
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      height: 633,
                                      decoration: BoxDecoration(
                                        color: AppColors.primaryBackground,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                      ),
                                      child: _buildQuiz(list),
                                    ),
                                  ),
                                ])))
                  ])));
    } else {
      return Text("Waiting");
    }
  }

  Widget _buildQuiz(QuizResult list) {
    return Column(
      children: [
        Padding(
          child: _buildQuizTitle(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ),
        Padding(
          child: _buildSelection(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ), this.prog_current > 0 ? Container(
          margin: EdgeInsets.only(top:30),
          child: Center(

            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 18,
                  height: 18,
                  margin: EdgeInsets.only(right: 10),
                  child: Image.asset(
                    "assets/images/arrow-icon_2.png",
                    fit: BoxFit.none,
                  ),
                ),
                Text(
                  'ย้อนกลับ',

                  style: TextStyle(
                    fontFamily: 'Kanit',
                    fontSize: 16,

                    color: Color(0xffbcc5d3),


                  ),
                ),
              ],
            ),
          ),
        ) : Container()
      ],
    );
  }

  Widget _buildQuizYesNo(QuizResult list) {
    return Column(
      children: [
        Padding(
          child: _buildQuizTitle(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ),
        Padding(
          child: _buildSelection(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ),
      ],
    );
  }

  Widget _buildQuizTitle(QuizResult list) {
    List<QuizInfo> info = list.result;
    List<AnswerInfo> choice = info[current].ansinfo;
    if (info[current].question == null) {
      return const SizedBox();
    }
    return /*Text(
                              info[current].question,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color.fromARGB(255, 196, 64, 62),
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 20,
                              ),
                            );*/

      Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 25),
            child: Text(
              'วันนี้',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Kanit',
                fontSize: 20,
                fontWeight: FontWeight.w300,
                color: Color(0xfff37b7d),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: 328,
              margin: EdgeInsets.only(top: 15, bottom: 40),
              child: Text(
                info[current].question,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColors.accentText,
                  fontFamily: "Kanit",
                  fontWeight: FontWeight.w400,
                  fontSize: 25,
                  height: 1.2,
                ),
              ),
            ),
          ),
        ],
      );
  }

  Widget _buildSelection(QuizResult list) {
    List<QuizInfo> info = list.result;
    List<AnswerInfo> choice = info[current].ansinfo;

    var type = info[current].type;

    if (type == "choice_four") {
      return Column(children: [
        choice[0].ans1 != "" ? choiceGen(choice[0].ans1, "DCDCEF") : Text(""),
        choice[1].ans2 != "" ? choiceGen(choice[1].ans2, "DCDCEF") : Text(""),
        choice[2].ans3 != "" ? choiceGen(choice[2].ans3, "DCDCEF") : Text(""),
        choice[3].ans4 != "" ? choiceGen(choice[3].ans4, "DCDCEF") : Text(""),
        choice[4].ans5 != "" ? choiceGen(choice[4].ans5, "DCDCEF") : Text(""),
        choice[5].ans6 != "" ? choiceGen(choice[5].ans6, "DCDCEF") : Text("")
      ]);
    } else if (type == "choice_number") {
      /*return Column(
          children:  [
          choice[0].ans1 != null ? choiceGen(choice[0].ans1,"FDEFEF"):Text(""),
          choice[1].ans2 != null ? choiceGen(choice[1].ans2,"FDEFEF"):Text(""),
          ]);*/

      return choiceGenNumber("DCDCEF");
    } else {
      return choiceGenYesNo("DCDCEF");
    }

    /* return Column(
      children:  [
          choice[0].ans1 != null ? choiceGen(choice[0].ans1,"FDEFEF"):Text(""),
          choice[1].ans2 != null ? choiceGen(choice[1].ans2,"FDEFEF"):Text(""),
          choice[2].ans3 != null ? choiceGen(choice[2].ans3,"FDEFEF"):Text(""),
          choice[3].ans4 != null ? choiceGen(choice[3].ans4,"FDEFEF"):Text(""),
          choice[4].ans5 != null ? choiceGen(choice[4].ans5,"FDEFEF"):Text(""),
          choice[5].ans6 != null ? choiceGen(choice[5].ans6,"FDEFEF"):Text("")*/

    /*RaisedButton(
            child: Text("test1"),
            onPressed: () {
               if( this.total-1  ==  this.current){
                 setState((){
                  this.prog_current =  this.total;
                 });

               }else{
                setState((){
                     this.current =  this.current +1;
                      this.prog_current =  this.current ;
                });
               }

              //this.current =  this.current +1;
              //model.answer(widget);
            },
          ),*/

    /* , new RaisedButton(
            child: Text("test2"),
            onPressed: () {
              //model.answer(widget);
            },
          ),
          new RaisedButton(
            child: Text("test3"),
            onPressed: () {
              //model.answer(widget);
            },
          ),*/
    //  ]);
  }

  _saveData(int current) async {
    /*SharedPreferences prefs = await SharedPreferences.getInstance();
    print("save $current");
    prefs.setInt("startq3", current);*/

    updateQuiz("$current", "3");
  }

  Widget choiceGen(String str, String color) {
    return new InkWell(
      onTap: () {
        if (this.total - 1 == this.current) {
          setState(() {
            _saveData(this.total);
            this.prog_current = this.total;
            Route route = MaterialPageRoute(builder: (context) => QuizIn2Finish());
            Navigator.pushReplacement(context, route);
          });
        } else {
          setState(() {
            this.current = this.current + 1;
            this.prog_current = this.current;
            _saveData(this.prog_current);
          });
        }
      },
      child: Container(
          height: 50,
          margin: EdgeInsets.only(top: 10, right: 2),
          decoration: BoxDecoration(
            color: HexColor(color),
            borderRadius: Radii.k10pxRadius,
          ),
          child: Container(
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text(
                    str,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: HexColor("4C5264"),
                      fontFamily: "Kanit",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  )),
            ),
          )),
    );
  }

  Widget choiceGenYesNo(String color) {
    return InkWell(
        onTap: () {
          if (this.total - 1 == this.current) {
            setState(() {
              _saveData(this.total);
              this.prog_current = this.total;
              Route route =
              MaterialPageRoute(builder: (context) => QuizIn2Finish());
              Navigator.pushReplacement(context, route);
            });
          } else {
            setState(() {
              this.current = this.current + 1;
              this.prog_current = this.current;
              _saveData(this.prog_current);
            });
          }
        },
        child: Column(children: [
          Padding(
            padding: EdgeInsets.only(top: 70),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                      margin: EdgeInsets.only(left: 19),
                      width: 138,
                      height: 70,
                      decoration: BoxDecoration(
                        color: HexColor(color),
                        borderRadius: Radii.k10pxRadius,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "ใช่",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: HexColor("801E1C"),
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                          )
                        ],
                      )),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                      margin: EdgeInsets.only(right: 19),
                      width: 138,
                      height: 70,
                      decoration: BoxDecoration(
                        color: HexColor(color),
                        borderRadius: Radii.k10pxRadius,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "ไม่ใช่",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: HexColor("801E1C"),
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                          )
                        ],
                      )),
                )
              ],
            ),
          )
        ]));
  }

  Widget choiceGenNumber(String color) {
    return new Column(children: [
      Padding(
        padding: EdgeInsets.only(top: 50),
        child: Row(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: 50,
                        margin: EdgeInsets.only(top: 91),
                        child: Stack(alignment: Alignment.center, children: [
                          Positioned(
                              left: 0,
                              child: Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 243, 123, 125),
                                  borderRadius: Radii.k10pxRadius,
                                ),
                                child: InkWell(
                                  onTap: () {
                                    mainBloc.decrementCounter(this);
                                    //Route route = MaterialPageRoute(builder: (context) => QuizIn2Page());
                                    //Navigator.pop(context);
                                  },
                                  child: Text(
                                    "-",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: HexColor("FFFFFF"),
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 30,
                                    ),
                                  ),
                                ),
                              )),
                          Positioned(
                            child: Container(
                              width: 197,
                              height: 50,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 237, 243, 248),
                                borderRadius: Radii.k10pxRadius,
                              ),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    TextField(
                                      textAlignVertical:
                                      TextAlignVertical.center,
                                      textAlign: TextAlign.center,
                                      controller: TextEditingController()
                                        ..text = "${mainBloc.counter1}",
                                      maxLines: 1,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration.collapsed(
                                          hintText: "",
                                          hintStyle: TextStyle(
                                            fontFamily: 'Kanit',
                                            fontSize: 16,
                                            color: Color(0xffbcc5d3),
                                          )),
                                      style: new TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 20,
                                        fontWeight: FontWeight.w400,
                                        color: Color.fromARGB(255, 196, 64, 62),
                                      ),
                                      onChanged: (text) {
                                        // _subject = text;
                                      },
                                    )
                                  ]),
                            ),
                          ),
                          Positioned(
                            right: 0,
                            child: Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 243, 123, 125),
                                  borderRadius: Radii.k10pxRadius,
                                ),
                                child: InkWell(
                                  onTap: () {
                                    // setState(() {
                                    //this.number = this.number +1;
                                    // });
                                    mainBloc.incrementCounter(this);
                                    //Route route = MaterialPageRoute(builder: (context) => QuizIn2Page());
                                    //Navigator.pop(context);
                                  },
                                  child: Text(
                                    "+",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: HexColor("FFFFFF"),
                                      fontFamily: "Kanit",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 30,
                                    ),
                                  ),
                                )),
                          ),
                        ])))
              ],
            ),
          ],
        ),
      ),
      InkWell(
          onTap: () {
            if (this.total - 1 == this.current) {
              setState(() {
                _saveData(this.total);
                this.prog_current = this.total;
                Route route =
                MaterialPageRoute(builder: (context) => QuizIn2Finish());
                Navigator.pushReplacement(context, route);
              });
            } else {
              setState(() {
                this.current = this.current + 1;
                this.prog_current = this.current;
                _saveData(this.prog_current);
              });
            }
          },
          child: Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(top: 47),
              width: 315,
              height: 50,
              decoration: BoxDecoration(
                color: HexColor("F8B6B8"),
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "ถัดไป",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color.fromARGB(255, 94, 69, 137),
                      fontFamily: "Kanit",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  )
                ],
              ),
            ),
          ))
    ]);
  }
}
