import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';

class MoreInfo extends StatefulWidget {
  @override
  _MoreInfoState createState() => _MoreInfoState();
}

class _MoreInfoState extends State<MoreInfo> {

  String kNavigationExamplePage = '''
<!DOCTYPE html><html>
<head><title>Navigation Delegate Example</title></head>
<body>
<p>
The navigation delegate is set to block navigation to the youtube website.
</p>
<ul>
<ul><a href="https://www.youtube.com/">https://www.youtube.com/</a></ul>
<ul><a href="https://www.google.com/">https://www.google.com/</a></ul>
</ul>
</body>
</html>
''';


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        resizeToAvoidBottomPadding: false ,

        appBar: AppBar(
          leading: BackButton(
              color: Colors.black
          ),
          backgroundColor: HexColor("FFFFFF"),
          title:  Container(
            child: const Text(
              'นโยบายความเป็นส่วนตัว',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Kanit',
                fontSize: 20,
                color: Color(0xff000000),
              ),
            ),
          ),
        ),
        body: ListView(
            children: <Widget>[

              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children:<Widget>[Padding(
                  padding: const EdgeInsets.only(left:18.0,top: 20),
                  child: const Text(
                  'นโยบายความเป็นส่วนตัว',

                  style: TextStyle(
                    fontFamily: 'Kanit',
                    fontSize: 22,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff4c5264),


                  ),
              ),
                ),Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Container(child:const Text(
                  'เพื่อเป็นอุปกรณ์ช่วยเหลือผู่ป่วยยังไม่ทราบแน่ชัด สามารถเกิดได้ทั้งในลำไส้ใหญ่และลำไส้เล็ก โดยมีโรคที่เกี่ยวข้องอยู่ 2 โรค คือ Crohn’s disease และ ulcerative colitis ซึ่งเป็นโรคที่พบได้บ่อยในชาวตะวันตกและตะวันออกกลาง ส่วนชาวเอเชียพบได้บ้างแต่ยังน้อยอยู่มาก\n\nยังไม่ทราบแน่ชัด สามารถเกิดได้ทั้งในลำไส้ใหญ่และลำไส้เล็ก โดยมีโรคที่เกี่ยวข้องอยู่ 2 โรค คือ Crohn’s disease และ ulcerative colitis ซึ่งเป็นโรคที่พบได้บ่อยในชาวตะวันตกและตะวันออกกลาง ส่วนชาวเอเชียพบได้บ้างแต่ยังน้อยอยู่มาก\n\nเพื่อเป็นอุปกรณ์ช่วยเหลือผู่ป่วยยังไม่ทราบแน่ชัด สามารถเกิดได้ทั้งในลำไส้ใหญ่และลำไส้เล็ก โดยมีโรคที่เกี่ยวข้องอยู่ 2 โรค คือ Crohn’s disease และ ulcerative colitis ซึ่งเป็นโรคที่พบได้บ่อยในชาวตะวันตกและตะวันออกกลาง ส่วนชาวเอเชียพบได้บ้างแต่ยังน้อยอยู่มาก\n\nยังไม่ทราบแน่ชัด สามารถเกิดได้ทั้งในลำไส้ใหญ่และลำไส้เล็ก โดยมีโรคที่เกี่ยวข้องอยู่ 2 โรค คือ Crohn’s disease และ ulcerative colitis ซึ่งเป็นโรคที่พบได้บ่อยในชาวตะวันตกและตะวันออกกลาง ส่วนชาวเอเชียพบได้บ้างแต่ยังน้อยอยู่มาก',

                  style: TextStyle(
                    fontFamily: 'Kanit',
                    fontSize: 16,

                    color: Color(0xff4c5264),


                  ),
              ),),
                )],)



            ]
        ) //
    );
  }
}
