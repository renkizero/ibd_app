import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqlcool/sqlcool.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  var firstname;
  var lastname;
  var email;
  var tel;
  var gender;
  var birthday;

  SelectBloc bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.bloc = SelectBloc(database: db, table: "users", reactive: true);

    _getData();
  }

  _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString("email"));

    this.setState(() {
      this.setState(() async {
        try {
          List<Map<String, dynamic>> rows = await db.select(
            table: "users",
            where: "id  = 1",
          );
          print(rows[0]);
          this.firstname = rows[0]["firstname"];
          this.lastname = rows[0]["lastname"];

          this.email = rows[0]["email"];
          this.tel = rows[0]["phone"];
          var gen = rows[0]["gender"];
          if (gen == "นาย") {
            this.gender = "ชาย";
          } else {
            this.gender = "หญิง";
          }
          this.birthday = prefs.getString("birthday") ?? '-';
        } catch (e) {
          rethrow;
        }

        // this.firstname = prefs.getString("firstname");
        // this.lastname = prefs.getString("lastname");
        // print(this.email);
      });

      // print(this.email);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: BackButton(color: Colors.black),
          backgroundColor: HexColor("FFFFFF"),
          title: Container(
            child: const Text(
              'ข้อมูลส่วนตัว',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Kanit',
                fontSize: 20,
                color: Color(0xff000000),
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) {},
                ),
              ),
              child: Text(
                'แก้ไข',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Kanit',
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                  color: Color(0xff000000),
                ),
              ),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
        ),
        body: ListView(children: <Widget>[
          Container(
            child: StreamBuilder<List<Map>>(
                stream: this.bloc.items,
                builder: (context, snapshot) {
                 // print(snapshot.hasData);
                  if (snapshot.hasData &&
                      snapshot.data != null &&
                      snapshot.data.length > 0) {
                    return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  margin: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 1.0),
                                  child: Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      width: 196,
                                      height: 164,
                                      child: Image.asset(
                                        "assets/images/path-1967.png",
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.topCenter,
                                      child: Container(
                                        width: 102,
                                        height: 102,
                                        child: Image.asset(
                                          "assets/images/group-1755.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 9),
                                      child: Text(
                                        "$firstname $lastname",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: HexColor("000000"),
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 20,
                                          letterSpacing: -0.2,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 20,
                            child: Container(
                              width: 120,
                              height: 1,
                              margin: EdgeInsets.only(left: 19, top: 2),
                              child: Row(
                                children: [],
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.9,
                                height: 1,
                                color: const Color(0xffe2e8ed),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      'ชื่อ',
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xffbcc5d3),
                                      ),
                                    ),
                                    Text(
                                      "$firstname",
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xff000000),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height: 1,
                                  color: const Color(0xffe2e8ed),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      'นามสกุล',
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xffbcc5d3),
                                      ),
                                    ),
                                    Text(
                                      "$lastname",
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xff000000),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height: 1,
                                  color: const Color(0xffe2e8ed),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      'วันเกิด',
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xffbcc5d3),
                                      ),
                                    ),
                                    Text(
                                      "$birthday",
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xff000000),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height: 1,
                                  color: const Color(0xffe2e8ed),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      'เพศ',
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xffbcc5d3),
                                      ),
                                    ),
                                    Text(
                                      "$gender",
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xff000000),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height: 1,
                                  color: const Color(0xffe2e8ed),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      'อีเมล',
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xffbcc5d3),
                                      ),
                                    ),
                                    Text(
                                      "$email",
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xff000000),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height: 1,
                                  color: const Color(0xffe2e8ed),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      'เบอร์โทรศัพท์',
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xffbcc5d3),
                                      ),
                                    ),
                                    Text(
                                      "$tel",
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        color: Color(0xff000000),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 30, right: 30),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height: 1,
                                  color: const Color(0xffe2e8ed),
                                ),
                              ),
                            ],
                          ), /*Center(
                            child: Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Container(
                              width: 335,
                              height: 50,
                              decoration: BoxDecoration(
                                color: const Color(0xfff8b6b8),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: InkWell(
                                onTap: () async {
                                  //_onChanged(_subject, _note);

                                  Navigator.pop(context);
                                },
                                child: Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'บันทึกข้อมูล',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: 'Kanit',
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff5e4589),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                      ),
                          ),*/
                        ]);
                  } else {
                    return Container();
                  }
                }),
          )
        ]) //
        );
  }
}
