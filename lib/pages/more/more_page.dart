import 'package:flutter/material.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:ibd_application/pages/home/noti_page.dart';
import 'package:ibd_application/pages/login/login_page.dart';
import 'package:ibd_application/pages/more/edit_info.dart';
import 'package:ibd_application/pages/more/edit_profile.dart';
import 'package:ibd_application/pages/more/more_info.dart';
import 'package:ibd_application/pages/more/more_info1.dart';
import 'package:ibd_application/pages/register/register_page1.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:url_launcher/url_launcher.dart';

import 'dbviewer.dart';

class MorePage extends StatefulWidget {
  const MorePage({Key key}) : super(key: key);

  static const routeName = '/more';
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  var firstname;
  var lastname;
  SelectBloc  bloc;
  @override
  void initState() {
    // TODO: implement initState

    this.bloc = SelectBloc(
        database: db,
        table: "users",
        reactive: true);

    //_getData();
    super.initState();

  }

  Future<void> _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString("email"));

    this.setState(() async {
      try {
        List<Map<String, dynamic>> rows = await db.select(
          table: "users",
          where: "id  = 1",
        );
        print(rows[0]);
        this.firstname = rows[0]["firstname"];
        this.lastname = rows[0]["firstname"];
      } catch (e) {
        rethrow;
      }

     // this.firstname = prefs.getString("firstname");
     // this.lastname = prefs.getString("lastname");
      // print(this.email);
    });
  }

  _launchURL(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: HexColor("FFFFFF"),
        title: const Text(
          'More',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Kanit',
            fontSize: 24,
            color: Color(0xff000000),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).push(MaterialPageRoute<void>(
              builder: (BuildContext context) {
                return new NotiPage();
              },
            )),
            child: Stack(
              children: <Widget>[
                Container(
                  width: 45,
                  height: 45,
                  child: Align(
                    child: Image.asset(
                      "assets/images/notifications.png",
                      fit: BoxFit.none,
                    ),
                  ),
                  decoration: BoxDecoration(),
                ),
                Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 22.0),
                      child: Container(
                        width: 25,
                        height: 25,
                        decoration: BoxDecoration(
                          color: const Color(0xffc4403e),
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30.0, top: 2),
                      child: Text(
                        '1',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Kanit',
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Color(0xffffffff),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
      body: ListView(children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 30),
              child: Stack(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 1.0),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 196,
                        height: 164,
                        child: Image.asset(
                          "assets/images/path-1967.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 102,
                          height: 102,
                          child: Image.asset(
                            "assets/images/group-1755.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                      StreamBuilder<List<Map>>(
                        stream: bloc.items,
                        builder: (context, snapshot) {
                          print(snapshot.data);
                          if (snapshot.hasData &&  snapshot.data != null && snapshot.data.length > 0) {
                            var item = snapshot.data;
                            firstname = item[0]["firstname"];
                            lastname = item[0]["lastname"];
                            return Container(
                              margin: EdgeInsets.only(top: 9),
                              child: Text(
                                "$firstname $lastname",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: HexColor("000000"),
                                  fontFamily: "Kanit",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 20,
                                  letterSpacing: -0.2,
                                ),
                              ),
                            );
                          }else{
                            return Container();
                          }

                        }
                      )
                    ],
                  )
                ],
              ),
            ),
            Container(
              height: 70,
              child: Container(
                width: 120,
                height: 27,
                margin: EdgeInsets.only(left: 19, top: 9),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "โปรไฟล์",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: HexColor("000000"),
                          fontFamily: "Kanit",
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          letterSpacing: -0.2,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: 40,
              child: Container(
                width: 120,
                height: 27,
                margin: EdgeInsets.only(left: 19, top: 9),
                child: Row(
                  children: [
                    Container(
                      width: 24,
                      height: 27,
                      child: Image.asset(
                        "assets/images/accept-call-icon.png",
                        fit: BoxFit.none,
                      ),
                    ),
                    Spacer(),
                    InkWell(
                      onTap: () async {
                        Navigator.push(
                            context,
                            // MaterialPageRoute(builder: (context) => AppointMentAddPage())),
                            MaterialPageRoute(
                                builder: (context) => EditProfile()));
                      },
                      child: Text(
                        "ข้อมูลส่วนตัว",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: HexColor("4C5264"),
                          fontFamily: "Kanit",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            ),
            Container(
              height: 40,
              child: InkWell(
                onTap: () async {
                  Navigator.push(
                      context,
                      // MaterialPageRoute(builder: (context) => AppointMentAddPage())),
                      MaterialPageRoute(builder: (context) => EditInfo()));
                },
                child: Container(
                  width: 140,
                  height: 27,
                  margin: EdgeInsets.only(left: 19, top: 9),
                  child: Row(
                    children: [
                      Container(
                        width: 24,
                        height: 27,
                        child: Image.asset(
                          "assets/images/path-1303.png",
                          fit: BoxFit.none,
                        ),
                      ),
                      Spacer(),
                      Text(
                        "ประวัติการรักษา",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: HexColor("4C5264"),
                          fontFamily: "Kanit",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            ),
            Container(
              height: 70,
              child: Container(
                width: 130,
                height: 27,
                margin: EdgeInsets.only(left: 19, top: 9),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "การตั้งค่า",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: HexColor("000000"),
                          fontFamily: "Kanit",
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          letterSpacing: -0.2,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: 40,
              child: Container(
                width: 100,
                height: 25,
                margin: EdgeInsets.only(left: 19, top: 9),
                child: Row(
                  children: [
                    Container(
                      width: 20,
                      height: 20,
                      child: Image.asset(
                        "assets/images/invite.png",
                        fit: BoxFit.none,
                      ),
                    ),
                    Spacer(),
                    Container(
                      child: InkWell(
                        onTap: () async {
                          _launchURL("admin@ibd.com", "", "");
                        },
                        child: Text(
                          "ติดต่อเรา",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: HexColor("4C5264"),
                            fontFamily: "Kanit",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            ),
            Container(
              height: 40,
              child: Container(
                width: 180,
                height: 25,
                margin: EdgeInsets.only(left: 19, top: 9),
                child: Row(
                  children: [
                    Container(
                      width: 20,
                      height: 20,
                      child: Image.asset(
                        "assets/images/icon-setting.png",
                        fit: BoxFit.none,
                      ),
                    ),
                    Spacer(),
                    Text(
                      "ตั้งค่าความเป็นส่วนตัว",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: HexColor("4C5264"),
                        fontFamily: "Kanit",
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            ),
            Container(
              height: 40,
              child: InkWell(
                onTap: () async {
                  Navigator.push(
                      context,
                      // MaterialPageRoute(builder: (context) => AppointMentAddPage())),
                      MaterialPageRoute(builder: (context) => MoreInfo()));
                },
                child: Container(
                  width: 195,
                  height: 25,
                  margin: EdgeInsets.only(left: 19, top: 9),
                  child: Row(
                    children: [
                      Container(
                        width: 21,
                        height: 24,
                        child: Image.asset(
                          "assets/images/group-2022.png",
                          fit: BoxFit.none,
                        ),
                      ),
                      Spacer(),
                      Text(
                        "นโยบายความเป็นส่วนตัว",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: HexColor("4C5264"),
                          fontFamily: "Kanit",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            ),
            Container(
              height: 40,
              child: InkWell(
                onTap: () async {
                  Navigator.push(
                      context,
                      // MaterialPageRoute(builder: (context) => AppointMentAddPage())),
                      MaterialPageRoute(builder: (context) => More1Info()));
                },
                child: Container(
                  width: 179,
                  height: 25,
                  margin: EdgeInsets.only(left: 19, top: 9),
                  child: Row(
                    children: [
                      Container(
                        width: 21,
                        height: 24,
                        child: Image.asset(
                          "assets/images/pending-shipments.png",
                          fit: BoxFit.none,
                        ),
                      ),
                      Spacer(),
                      Text(
                        "ข้อกำหนดและเงื่อนไข",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: HexColor("4C5264"),
                          fontFamily: "Kanit",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            ),
            Container(
              height: 40,
              child: Container(
                width: 131,
                height: 26,
                margin: EdgeInsets.only(left: 19, top: 9),
                child: Row(
                  children: [
                    Container(
                      width: 18,
                      height: 20,
                      child: Image.asset(
                        "assets/images/group-2162.png",
                        fit: BoxFit.none,
                      ),
                    ),
                    Spacer(),
                    Text(
                      "แก้ไขรหัสผ่าน",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: HexColor("4C5264"),
                        fontFamily: "Kanit",
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            ),
            Container(
              height: 40,
              child: Container(
                width: 131,
                height: 26,
                margin: EdgeInsets.only(left: 19, top: 9),
                child: Row(
                  children: [
                    Container(
                      width: 20,
                      height: 21,
                      child: Image.asset(
                        "assets/images/sign-out-icon.png",
                        fit: BoxFit.none,
                      ),
                    ),
                    Spacer(),
                    InkWell(
                        onTap: () async {
                          _popupDialog(context);
                        },
                        child: Text(
                          "ออกจากระบบ",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: HexColor("4C5264"),
                            fontFamily: "Kanit",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                          ),
                        )),
                  ],
                ),
              ),
            ),
            Container(
              height: 2,
              margin: EdgeInsets.only(left: 19, top: 10, right: 19),
              decoration: BoxDecoration(
                color: HexColor("E2E8ED"),
              ),
              child: Container(),
            )
          ],
        )
      ]),
    );
  }

  void _popupDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Alert'),
            content: Text('ต้องการออกจากระบบ'),
            actions: <Widget>[
              FlatButton(onPressed: () => _delPref(), child: Text('ตกลง')),
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('ยกเลิก')),
            ],
          );
        });
  }

  Future<void> _delPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("register", 1);

    Route route = MaterialPageRoute(builder: (context) => RegisterFirstPage());
    Navigator.pushAndRemoveUntil(context, route, (route) => false);
  }
}
