import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqlcool/sqlcool.dart';

class EditInfo extends StatefulWidget {
  @override
  _EditInfoState createState() => _EditInfoState();
}

class _EditInfoState extends State<EditInfo> {
  var doctor;
  var hopistal;
  var user_choice;
  var probiotic;
  var firstname;
  var lastname;

  SelectBloc blocPInfo;
  SelectBloc bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.bloc = SelectBloc(database: db, table: "users", reactive: true);

    this.blocPInfo =
        SelectBloc(database: db, table: "paintinfo", reactive: true);

    _getData();
  }

  _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString("email"));

    this.setState(() {
      this.firstname = prefs.getString("firstname") ?? "";
      this.lastname = prefs.getString("lastname") ?? "";
      this.doctor = prefs.getString("doctor") ?? "";
      this.hopistal = prefs.getString("hopistal") ?? "";
      this.user_choice = prefs.getString("user_choice") ?? "";
      this.probiotic = prefs.getString("probiotic") ?? "";

      // print(this.email);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: BackButton(color: Colors.black),
          backgroundColor: HexColor("FFFFFF"),
          title: Container(
            child: const Text(
              'ประวัติการรักษา',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Kanit',
                fontSize: 20,
                color: Color(0xff000000),
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) {},
                ),
              ),
              child: Text(
                'แก้ไข',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Kanit',
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                  color: Color(0xff000000),
                ),
              ),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
        ),
        body: ListView(children: <Widget>[
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 1.0),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            width: 196,
                            height: 164,
                            child: Image.asset(
                              "assets/images/path-1967.png",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: 102,
                              height: 102,
                              child: Image.asset(
                                "assets/images/group-1755.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                          Container(
                            child: StreamBuilder<List<Map>>(
                                stream: bloc.items,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData &&
                                      snapshot.data != null &&
                                      snapshot.data.length > 0) {

                                    var items =  snapshot.data;
                                    this.firstname = items[0]["firstname"];
                                    this.lastname = items[0]["lastname"];
                                    return Container(
                                      margin: EdgeInsets.only(top: 9),
                                      child: Text(
                                        "$firstname $lastname",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: HexColor("000000"),
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 20,
                                          letterSpacing: -0.2,
                                        ),
                                      ),
                                    );
                                  } else {
                                    return Container();
                                  }
                                }),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  height: 20,
                  child: Container(
                    width: 120,
                    height: 1,
                    margin: EdgeInsets.only(left: 19, top: 2),
                    child: Row(
                      children: [],
                    ),
                  ),
                ),
                Container(
                  child: StreamBuilder<List<Map>>(
                    stream: blocPInfo.items,
                    builder: (context, snapshot) {
    if (snapshot.hasData &&
    snapshot.data != null &&
    snapshot.data.length > 0) {

      var items = snapshot.data;
      this.hopistal = items[0]["hospital"];
      this.doctor = items[0]["doctor"];
      this.user_choice = items[0]["choice"];
      this.probiotic = items[0]["med"];


      return Column(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 1,
                color: const Color(0xffe2e8ed),
              ),
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      'โรงพยาบาล',
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xffbcc5d3),
                      ),
                    ),
                    Text(
                      "$hopistal",
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xff000000),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 1,
                  color: const Color(0xffe2e8ed),
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      'คุณหมอเจ้าของไข้',
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xffbcc5d3),
                      ),
                    ),
                    Text(
                      "$doctor",
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xff000000),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 1,
                  color: const Color(0xffe2e8ed),
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      'โรคประจำตัว',
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xffbcc5d3),
                      ),
                    ),
                    Text(
                      "$user_choice",
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xff000000),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 1,
                  color: const Color(0xffe2e8ed),
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      'ข้อมูลยาที่ใช้',
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xffbcc5d3),
                      ),
                    ),
                    Text(
                      "$probiotic",
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,
                        color: Color(0xff000000),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.only(top: 12.0, left: 30, right: 30),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 1,
                  color: const Color(0xffe2e8ed),
                ),
              ),
            ],
          ),],

      );
    }else{
      return Container();
    }
                    }
                  ),
                ),
                Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 70),
                    child: Container(
                        width: 335,
                        height: 50,
                        decoration: BoxDecoration(
                          color: const Color(0xfff8b6b8),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: InkWell(
                          onTap: () async {
                            //_onChanged(_subject, _note);

                            Navigator.pop(context);
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'บันทึกข้อมูล',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: 'Kanit',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff5e4589),
                                ),
                              ),
                            ],
                          ),
                        )),
                  ),
                ),
              ])
        ]) //
        );
  }
}
