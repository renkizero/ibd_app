import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'package:ibd_application/util.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckingFinish extends StatefulWidget {
  @override
  _CheckingFinishState createState() => _CheckingFinishState();
}

class _CheckingFinishState extends State<CheckingFinish> {

  var emo1 = 0;
  var emo2 = 0;
  var emo3 = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getData();
  }

  _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString("email"));

    this.setState(() {
      this.emo1 = prefs.getInt("user_emo_1") ?? 0;
      this.emo2 = prefs.getInt("user_emo_2") ?? 0;
      this.emo3 = prefs.getInt("user_emo_3") ?? 0;
     //  print(this.emo1);
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
              color: HexColor("C4C4E0"),
            ),
            child: Container(
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                color: HexColor("C4C4E0"),
              ),
              child: Column(
                children: [
                  InkWell(
                      onTap: () {
                        //Route route = MaterialPageRoute(builder: (context) => QuizPage());
                        Navigator.pop(context);
                      },
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          width: 17,
                          height: 17,
                          margin: EdgeInsets.only(top: 42, right: 9),
                          child: Image.asset(
                            "assets/images/group-2039.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      )),
                  Container(
                    width: 179,
                    height: 179,
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        Positioned(
                          top: 72,
                          child: Image.asset(
                            "assets/images/Group_2029.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 315,
                    height: 61,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 0,
                          child: Text(
                            "คุณได้ทำแบบสอบถามเสร็จเรียบร้อยแล้ว",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: HexColor("4C5264"),
                              fontFamily: "Kanit",
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: HexColor("FFFFFF"),
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    width: 315,
                    height: 180,
                    margin: EdgeInsets.only(top: 18),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            width: 100,
                            height: 120,
                            child: Column(children: [
                              CircularPercentIndicator(
                                radius: 80.0,
                                lineWidth: 10.0,
                                percent: (emo1/100),
                                center: Text(
                                  "$emo1%",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: HexColor("5E4589"),
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20,
                                  ),
                                ),
                                circularStrokeCap: CircularStrokeCap.round,
                                backgroundColor: Colors.white,
                                linearGradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    HexColor("BB3939"),
                                    HexColor("BB3939")
                                  ],
                                ),
                              ),
                              Text("สุขภาพ",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: HexColor("5E4589"),
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ))
                            ])),
                        Container(
                            width: 100,
                            height: 120,
                            child: Column(children: [
                              CircularPercentIndicator(
                                radius: 80.0,
                                lineWidth: 10.0,
                                percent: (emo2/100),
                                center: Text(
                                  "$emo2%",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: HexColor("5E4589"),
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20,
                                  ),
                                ),
                                circularStrokeCap: CircularStrokeCap.round,
                                backgroundColor: Colors.white,
                                linearGradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    HexColor("7E61B0"),
                                    HexColor("7E61B0")
                                  ],
                                ),
                              ),
                              Text("ความตึงเครียด",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: HexColor("5E4589"),
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ))
                            ])),
                        Container(
                            width: 100,
                            height: 120,
                            child: Column(children: [
                              CircularPercentIndicator(
                                radius: 80.0,
                                lineWidth: 10.0,
                                percent: (emo3/100),
                                center: Text(
                                  "$emo3%",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: HexColor("5E4589"),
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20,
                                  ),
                                ),
                                circularStrokeCap: CircularStrokeCap.round,
                                backgroundColor: Colors.white,
                                linearGradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    HexColor("6BB546"),
                                    HexColor("6BB546")
                                  ],
                                ),
                              ),
                              Text("ความเหนื่อย",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: HexColor("5E4589"),
                                    fontFamily: "Kanit",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ))
                            ]))
                      ],
                    ),
                  ), Padding(
                    padding: const EdgeInsets.only(top:18.0),
                    child: Text(
                      'สุขภาพคุณวันนี้อยู่ในเกณฑ์ไม่ค่อยดีนัก',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Kanit',
                        fontSize: 16,

                        color: Color(0xff4c5264),


                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      margin: EdgeInsets.only(top: 27),
                      width: 315,
                      height: 50,
                      decoration: BoxDecoration(
                        color: HexColor("F8B6B8"),
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                              onTap: () async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();

                                DateTime dateTimeNow = DateTime.now();
                                prefs.setInt('daliy', dateTimeNow.day);
                                //Route route = MaterialPageRoute(builder: (context) => QuizPage());
                                //Navigator.pop(context);

                                Route route =
                                MaterialPageRoute(builder: (context) => HomePage.wrapped());

                                Navigator.pushReplacement(context, route);
                              },
                              child: Text(
                                "สรุปผล",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 94, 69, 137),
                                  fontFamily: "Kanit",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                ),
                              ))
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}
