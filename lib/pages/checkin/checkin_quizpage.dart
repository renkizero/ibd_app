import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:ibd_application/bloc/checking_bloc.dart';
import 'package:ibd_application/bloc/checking_bloc1.dart';
import 'package:ibd_application/database.dart';
import 'package:ibd_application/models/entities/answer_info.dart';
import 'package:ibd_application/models/entities/entities.dart';
import 'package:ibd_application/pages/checkin/checin_fin.dart';
import 'package:ibd_application/pages/quiz/quiz_fin.dart';
import 'package:ibd_application/util.dart';
import 'package:ibd_application/values/values.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_logger/simple_logger.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class CheckQuizPage extends StatefulWidget {
  static const routeName = '/CheckQuizPage';

  const CheckQuizPage({Key key}) : super(key: key);

  @override
  _CheckQuizPageState createState() => _CheckQuizPageState();
}

class _CheckQuizPageState extends State<CheckQuizPage> {


  static const double _horizontalMargin = 16;
  int current = 0;
  int prog_current = 0;
  final logger = SimpleLogger();
  int total = 0;
  double _lowerValue = 50;
  double _upperValue = 180;
  double _sliderCurrent = 50;
  var number = 0;
  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: StreamBuilder<QuizResult>(
          stream: bloc.subject.stream,
          builder: (context, AsyncSnapshot<QuizResult> snapshot) {
            print(snapshot.hasData);
            if (snapshot.hasData) {
              QuizResult list = snapshot.data;

              return _buildQuizAll(list);
            } else {
              return _buildErrorWidget();
            }
          },
        ));
  }

  Widget choiceGen(String str, String color) {
    return new InkWell(
      onTap: () {
        if (this.total - 1 == this.current) {
          setState(() {
            this.prog_current = this.total;
            Route route =
                MaterialPageRoute(builder: (context) => CheckingFinish());
            Navigator.pushReplacement(context, route);
          });
        } else {
          setState(() {
            this.current = this.current + 1;
            this.prog_current = this.current;
          });
        }
      },
      child: Container(
          height: 50,
          margin: EdgeInsets.only(top: 10, right: 2),
          decoration: BoxDecoration(
            color: HexColor(color),
            borderRadius: Radii.k10pxRadius,
          ),
          child: Container(
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text(
                    str,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: HexColor("801E1C"),
                      fontFamily: "Kanit",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  )),
            ),
          )),
    );
  }

  Widget choiceGenNumber(String color) {
    return new Column(children: [
      Padding(
        padding: EdgeInsets.only(top: 1),
        child: Row(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: 70,
                        margin: EdgeInsets.only(top: 21),
                        child: Stack(alignment: Alignment.center, children: [
                          Positioned(
                            child: Container(
                              width: 350,
                              height: 70,
                              decoration: BoxDecoration(
                                borderRadius: Radii.k10pxRadius,
                              ),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Stack(children: [
                                      FlutterSlider(
                                        values: [this._sliderCurrent],
                                        rangeSlider: false,
                                        max: 100,
                                        min: 0,
                                        visibleTouchArea: false,
                                        handler: FlutterSliderHandler(
                                          decoration: BoxDecoration(),
                                          child: Material(
                                            type: MaterialType.circle,
                                            color: HexColor("75BE43"),
                                            elevation: 10,
                                            child: Container(
                                                padding: EdgeInsets.all(5)),
                                          ),
                                        ),
                                        trackBar: FlutterSliderTrackBar(
                                          inactiveTrackBarHeight: 14,
                                          activeTrackBarHeight: 14,
                                          inactiveTrackBar: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            color: HexColor("ECF1F5"),
                                          ),
                                          activeTrackBar: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(4),
                                              color: HexColor("ECF1F5")),
                                        ),
                                        onDragging: (handlerIndex, lowerValue,
                                            upperValue) {
                                          _lowerValue = lowerValue;
                                          _upperValue = upperValue;
                                          setState(() {
                                            this._sliderCurrent = _lowerValue;
                                            print(_lowerValue);

                                          });
                                        },
                                      ),
                                      Positioned.fill(
                                        child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Image.asset(
                                              "assets/images/Group_1531.png",
                                              fit: BoxFit.none,
                                            )),
                                      ),
                                      Positioned.fill(
                                        child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Image.asset(
                                              "assets/images/happy-icon.png",
                                              fit: BoxFit.none,
                                            )),
                                      ),
                                    ]),
                                  ]),
                            ),
                          )
                        ])))
              ],
            ),
          ],
        ),
      ),
      InkWell(
          onTap: () {
            if (this.total - 1 == this.current) {
              setState(() {
                var old =  this._sliderCurrent;
                _saveData(old,this.total);
                _saveDataDaliy(old,this.total,this.total);
                this.prog_current = this.total;
                Route route =
                    MaterialPageRoute(builder: (context) => CheckingFinish());
                Navigator.pushReplacement(context, route);
              });
            } else {
              setState(() {
                var old =  this._sliderCurrent;
                this._sliderCurrent = 50;
                this.current = this.current + 1;
                this.prog_current = this.current;
                _saveData(old,this.current);
                _saveDataDaliy(old,this.total,this.current);
              });
            }
          },
          child: Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(top: 47),
              width: 315,
              height: 50,
              decoration: BoxDecoration(
                color: HexColor("F8B6B8"),
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "ถัดไป",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color.fromARGB(255, 94, 69, 137),
                      fontFamily: "Kanit",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  )
                ],
              ),
            ),
          )),Padding(
            padding: const EdgeInsets.only(top:30.0),
            child: Center(

        child:  this.current > 0 ? InkWell(
            onTap: () async {
              setState(() {
                this.prog_current = this.prog_current - 1;
                this.current = this.current - 1;
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 18,
                  height: 18,
                  margin: EdgeInsets.only(right: 10),
                  child: Image.asset(
                    "assets/images/arrow-icon_2.png",
                    fit: BoxFit.none,
                  ),
                ),
                Text(
                  'ย้อนกลับ',

                  style: TextStyle(
                    fontFamily: 'Kanit',
                    fontSize: 16,

                    color: Color(0xffbcc5d3),


                  ),
                ),
              ],
            ),
        ) : Container(),
      ),
          )
    ]);
  }


  @override
  void dispose() {
    mainBloc = null; // destroying the mainBloc object to free resources
    super.dispose();
  }
  @override
  void initState() {
    bloc.getQuiz();
    mainBloc = CheckingBloc();
    super.initState();
  }

  Widget _buildErrorWidget() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Text("Error occured:"),
      ],
    ));
  }

  Widget _buildQuiz(QuizResult list) {
    return Column(
      children: [
        Padding(
          child: _buildQuizTitle(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ),
        Padding(
          child: _buildSelection(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ),
      ],
    );
  }

  Widget _buildQuizAll(QuizResult list) {
    List<QuizInfo> info = list.result;

    this.total = list.result.length;

    var type = info[current].type;

    var condition = 1;

    if (condition == 1) {
      return Scaffold(
          body:
              /*Center(
           child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StepProgressIndicator(
                      totalSteps: this.total,
                      currentStep: this.prog_current,
                      selectedColor: Colors.red,
                      unselectedColor: Colors.yellow,
                    ),
              _buildQuiz(list)
          ]
           )
         )
      );*/

              Container(
                  constraints: BoxConstraints.expand(),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment(0.5, 0),
                      end: Alignment(0.5, 1),
                      stops: [
                        0,
                        1,
                      ],
                      colors: [
                        HexColor("C4C4E0"),
                        HexColor("C4C4E0"),
                      ],
                    ),
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        InkWell(
                            onTap: () {
                              //Route route = MaterialPageRoute(builder: (context) => CheckQuizPage());
                              Navigator.pop(context);
                            },
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                width: 17,
                                height: 17,
                                margin: EdgeInsets.only(top: 42, right: 9),
                                child: Image.asset(
                                  "assets/images/group-2039.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            )),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: EdgeInsets.only(top: 1),
                            child: Text(
                              "Wellness Check-in",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.primaryText,
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: EdgeInsets.only(top: 1),
                            child: Text(
                              "",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.primaryText,
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 28,
                          margin: EdgeInsets.only(left: 18, top: 18, right: 19),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                height: 23,
                                margin: EdgeInsets.only(left: 2),
                                child: Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "TODAY",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: AppColors.primaryText,
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "${this.prog_current + 1} / ${this.total}",
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          color: AppColors.primaryText,
                                          fontFamily: "Kanit",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Spacer(),
                              Container(
                                height: 4,
                                margin: EdgeInsets.only(right: 1),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    StepProgressIndicator(
                                      padding: 0,
                                      totalSteps: this.total,
                                      currentStep: this.prog_current + 1,
                                      selectedColor: HexColor("F97B7D"),
                                      unselectedColor: HexColor("42296E"),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Container(
                                margin: EdgeInsets.only(top: 25),
                                child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Positioned(
                                        left: 0,
                                        top: 0,
                                        right: 0,
                                        child: Container(
                                          height: 633,
                                          decoration: BoxDecoration(
                                            color: AppColors.primaryBackground,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20)),
                                          ),
                                          child: _buildQuiz(list),
                                        ),
                                      ),
                                    ])))
                      ])));
    } else {
      return Text("Waiting");
    }
  }

  Widget _buildQuizTitle(QuizResult list) {
    List<QuizInfo> info = list.result;
    List<AnswerInfo> choice = info[current].ansinfo;
    if (info[current].question == null) {
      return const SizedBox();
    }
    var namepic = "assets/images/Group_2171.png";
    if (info[current].type == "medium") {
      namepic = "assets/images/Group_2633.png";
    } else if (info[current].type == "bad") {
      namepic = "assets/images/Group_2634.png";
    }
    return /*Text(
                              info[current].question,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color.fromARGB(255, 196, 64, 62),
                                fontFamily: "Kanit",
                                fontWeight: FontWeight.w400,
                                fontSize: 20,
                              ),
                            );*/

        Align(
      alignment: Alignment.topCenter,
      child: Container(
        margin: EdgeInsets.only(top: 25, bottom: 10),
        child: Column(
          children: <Widget>[
            Image.asset(
              namepic,
              fit: BoxFit.none,
            ),
            Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: Text(
                  info[current].question,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: AppColors.accentText,
                    fontFamily: "Kanit",
                    fontWeight: FontWeight.w400,
                    fontSize: 25,
                    height: 1.2,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _buildQuizYesNo(QuizResult list) {
    return Column(
      children: [
        Padding(
          child: _buildQuizTitle(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ),
        Padding(
          child: _buildSelection(list),
          padding: EdgeInsets.symmetric(horizontal: _horizontalMargin),
        ),
      ],
    );
  }

  Widget _buildSelection(QuizResult list) {
    List<QuizInfo> info = list.result;
    List<AnswerInfo> choice = info[current].ansinfo;

    var type = info[current].type;

    return choiceGenNumber("FDEFEF");

    /* return Column(
      children:  [
          choice[0].ans1 != null ? choiceGen(choice[0].ans1,"FDEFEF"):Text(""),
          choice[1].ans2 != null ? choiceGen(choice[1].ans2,"FDEFEF"):Text(""),
          choice[2].ans3 != null ? choiceGen(choice[2].ans3,"FDEFEF"):Text(""),
          choice[3].ans4 != null ? choiceGen(choice[3].ans4,"FDEFEF"):Text(""),
          choice[4].ans5 != null ? choiceGen(choice[4].ans5,"FDEFEF"):Text(""),
          choice[5].ans6 != null ? choiceGen(choice[5].ans6,"FDEFEF"):Text("")*/

    /*RaisedButton(
            child: Text("test1"),
            onPressed: () {
               if( this.total-1  ==  this.current){
                 setState((){
                  this.prog_current =  this.total;
                 });

               }else{
                setState((){
                     this.current =  this.current +1;
                      this.prog_current =  this.current ;
                });
               }

              //this.current =  this.current +1;
              //model.answer(widget);
            },
          ),*/

    /* , new RaisedButton(
            child: Text("test2"),
            onPressed: () {
              //model.answer(widget);
            },
          ),
          new RaisedButton(
            child: Text("test3"),
            onPressed: () {
              //model.answer(widget);
            },
          ),*/
    //  ]);
  }

  _saveData(double percent,int point) async {
    SharedPreferences prefs =
    await SharedPreferences.getInstance();
    var tmp = "user_emo_$point";
    print(tmp );
    prefs.setInt(tmp, percent.toInt());
  }

  _saveDataDaliy(double percent,int point,int type) async {
    SharedPreferences prefs =
    await SharedPreferences.getInstance();
    var d = DateTime.now();


    var day = d.day.toString().padLeft(2, '0');
    var month = d.month.toString().padLeft(2, '0');
    var year = d.year.toString().padLeft(2, '0');

    var save = "$day$month$year";
    var tmp = "$save:user_emo_$point";
    print(tmp );
    prefs.setInt(tmp, percent.toInt());

    saveWell(save, "$day", "$month", "$year", "$type",  "$percent");

  }
}
