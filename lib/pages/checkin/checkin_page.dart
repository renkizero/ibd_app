import 'dart:async';
import 'package:ibd_application/hexcolor.dart';
import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:intl/intl.dart' as intl;
import 'package:bezier_chart/bezier_chart.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibd_application/pages/checkin/checkin_quizpage.dart';
import 'package:jiffy/jiffy.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:toggle_switch/toggle_switch.dart';

class CheckInPage extends StatefulWidget {
  const CheckInPage({Key key}) : super(key: key);

  static const routeName = '/checkin';

  @override
  _CheckInPageState createState() => _CheckInPageState();
}

class _CheckInPageState extends State<CheckInPage> {
  DateTime fromDate;
  DateTime toDate;
  DateTime date1;
  DateTime date2;

  SelectBloc blocWell;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var _switch1 = true;
  var _switch2 = true;
  var _switch3 = true;

  int _currentSelection = 0;
  Map<int, Widget> _children = {
    0: Text('สัปดาห์'),
    1: Text('6 เดือน'),
  };
  final List<DataPoint> dt1 = [];
  final List<DataPoint> dt2 = [];
  final List<DataPoint> dt3 = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    fromDate = DateTime(2020, 04, 12);
    toDate = DateTime.now();

    this.blocWell = SelectBloc(
        database: db,
        table: "well",
        reactive: true);


  //   print(this.blocWell);

     _getDaliy();

    var dayOfWeek = 0;
    DateTime date = DateTime.now();
    var lastMonday = date.subtract(Duration(days: date.weekday - dayOfWeek)).toIso8601String();

    //print(lastMonday);
  }

  _getDaliy() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int daliy = prefs.getInt('daliy');
    DateTime dateTimeNow = DateTime.now();

    if (daliy != dateTimeNow.day) {
      startTime();
    }

  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));

    _refreshController.loadComplete();
  }

  startTime() async {
    var duration = new Duration(microseconds: 1000);
    return new Timer(duration, route);
  }

  route() {
    //Navigator.pop(
    //  context, MaterialPageRoute(builder: (context) => CheckQuizPage()));

    Navigator.push(
        context, MaterialPageRoute(builder: (context) => CheckQuizPage()));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              title: const _Title(),
              elevation: 0.0,
              actions: <Widget>[],
            ),
            body: SmartRefresher(
                enablePullDown: true,
                enablePullUp: true,
                header: WaterDropHeader(),
                footer: CustomFooter(
                  builder: (BuildContext context, LoadStatus mode) {
                    Widget body;
                    if (mode == LoadStatus.idle) {
                    } else if (mode == LoadStatus.loading) {
                    } else if (mode == LoadStatus.failed) {
                    } else if (mode == LoadStatus.canLoading) {
                    } else {}
                    return Container(
                      height: 55.0,
                      child: Center(child: body),
                    );
                  },
                ),
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: ListView(children: <Widget>[
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(children: <Widget>[
                        ToggleSwitch(
                            minWidth: MediaQuery.of(context).size.width * 0.4,
                            cornerRadius: 10,
                            activeBgColor: HexColor("8A6CBC"),
                            activeTextColor: HexColor("FFFFFF"),
                            inactiveBgColor: HexColor("E9DFFA"),
                            inactiveTextColor: HexColor("BCC5D3"),
                            labels: ['สัปดาห์', '6 เดือน'],
                            activeColors: [
                              HexColor("8A6CBC"),
                              HexColor("8A6CBC")
                            ],
                            onToggle: (index) {
                              print('switched to: $index');
                            }),
                        Padding(
                          padding: const EdgeInsets.only(top: 18.0),
                          child: Row(
                            children: <Widget>[
                              Image.asset(
                                'assets/images/path_l.png',
                                width: 40,
                                height: 35,
                              ),
                              Text(
                                '30 - 4 เมษายน 2563',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: 'Kanit',
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff5e4589),
                                ),
                              ),
                              Image.asset(
                                'assets/images/path_r.png',
                                width: 40,
                                height: 35,
                              )
                            ],
                          ),
                        )
                      ])
                    ],
                  ),
                  chat(),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 30),
                    child: Row(children: <Widget>[
                      Text(
                        'สัปดาห์ที่ผ่านมาของคุณ',
                        style: TextStyle(
                          fontFamily: 'Kanit',
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff000000),
                        ),
                      )
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 0, left: 1),
                    child: MergeSemantics(
                      child: ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 10,
                              height: 10,
                              decoration: BoxDecoration(
                                color: HexColor("C4403E"),
                                shape: BoxShape.circle,
                              ),
                            ),
                            Text(
                              ' สุขภาพ',
                              style: TextStyle(
                                fontFamily: 'Kanit',
                                fontSize: 16,
                                color: Color(0xff000000),
                              ),
                            )
                          ],
                        ),
                        trailing: CupertinoSwitch(
                          value: _switch1,
                          onChanged: (bool value) {
                            setState(() {
                              _switch1 = value;
                            });
                          },
                        ),
                        onTap: () {
                          setState(() {
                            _switch1 = !_switch1;
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 0, left: 1),
                    child: MergeSemantics(
                      child: ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 10,
                              height: 10,
                              decoration: BoxDecoration(
                                color: HexColor("8A6CBC"),
                                shape: BoxShape.circle,
                              ),
                            ),
                            Text(
                              ' ความตึงเครียด',
                              style: TextStyle(
                                fontFamily: 'Kanit',
                                fontSize: 16,
                                color: Color(0xff000000),
                              ),
                            )
                          ],
                        ),
                        trailing: CupertinoSwitch(
                          value: _switch2,
                          onChanged: (bool value) {
                            setState(() {
                              _switch2 = value;
                            });
                          },
                        ),
                        onTap: () {
                          setState(() {
                            _switch2 = !_switch2;
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 0, left: 1),
                    child: MergeSemantics(
                      child: ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 10,
                              height: 10,
                              decoration: BoxDecoration(
                                color: HexColor("75BE43"),
                                shape: BoxShape.circle,
                              ),
                            ),
                            Text(
                              ' ความเมื่อยล้า',
                              style: TextStyle(
                                fontFamily: 'Kanit',
                                fontSize: 16,
                                color: Color(0xff000000),
                              ),
                            )
                          ],
                        ),
                        trailing: CupertinoSwitch(
                          value: _switch3,
                          onChanged: (bool value) {
                            setState(() {
                              _switch3 = value;
                            });
                          },
                        ),
                        onTap: () {
                          setState(() {
                            _switch3 = !_switch3;
                          });
                        },
                      ),
                    ),
                  )
                ]))));
  }

  Widget chat() {

    date1 = toDate.subtract(Duration(days: 2));
    date2 = toDate.subtract(Duration(days: 3));

   // dt.add(DataPoint<DateTime>(value: 10, xAxis: date1));
    //dt.add(DataPoint<DateTime>(value: 20, xAxis: date2));
    //dt.add(DataPoint<DateTime>(value: 30, xAxis: date2));


    return StreamBuilder<List<Map>>(
      stream: blocWell.items,
      builder: (context, snapshot) {
      if (snapshot.hasData &&  snapshot.data != null && snapshot.data.length > 0) {

        var item = snapshot.data;
    print(_switch1);
       // date1 = Jiffy("2019-10-18") as DateTime;
        for (var s in item) {
            //  print(s["datefull"]);
          var val = double.parse(s["val"].toString());
          var type = s["type"].toString();
          var day = s["day"].toString();
          var month = s["month"].toString();
          var year =  s["year"].toString();

          var date = DateTime.parse("$year-$month-$day 00:00:00");
         // print("type $type");
          if(type == "1" ){
            dt1.add(DataPoint<DateTime>(value: val, xAxis: date));
          }else if(type == "2" ){
            dt2.add(DataPoint<DateTime>(value: val, xAxis: date));
          }else if(type == "3" ){
            dt3.add(DataPoint<DateTime>(value: val, xAxis: date));
          }

        }
        return Container(
          height: 200,
          width: MediaQuery.of(context).size.width * 0.7,
          child: BezierChart(
            fromDate: fromDate,
            bezierChartScale: BezierChartScale.WEEKLY,
            toDate: toDate,
            selectedDate: toDate,
            footerDateTimeBuilder: (DateTime value, BezierChartScale scaleType) {
              final newFormat = intl.DateFormat('dd');
              return newFormat.format(value);
            },
            series: [
               if(_switch1) BezierLine(
                lineColor: HexColor("C4403E"),
                onMissingValue: (dateTime) {
                  if (dateTime.day.isEven) {
                    return 0.0;
                  }
                  return 0.0;
                },
                data: dt1,
              )  ,
              if(_switch2) BezierLine(
                lineColor: HexColor("8A6CBC"),
                onMissingValue: (dateTime) {
                  if (dateTime.day.isEven) {
                    return 0.0;
                  }
                  return 0.0;
                },
                data: dt2,
              ), if(_switch3) BezierLine(
                lineColor: HexColor("75BE43"),
                onMissingValue: (dateTime) {
                  if (dateTime.day.isEven) {
                    return 0.0;
                  }
                  return 0.0;
                },
                data: dt3,
              )
            ],
            config: BezierChartConfig(
              verticalIndicatorFixedPosition: true,
              bubbleIndicatorColor: Colors.white.withOpacity(0.9),
              stepsYAxis: 10,
              footerHeight: 20,
              xAxisTextStyle: TextStyle(
                fontFamily: 'Kanit',
                fontSize: 10,
                color: Color(0xff000000),
              ),
              verticalIndicatorStrokeWidth: 3.0,
              verticalIndicatorColor: Colors.white,
              showVerticalIndicator: true,
              backgroundColor: Colors.white,
              snap: false,
            ),
          ),
        );
      }else{
        return Container();
      }
      }
    );
  }
}

class _Title extends StatelessWidget {
  const _Title({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          "Wellness Check-in",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: HexColor("000000"),
            fontFamily: "Kanit",
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        )
      ],
    );
  }
}
