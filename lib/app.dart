import 'dart:io';
import 'package:flutter/material.dart';
import 'package:ibd_application/pages/home/splash_page.dart';
import 'package:ibd_application/pages/howto/landing_page.dart';
import 'package:mono_kit/mono_kit.dart';
import 'package:provider/provider.dart';
import 'package:ibd_application/pages/home/home_page.dart';
import 'theme.dart';
import 'router.dart';

import 'package:flutter/services.dart';


class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
      Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,  
    ));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: buildLightTheme(context),
      darkTheme: buildLightTheme(context),
//      themeMode: ThemeMode.dark,
    //  home: HomePage.wrapped(),
      home: SplashPage(),
      onGenerateRoute: Provider.of<Router>(context).onGenerateRoute,
      builder: (context, child) => TextScaleFactor(child: child),
    );
  }
}