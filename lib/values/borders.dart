/*
*  borders.dart
*  Main_1
*
*  Created by [Author].
*  Copyright © 2018 [Company]. All rights reserved.
    */

import 'package:flutter/rendering.dart';


class Borders {
  static const BorderSide primaryBorder = BorderSide(
    color: Color.fromARGB(255, 226, 232, 237),
    width: 1,
    style: BorderStyle.solid,
  );
}