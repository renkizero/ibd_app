/*
*  gradients.dart
*  Main_1
*
*  Created by [Author].
*  Copyright © 2018 [Company]. All rights reserved.
    */

import 'package:flutter/rendering.dart';


class Gradients {
  static const Gradient primaryGradient = LinearGradient(
    begin: Alignment(0.29475, 0.07834),
    end: Alignment(0.75465, 1.42425),
    stops: [
      0,
      1,
    ],
    colors: [
      Color.fromARGB(255, 138, 108, 188),
      Color.fromARGB(255, 243, 123, 125),
    ],
  );
}