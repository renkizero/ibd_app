/*
*  colors.dart
*  Main_1
*
*  Created by [Author].
*  Copyright © 2018 [Company]. All rights reserved.
    */

import 'dart:ui';


class AppColors {
  static const Color primaryBackground = Color.fromARGB(255, 255, 255, 255);
  static const Color primaryElement = Color.fromARGB(255, 226, 232, 237);
  static const Color secondaryElement = Color.fromARGB(255, 255, 255, 255);
  static const Color accentElement = Color.fromARGB(255, 196, 64, 62);
  static const Color primaryText = Color.fromARGB(255, 76, 82, 100);
  static const Color secondaryText = Color.fromARGB(255, 255, 255, 255);
  static const Color accentText = Color.fromARGB(255, 94, 69, 137);
  static const Color blackText = Color.fromARGB(255, 94, 69, 137);
}