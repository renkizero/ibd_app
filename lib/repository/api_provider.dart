import 'dart:convert';
import 'package:ibd_application/models/entities/entities.dart';
import 'package:dio/dio.dart';

class UserApiProvider {
  final String _endpoint1 = "http://bnktoday.com/quiz1.json";
  final String _endpoint2 = "http://bnktoday.com/quiz2.json";
  final String _endpoint3 = "http://bnktoday.com/quiz3.json";
  final String _endpointChecking = "http://bnktoday.com/check.json";
  final Dio _dio = Dio();

  Future<QuizResult> getQuiz() async {
    try {
      Response response = await _dio.get(_endpoint1);
      return QuizResult.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<QuizResult> getQuiz2() async {
    try {
      Response response = await _dio.get(_endpoint2);
      return QuizResult.fromJson(response.data);
    } catch (error, stacktrace) {}
  }

  Future<QuizResult> getQuiz3() async {
    try {
      Response response = await _dio.get(_endpoint3);
      return QuizResult.fromJson(response.data);
    } catch (error, stacktrace) {}
  }


  Future<QuizResult> getCheckingQuiz() async {
    try {
      Response response = await _dio.get(_endpointChecking);
      return QuizResult.fromJson(response.data);
    } catch (error, stacktrace) {}
  }
}
