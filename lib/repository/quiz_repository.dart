import 'package:ibd_application/models/entities/entities.dart';
import 'package:ibd_application/repository/api_provider.dart';

class UserRepository {
  UserApiProvider _apiProvider = UserApiProvider();

  Future<QuizResult> getQuiz() {
    return _apiProvider.getQuiz();
  }

  Future<QuizResult> getQuiz2() {
    return _apiProvider.getQuiz2();
  }

  Future<QuizResult> getQuiz3() {
    return _apiProvider.getQuiz3();
  }


  Future<QuizResult> getCheckingQuiz() {
    return _apiProvider.getCheckingQuiz();
  }
}
