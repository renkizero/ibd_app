import 'package:flutter/material.dart';
import 'package:ibd_application/pages/pages.dart';
import 'package:ibd_application/pages/quiz/quiz_page.dart';
import 'package:mono_kit/mono_kit.dart';
import 'util.dart';
import 'package:ibd_application/pages//home/originals_page.dart';


typedef WidgetPageBuilder = Widget Function(
  BuildContext context,
  RouteSettings settings,
);

class Router {
  static const root = '/';

  final _routes = <String, WidgetPageBuilder>{
    OriginalsPage.routeName: (context, settings) => const OriginalsPage(),
  };
  final _fadeRoutes = <String, WidgetPageBuilder>{};
  final _modalRoutes = <String, WidgetPageBuilder>{
    QuizPage.routeName: (context, settings) => const QuizPage(current: 0),
    //AppointMentAddPage.routeName: (context, settings) => const AppointMentAddPage(),
  };
  
    

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    logger.info(settings.name);
    var pageBuilder = _routes[settings.name];
    if (pageBuilder != null) {
      return MaterialPageRoute<void>(
        builder: (context) => pageBuilder(context, settings),
        settings: settings,
      );
    }
    pageBuilder = _fadeRoutes[settings.name];
    if (pageBuilder != null) {
      return FadePageRoute<void>(
        builder: (context) => pageBuilder(context, settings),
        settings: settings,
      );
    }

    pageBuilder = _modalRoutes[settings.name];
    if (pageBuilder != null) {
      return MaterialPageRoute<void>(
        builder: (context) => pageBuilder(context, settings),
        settings: settings,
        fullscreenDialog: true,
      );
    }

    assert(false, 'unexpected settings: $settings');
    return null;
  }
}
