import 'package:flutter/material.dart';
import 'package:sqlcool/sqlcool.dart';

Future<void> initDb(
    {@required Db db,
    String path = "idb.sqlite",
    bool absPath = false}) async {
 
  final memo = DbTable("memo")
    ..varchar("date")
    ..varchar("month")
    ..varchar("year")
     ..varchar("hour")
      ..varchar("min")
      ..varchar("note")
      ..integer("status")
      ..timestamp('updated')
    ..index("updated");



    final note = DbTable("note")
    ..varchar("subject")
    ..varchar("note")
     ..timestamp('updated')
    ..index("updated");



    final noti = DbTable("noti")
    ..varchar("subject")
    ..varchar("note")
     ..timestamp('updated')
    ..index("updated");





  final quiz = DbTable("quiz")
    ..varchar("topic")
    ..varchar("total")
    ..timestamp('choice');


  final well = DbTable("well")
    ..varchar("datefull")
    ..varchar("day")
    ..varchar("month")
    ..varchar("year")
    ..varchar("type")
    ..varchar("val");



  final paintinfo = DbTable("paintinfo")
    ..varchar("hospital")
    ..varchar("doctor")
    ..varchar("choice")
    ..varchar("med");


  final user = DbTable("users")
    ..varchar("title")
    ..varchar("firstname")
    ..varchar("lastname")
    ..varchar("email")
    ..varchar("phone")
    ..varchar("password");



  final populateQueries = <String>[
    'INSERT INTO quiz(topic,total,choice) VALUES("1","4","0")',
    'INSERT INTO quiz(topic,total,choice) VALUES("2","3","0")',
    'INSERT INTO quiz(topic,total,choice) VALUES("3","3","0")',
    'INSERT INTO paintinfo(hospital,doctor,choice,med) VALUES("","","","")',
    'INSERT INTO users(title,firstname,lastname,email,phone,password) VALUES(1,"","","","","")',
  ];


  // prepare the queries
  // initialize the database
  await db
      .init(
          path: path,
          schema: [
            memo,
            note,
            noti,
            quiz,
            well,
            paintinfo,
            user
          ],
          queries: populateQueries,
          absolutePath: absPath,
          verbose: true)
      .catchError((dynamic e) {
    throw ("Error initializing the database: ${e.message}");
  });
  print("Database initialized with schema:");
  db.schema.describe();
}