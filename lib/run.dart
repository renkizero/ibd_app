import 'package:bloc_provider/bloc_provider.dart';
import 'package:disposable_provider/disposable_provider.dart';
import 'package:flutter/widgets.dart';
import 'package:ibd_application/bloc/ibd_bloc.dart';
import 'package:ibd_application/bloc/ibd_blocprovider.dart';
import 'package:provider/provider.dart';
import 'package:vsync_provider/vsync_provider.dart';
import 'package:disposable_provider/disposable_provider.dart';

import 'app.dart';
import 'models/model.dart';
import 'router.dart';
import 'theme.dart';
import 'init_db.dart';
import 'pages/constants/dbconfig.dart';

void run() {
  //SyncfusionLicense.registerLicense("NT8mJyc2IWhia31ifWN9Z2FoZnxiZnxhY2Fjc2FpY2VpYGRzAx5oITYpPDAhPCAgEzQ+Mjo/fTA8Pg==");

  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    MultiProvider(
      providers: [
        Provider(create: (context) => Router()),
        VsyncProvider(),
        ChangeNotifierProvider(create: (context) => ThemeNotifier()),
        DisposableProvider(
          create: (context) => PlayerNotifier(
            themeNotifier: context.read(),
            tickerProvider: VsyncProvider.of(context),
          ),
        )
      ],
      child: App(),
    ),
  );

   initDb(db: db);
}