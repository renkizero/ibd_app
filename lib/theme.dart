import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mono_kit/mono_kit.dart';
import 'package:provider/provider.dart';


const _primaryColor = Color(0xFFFC091C);


  
ThemeData buildLightTheme(BuildContext context) {

  return ThemeData.from(
    colorScheme: const ColorScheme.light(
      primary: _primaryColor,
      secondary: Colors.blueAccent,
    ),
  ).followLatestSpec().copyWith(
      appBarTheme: AppBarTheme(
        color: Colors.transparent,
            brightness: Brightness.light
      ),
      dividerColor: Colors.black38);
}

// TODO(mono): 対応
ThemeData buildDarkTheme() {
  return ThemeData.from(
    colorScheme: const ColorScheme.dark(
      secondary: _primaryColor,
    ),
  ).followLatestSpec();
}

class ThemeNotifier with ChangeNotifier {
  
  var _appBarBrightness = Brightness.dark;
  Brightness get appBarBrightness => _appBarBrightness;
  set appBarBrightness(Brightness brightness) {
    if (_appBarBrightness == brightness) {
      return;
    }
    _appBarBrightness = brightness;
    notifyListeners();
  }
}