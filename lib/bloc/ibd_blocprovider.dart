import 'package:bloc_provider/bloc_provider.dart';
import 'package:ibd_application/bloc/ibd_bloc.dart';
import 'package:flutter/widgets.dart';

class IBDBlocProvider extends BlocProvider<IBDBloc> {
  IBDBlocProvider({
    @required Widget child,
  }) : super(
          child: child,
          creator: (context, _bag) {
            assert(context != null);
            return IBDBloc();
          },
        );

  static IBDBloc of(BuildContext context) => BlocProvider.of(context);
}