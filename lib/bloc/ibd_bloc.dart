import 'package:ibd_application/pages/constants/dbconfig.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc_provider/bloc_provider.dart';
import 'package:sqlcool/sqlcool.dart';

class IBDBloc implements Bloc {
  final _countController = BehaviorSubject<int>.seeded(0);
  final _incrementController = PublishSubject<void>();

  IBDBloc() {
    _incrementController
        .scan<int>((s, v, i) => s + 1, 0)
        .pipe(_countController);
  }

void getNotes() async {
		// Retrieve all the notes from the database
		SelectBloc bloc = SelectBloc(
        database: db, table: "memo", orderBy: 'updated DESC', reactive: true);

	}


  ValueStream<int> get count => _countController;
  Sink<void> get increment => _incrementController.sink;

  @override
  void dispose() async {
    await _incrementController.close();
    await _countController.close();
  }
}