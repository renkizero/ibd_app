import '../bloc_setting.dart';

class AppointMentBloc extends BloCSetting {
  final String title = "setState() is powerful";

  bool isReload = false;

  shouldRefresh(state) {
    isReload = true;
    rebuildWidgets(
      setStates: () {
        isReload = isReload;
      },
      states: [state],
    );

  }


}

AppointMentBloc appointmentBloc;