import '../bloc_setting.dart';

class CheckingBloc extends BloCSetting {
  final String title = "setState() is powerful";
  int counter1 = 1;

  incrementCounter(state) {
    if (counter1 < 0) {
      counter1 = 0;
    }

    rebuildWidgets(
      setStates: () {
        counter1++;
      },
      states: [state],
    );

    // mainBloc?.counter1++;
  }

  decrementCounter(state) {
    if (counter1 <= 0) {
      counter1 = 0;
    }

    rebuildWidgets(
      setStates: () {
        if (counter1 <= 0) {
          counter1 = 0;
        } else {
          counter1--;
        }
      },
      states: [state],
    );
  }
}

CheckingBloc mainBloc;
