import 'package:flutter/widgets.dart';
import 'package:ibd_application/bloc/quiz_blocn.dart';

class QuizBlocProvider extends InheritedWidget {
  const QuizBlocProvider({
    Key key,
    @required Widget child,
    @required this.bloc,
  }) : super(key: key, child: child);

  final QuizBlocN bloc;

  static QuizBlocN of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<QuizBlocProvider>()
        .bloc;
  }

  @override
  bool updateShouldNotify(QuizBlocProvider oldWidget) =>
      bloc != oldWidget.bloc;
}