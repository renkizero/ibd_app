import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:ibd_application/bloc/index.dart';
import 'package:ibd_application/models/entities/entities.dart';
import 'package:ibd_application/repository/quiz_repository.dart';
import 'package:rxdart/rxdart.dart';

class CheckingBlocBloc {
  final UserRepository _repository = UserRepository();
  final BehaviorSubject<QuizResult> _subject = BehaviorSubject<QuizResult>();

  getQuiz() async {
    QuizResult response = await _repository.getCheckingQuiz();
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<QuizResult> get subject => _subject;
}

final bloc = CheckingBlocBloc();
